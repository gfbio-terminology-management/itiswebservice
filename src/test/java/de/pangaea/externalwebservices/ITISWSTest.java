package de.pangaea.externalwebservices;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import javax.json.JsonObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import gov.usgs.itis.itis_service.ITISWSServiceImpl;
import gov.usgs.itis.itis_service.data.xsd.SvcScientificName;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo;

public class ITISWSTest {

  private static ITISDAO itisDAO;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  @Before
  public void setUp() throws Exception {
    itisDAO = new ITISDAO();
  }

  @After
  public void tearDown() throws Exception {}

  @Test
  @Ignore
  public void testQuercus() {

    SvcScientificName[] check = itisDAO.searchByNameExact("Quercus robur");
    assertTrue(check.length == 1);
    SvcTaxonRankInfo taxonInfo = itisDAO.getTaxonRankInfoByTSN(check[0].getTsn());
    assertTrue(taxonInfo.getTsn().equals(check[0].getTsn()));
    assertTrue(taxonInfo.getRankName().equals("Species"));

    check = itisDAO.searchByNameExact("fora");
    assertNull(check[0]);

    // Pedicularis lanata
    check = itisDAO.searchByNameExact("Pedicularis lanata");
    taxonInfo = itisDAO.getTaxonRankInfoByTSN(check[0].getTsn());
  }

  @Test
  @Ignore
  public void WSSynonymsTest() {
    System.out.println("*** Test synonym service ***");
    ITISWSServiceImpl serviceIMPL = new ITISWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.getSynonyms("651623").create();
    assertNotNull(check);
    System.out.println(check);
  }

  @Test
  @Ignore
  public void WSMetadata() {
    System.out.println("*** Test metadata service ***");
    ITISWSServiceImpl serviceIMPL = new ITISWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.getMetadata().create();
    assertNotNull(check);
    System.out.println(check);
  }

  @Test
  @Ignore
  public void WSSearchTest() {
    System.out.println("*** Test search service ***");
    ITISWSServiceImpl serviceIMPL = new ITISWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.search("anomalina", "included").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  @Ignore
  public void WSHierarchy() {
    System.out.println("*** Test hierarchy service ***");
    ITISWSServiceImpl serviceIMPL = new ITISWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.getHierarchy("45796").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  @Ignore
  public void WSallbroader() {
    System.out.println("*** Test allbroader service ***");
    ITISWSServiceImpl serviceIMPL = new ITISWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.getAllBroader("45796").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  @Ignore
  public void WStermO() {
    System.out.println("*** Test tern o service ***");
    ITISWSServiceImpl serviceIMPL = new ITISWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.getTermInfosOriginal("44030").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  @Ignore
  public void WStermC() {
    System.out.println("*** Test tern c service ***");
    ITISWSServiceImpl serviceIMPL = new ITISWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.getTermInfosCombined("44030").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  @Ignore
  public void WStermP() {
    System.out.println("*** Test tern p service ***");
    ITISWSServiceImpl serviceIMPL = new ITISWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.getTermInfosProcessed("44030").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

}
