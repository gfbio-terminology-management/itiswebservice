/**
 * 
 */
package de.pangaea.externalwebservices;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import javax.json.JsonObject;
import org.apache.log4j.Logger;
import org.gfbio.interfaces.WSInterface.SearchTypes;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import gov.usgs.itis.itis_service.ITISRestfulWSImpl;
import gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData;
import gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness;
import gov.usgs.itis.itis_service.data.xsd.SvcScientificName;
import gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonName;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo;

public class ITISRestfulWSTest {

  private static ITISFacade itisFacade;
  private static ITISRestfulWSImpl restfulWSImpl;
  private static final Logger LOGGER = Logger.getLogger(ITISRestfulWSTest.class);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {

  }

  @Before
  public void setUp() throws Exception {
    itisFacade = new ITISFacade(false);
    restfulWSImpl = new ITISRestfulWSImpl();
  }

  @After
  public void tearDown() throws Exception {

  }

  @Test
  public void testMatchTypeSupport() {
    String match_type = "exact";
    assertTrue(restfulWSImpl.supportsMatchType(match_type));
    assertFalse(restfulWSImpl.supportsMatchType("included"));
  }

  @Test
  public void testGetCommonNamesFromTSN() {
    // 183833 -> African hunting dog, African Wild Dog, Painted Hunting dog
    SvcCommonNameList list = itisFacade.GetCommonNamesFromTSN("183833");
    // Arrays.stream(list.getCommonNames()).forEach(name ->
    // System.out.println(name.getCommonName()));
    assertEquals(3, list.getCommonNames().length);
    assertEquals("African hunting dog", list.getCommonNames(0).getCommonName());
  }

  @Test
  public void testGetScientificNameByTSN() {
    // 531894 -> "X Agroelymus adamsii var. anticostensis"
    SvcScientificName scName = itisFacade.getScientificName("531894");
    assertEquals("X Agroelymus adamsii var. anticostensis", scName.getCombinedName());
  }

  @Test
  public void testSearchByScientificName() {
    SvcScientificName[] res = itisFacade.searchByScientificName("Tardigrada", SearchTypes.exact);
    assertEquals(1, res.length);

    res = itisFacade.searchByScientificName("Quercus robur", SearchTypes.included);
    assertEquals(2, res.length);
    assertEquals("19405", res[0].getTsn());
    assertEquals("845209", res[1].getTsn());
  }

  @Test
  public void testGetTaxonRankInfoByTSN() {
    SvcTaxonRankInfo rn = itisFacade.getTaxonRankInfoByTSN("651623");
    assertEquals("Animalia", rn.getKingdomName());
    assertEquals("Species", rn.getRankName());
  }


  @Test
  public void testGetOtherSourcesFromTSN() {
    SvcTaxonOtherSourceList list = itisFacade.getOtherSourcesFromTSN("182662");
    assertEquals(3, list.getOtherSources().length);
    assertEquals("NODC Taxonomic Code", list.getOtherSources()[0].getSource());
  }

  @Test
  public void testGetCredibilityRating() {
    SvcCredibilityData credData = itisFacade.getCredibilityRatingFromTSN("183671");
    assertEquals("TWG standards met", credData.getCredRating());
  }

  @Test
  public void testGetGlobalSpeciesCompletenessFromTSN() {
    SvcGlobalSpeciesCompleteness cmplData =
        itisFacade.getGlobalSpeciesCompletenessFromTSN("180541");
    assertEquals("complete", cmplData.getCompleteness());
  }

  @Test
  public void testGetAcceptedNames() {
    SvcAcceptedNameList list = itisFacade.getAcceptedNamesFromTSN("183671");
    assertEquals("Acer rubrum var. drummondii", list.getAcceptedNames(0).getAcceptedName());
  }

  @Test
  public void testGetSynonyms() {
    // (1) check direct Facade access
    SvcSynonymNameList list = itisFacade.getTaxonSynonymsByTSN("526852");
    SvcTaxonName[] synonyms = list.getSynonyms();
    assertEquals(4, synonyms.length);

    // (2) check access via WS implementation
    ArrayList<JsonObject> check = restfulWSImpl.getSynonyms("651623").create();
    assertNotNull(check);
    assertEquals("Trixodes obesa",
        check.get(0).getJsonArray("synonyms").getString(0).replace("\"", ""));
  }

  @Test
  public void getTermInfosOriginal() {
    LOGGER.info(" *** getTermInfosOriginal() *** ");

    ArrayList<JsonObject> check = restfulWSImpl.getTermInfosOriginal("44030").create();
    assertNotNull(check);
    JsonObject j0 = check.get(0);
    assertEquals("Foraminiferida", j0.getString("ScientificName"));
    assertEquals("44030", j0.getString("TaxonomicSerialNumber"));
    assertEquals("Protozoa", j0.getString("Kingdom"));
    assertEquals("unknown", j0.getString("GlobalSpeciesCompleteness"));
  }

  @Test
  public void testSearch() {
    System.out.println("*** search() ***");
    ArrayList<JsonObject> check = restfulWSImpl.search("grizzly bear", "exact").create();

    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void testGetHierarchy() {
    System.out.println("*** getHierarchy() ***");
    ArrayList<JsonObject> check = restfulWSImpl.getHierarchy("45796").create();
    assertNotNull(check);
    assertEquals(12, check.size());
  }

  @Test
  public void testGetTermInfosCombined() {
    System.out.println("*** getTermInfosCombined() ***");
    ArrayList<JsonObject> check = restfulWSImpl.getTermInfosCombined("44030").create();
    assertNotNull(check);

    // object 1
    assertEquals("Foraminiferida", check.get(0).getString("label"));
    assertEquals("Protozoa", check.get(0).getString("kingdom"));
    assertEquals("unknown", check.get(0).getString("GlobalSpeciesCompleteness"));
    assertEquals("44030", check.get(0).getString("externalID"));

  }

  @Test
  public void testGetTermInfosProcessed() {
    System.out.println("*** getTermInfosProcessed() ***");
    ArrayList<JsonObject> check = restfulWSImpl.getTermInfosProcessed("44030").create();
    assertNotNull(check);

    // object 1
    assertEquals("Foraminiferida", check.get(0).getString("label"));
    assertEquals("Protozoa", check.get(0).getString("kingdom"));
    assertEquals("44030", check.get(0).getString("externalID"));
    assertEquals("Order", check.get(0).getString("rank"));
  }

  @Test
  public void testGetAllBroader() {
    System.out.println("*** testGetAllBroader() ***");
    ArrayList<JsonObject> check = restfulWSImpl.getAllBroader("558090").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }
}
