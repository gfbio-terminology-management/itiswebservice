/**
 * 
 */
package de.pangaea.externalwebservices;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.gfbio.interfaces.WSInterface.SearchTypes;
import org.json.JSONArray;
import org.json.JSONObject;
import gov.usgs.itis.itis_service.data.xsd.SvcAcceptedName;
import gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcCommonName;
import gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData;
import gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness;
import gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord;
import gov.usgs.itis.itis_service.data.xsd.SvcScientificName;
import gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonName;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSource;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo;

public class ITISFacade {

  private static final Logger LOGGER = Logger.getLogger(ITISFacade.class);
  private static final int TIMEOUT = 60000; // s
  private static final int NAME_TIMEOUT = 60000; // timeout for name-related searches, in s
  private boolean useRESTWS = false;

  public ITISFacade(boolean useREST) {
    LOGGER.info("ITISFacade loaded");
    LOGGER.debug("query timeout set to " + TIMEOUT);
    LOGGER.debug("name timeout set to " + NAME_TIMEOUT);

    this.useRESTWS = useREST;
  }

  /*
   * Facade Methods
   * 
   */

  public boolean checkITISAvailability() {

    LOGGER.info("checking ITIS connection");

    try {
      URL urlObj = new URL("https://services.itis.gov");
      HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
      con.setRequestMethod("GET");
      // Set connection timeout
      con.setConnectTimeout(NAME_TIMEOUT);
      con.connect();

      int code = con.getResponseCode();
      if (code == 200) {
        LOGGER.info("200 OK");
        return true;
      }

    } catch (Exception e) {
      LOGGER.error(e.getMessage());
      return false;
    }

    return false;

    // JSONObject jo = retrieveJSON("getLastChangeDate", null, NAME_TIMEOUT);

    // return jo != null ? true : false;
  }

  /**
   * 
   * @param scientificName
   * @param type
   * @return
   */
  public SvcScientificName[] searchByScientificName(String scientificName, SearchTypes type) {

    // LOGGER.info(" *** searchByScientificName *** ");

    JSONObject jo = null;
    JSONArray scnames = null;

    if (useRESTWS == true) {
      if (type == SearchTypes.exact)
        jo = ITISRESTClient.query("searchByScientificNameExact", scientificName, NAME_TIMEOUT);
      else if (type == SearchTypes.included)
        jo = ITISRESTClient.query("searchByScientificNameContains", scientificName, NAME_TIMEOUT);

      // either way we need to extract the sub-array containing the results
      scnames = jo.getJSONArray("scientificNames");
    } else {
      if (type == SearchTypes.exact)
        jo = ITISSolrClient.query("nameWOInd:", scientificName);
      else if (type == SearchTypes.included)
        jo = ITISSolrClient.query("nameWOInd:", "*" + scientificName + "*");

      // either way we need to extract the sub-array containing the results
      // nameWInd !? combinedName
      scnames = jo.getJSONArray("docs");
    }

    // if (jo == null)
    // return null;

    SvcScientificName[] svcsn = new SvcScientificName[scnames.length()];
    try {
      for (int i = 0; i < scnames.length(); i++) {
        // retrieve innermost object
        jo = (JSONObject) scnames.get(i);

        SvcScientificName snobj = new SvcScientificName();
        snobj.setTsn(jo.getString("tsn"));
        if (jo.has("combinedName") == true)
          snobj.setCombinedName(jo.getString("combinedName"));
        else
          snobj.setCombinedName(jo.getString("nameWInd"));
        snobj.setKingdom(jo.getString("kingdom"));
        svcsn[i] = snobj;
      }
    } catch (ClassCastException e) {
      LOGGER.info("JSON result set empty");
    }
    return svcsn;
  }

  /**
   * 
   * @param tsn
   * @return SvcTaxonRankInfo containing rank name and kingdom name
   */
  public SvcTaxonRankInfo getTaxonRankInfoByTSN(String tsn) {

    // LOGGER.info(" *** getTaxonRankInfoByTSN *** ");

    SvcTaxonRankInfo rnkInfo = new SvcTaxonRankInfo();
    JSONObject jo = null;

    if (useRESTWS) {
      jo = ITISRESTClient.query("getTaxonomicRankNameFromTSN", tsn);
      rnkInfo.setRankName(jo.getString("rankName"));
      rnkInfo.setKingdomName(jo.getString("kingdomName"));
    } else {
      jo = ITISSolrClient.query("tsn:", tsn);

      // nested 'docs' array
      JSONArray docs = jo.getJSONArray("docs");
      for (int i = 0; i < docs.length(); i++) {
        jo = docs.getJSONObject(i);
        rnkInfo.setRankName(jo.getString("rank"));
        rnkInfo.setKingdomName(jo.getString("kingdom"));

      }

      // for (Object o : docs) {
      // jo = (JSONObject) o;
      // rnkInfo.setRankName(jo.getString("rank"));
      // rnkInfo.setKingdomName(jo.getString("kingdom"));
      // }
    }

    return rnkInfo;
  }

  public SvcSynonymNameList getTaxonSynonymsByTSN(String tsn) {

    JSONObject jo = ITISRESTClient.query("getSynonymNamesFromTSN", tsn);

    if (jo == null)
      return null;

    JSONArray synonyms = jo.getJSONArray("synonyms");
    SvcTaxonName[] synonymsarr = new SvcTaxonName[synonyms.length()];
    SvcSynonymNameList scnNames = new SvcSynonymNameList();

    try {

      for (int i = 0; i < synonyms.length(); i++) {

        // retrieve innermost object
        jo = (JSONObject) synonyms.get(i);
        SvcTaxonName tname = new SvcTaxonName();
        tname.setSciName(jo.getString("sciName"));
        synonymsarr[i] = tname;
      }

      scnNames.setSynonyms(synonymsarr);
    } catch (ClassCastException e) {
      LOGGER.info("JSON result set empty");
    }

    return scnNames;
  }

  public SvcCommonName[] searchByCommonName(String commonName, SearchTypes type) {

    // LOGGER.info(" *** searchByCommonName *** ");
    long startTime = System.nanoTime();
    // we need the TSN, thus SOLR API cannot be used right now

    JSONObject jo = null, jobw = null, joew = null;

    SvcCommonName[] cmarr1 = {};
    SvcCommonName[] cmarr2 = {};

    // TODO 10/21/20 FB
    // Is searchByCommonName the intersection of ...BeginsWith and ...EndsWith?

    if (type == SearchTypes.included) {
      // if (useRESTWS) {
      jo = ITISRESTClient.query("searchByCommonName", commonName, NAME_TIMEOUT);
      return extractCommonNames(jo.getJSONArray("commonNames"));
      // }
      // else {
      // jo = ITISSolrClient.query("vernacular:", commonName);
      // return extractCommonNames(jo.getJSONArray("docs"));
      // }

    } else if (type == SearchTypes.exact) {
      // FIXME Solr API does not provide TSN, so we need to use the REST API here
      if (true) {
        jobw = ITISRESTClient.query("searchByCommonNameBeginsWith", commonName, NAME_TIMEOUT);
        joew = ITISRESTClient.query("searchByCommonNameEndsWith", commonName, NAME_TIMEOUT);

        if (jobw != null)
          cmarr1 = extractCommonNames(jobw.getJSONArray("commonNames"));
        if (joew != null)
          cmarr2 = extractCommonNames(joew.getJSONArray("commonNames"));

      } else {
        // TODO FB 11/11/20 use right search constraint
        // which is actually 'contains'
        jobw = ITISSolrClient.query("vernacular:", "*" + commonName + "*");

        // jobw = ITISSolrClient.query("vernacular:", commonName + "*");
        joew = ITISSolrClient.query("vernacular:", "*" + commonName + "*");

        // naming of JSON objects are somewhat different with Solr
        cmarr1 = extractCommonNames(jobw.getJSONArray("docs"));
        cmarr2 = extractCommonNames(joew.getJSONArray("docs"));
      }

      // if (jobw == null && joew == null)
      // return null;

      // SvcCommonName[] cmarr1 = extractCommonNames(jobw);
      // SvcCommonName[] cmarr2 = extractCommonNames(joew);

      Set<SvcCommonName> s1 = new HashSet<SvcCommonName>(Arrays.asList(cmarr1));
      Set<SvcCommonName> s2 = new HashSet<SvcCommonName>(Arrays.asList(cmarr2));
      if (s2.size() > 0)
        s1.retainAll(s2);

      SvcCommonName[] result = s1.toArray(new SvcCommonName[s1.size()]);

      long estimatedTime = System.nanoTime() - startTime;
      double elapsedTimeInSecond = (double) estimatedTime / 1_000_000_000;
      LOGGER.debug(elapsedTimeInSecond);

      return result;
    }

    return null;

  }

  public SvcHierarchyRecord[] getFullHierarchyFromTSN(String externalID) {
    JSONObject jo = ITISRESTClient.query("getFullHierarchyFromTSN", externalID, NAME_TIMEOUT);

    if (jo == null)
      return null;

    JSONArray list = jo.getJSONArray("hierarchyList");
    SvcHierarchyRecord[] records = new SvcHierarchyRecord[list.length()];

    try {

      for (int i = 0; i < list.length(); i++) {

        // retrieve innermost object
        jo = (JSONObject) list.get(i);
        SvcHierarchyRecord rec = new SvcHierarchyRecord();
        rec.setParentTsn(jo.getString("parentTsn"));
        rec.setTaxonName(jo.getString("taxonName"));
        rec.setRankName(jo.getString("rankName"));
        rec.setTsn(jo.getString("tsn"));
        records[i] = rec;
      }

    } catch (ClassCastException e) {
      LOGGER.info("JSON result set empty");
    }

    return records;
  }

  public SvcScientificName getScientificName(String tsn) {
    JSONObject jo = null;
    SvcScientificName scname = new SvcScientificName();

    if (useRESTWS) {
      jo = ITISRESTClient.query("getScientificNameFromTSN", tsn, NAME_TIMEOUT);
      scname.setCombinedName(jo.getString("combinedName"));
      scname.setTsn(jo.getString("tsn"));
    } else {
      jo = ITISSolrClient.query("tsn:", tsn);
      JSONArray docs = jo.getJSONArray("docs");
      for (int i = 0; i < docs.length(); i++) {
        jo = docs.getJSONObject(i);
        scname.setCombinedName(jo.getString("nameWInd"));
        scname.setTsn(jo.getString("tsn"));
      }
      // for (Object o : docs) {
      // jo = (JSONObject) o;
      // scname.setCombinedName(jo.getString("nameWInd"));
      // scname.setTsn(jo.getString("tsn"));
      // }
    }

    return scname;
  }

  /**
   * 
   * @param tsn
   * @return
   */
  public SvcCommonNameList GetCommonNamesFromTSN(String tsn) {
    JSONObject jo = null;
    JSONArray names = null;

    if (useRESTWS) {
      jo = ITISRESTClient.query("getCommonNamesFromTSN", tsn, NAME_TIMEOUT);
      names = jo.getJSONArray("commonNames");
    } else {
      jo = ITISSolrClient.query("tsn:", tsn);
      names = jo.getJSONArray("docs");
    }

    // ~ 1.6 s
    // JSONObject jo = ITISRESTClient.query("getCommonNamesFromTSN", tsn, NAME_TIMEOUT);
    //
    // if (jo == null)
    // return null;
    //
    // JSONArray names = jo.getJSONArray("commonNames");
    //
    SvcCommonName[] namesarr = new SvcCommonName[names.length()];
    SvcCommonNameList cmNames = new SvcCommonNameList();


    try {

      for (int i = 0; i < names.length(); i++) {

        // retrieve innermost object
        jo = (JSONObject) names.get(i);

        if (useRESTWS) {
          SvcCommonName cname = new SvcCommonName();
          cname.setCommonName(jo.getString("commonName"));
          namesarr[i] = cname;
        } else {
          if (jo.has("vernacular")) {
            JSONArray vernacular = jo.getJSONArray("vernacular"); // -> common names
            namesarr = new SvcCommonName[vernacular.length()];
            int n = 0;
            // for (Object o : vernacular) {
            for (int j = 0; j < vernacular.length(); j++) {
              SvcCommonName cname = new SvcCommonName();
              String name = vernacular.getString(j);
              name = name.replace("$", ";");
              name = name.replaceFirst(";", "");
              String[] parts = name.split(";");
              cname.setCommonName(parts[0]);

              namesarr[n] = cname;
              n++;
            }
          }
        }
      }

      cmNames.setCommonNames(namesarr);
    } catch (ClassCastException e) {
      LOGGER.info("JSON result set empty");
    }


    return cmNames;
  }

  public SvcAcceptedNameList getAcceptedNamesFromTSN(String tsn) {

    JSONObject jo = ITISRESTClient.query("getAcceptedNamesFromTSN", tsn);
    JSONArray names = jo.getJSONArray("acceptedNames");

    SvcAcceptedName[] namesarr = new SvcAcceptedName[names.length()];
    SvcAcceptedNameList accNames = new SvcAcceptedNameList();

    try {

      for (int i = 0; i < names.length(); i++) {

        // retrieve innermost object
        jo = (JSONObject) names.get(i);
        SvcAcceptedName aname = new SvcAcceptedName();
        aname.setAcceptedName(jo.getString("acceptedName"));
        aname.setAcceptedTsn(jo.getString("acceptedTsn"));
        namesarr[i] = aname;
      }

      accNames.setAcceptedNames(namesarr);
    } catch (ClassCastException e) {
      LOGGER.info("JSON result set empty");
    }

    return accNames;
  }

  public SvcCredibilityData getCredibilityRatingFromTSN(String termUri) {

    JSONObject jo = ITISRESTClient.query("getCredibilityRatingFromTSN", termUri);
    SvcCredibilityData credData = new SvcCredibilityData();
    credData.setCredRating(jo.getString("credRating"));

    return credData;
  }

  public SvcGlobalSpeciesCompleteness getGlobalSpeciesCompletenessFromTSN(String termUri) {
    JSONObject jo = ITISRESTClient.query("getGlobalSpeciesCompletenessFromTSN", termUri);

    SvcGlobalSpeciesCompleteness cmplData = new SvcGlobalSpeciesCompleteness();
    cmplData.setCompleteness(jo.getString("completeness"));

    return cmplData;
  }

  public SvcTaxonOtherSourceList getOtherSourcesFromTSN(String termUri) {
    JSONObject jo = ITISRESTClient.query("getOtherSourcesFromTSN", termUri);

    if (jo == null)
      return null;

    JSONArray sources = jo.getJSONArray("otherSources");

    SvcTaxonOtherSource[] sourcesarr = new SvcTaxonOtherSource[sources.length()];
    SvcTaxonOtherSourceList sourcesList = new SvcTaxonOtherSourceList();

    try {
      for (int i = 0; i < sources.length(); i++) {

        // retrieve innermost object
        jo = (JSONObject) sources.get(i);
        SvcTaxonOtherSource source = new SvcTaxonOtherSource();
        source.setSource(jo.getString("source"));
        sourcesarr[i] = source;
      }

    } catch (ClassCastException e) {
      LOGGER.error("JSON result set empty");
    }
    sourcesList.setOtherSources(sourcesarr);

    return sourcesList;
  }

  /*
   * Utility Methods
   * 
   */

  private SvcCommonName[] extractCommonNames(JSONArray cmnames) {
    List<SvcCommonName> tempList = new ArrayList<SvcCommonName>();
    try {
      for (int i = 0; i < cmnames.length(); i++) {
        JSONObject jo = (JSONObject) cmnames.get(i);

        SvcCommonName cname = new SvcCommonName();
        // FIXME Solr API does not provide TSN, so we need to use the REST API here
        if (true) {
          // when working with SOAP or REST calls
          cname.setCommonName(jo.getString("commonName"));
          cname.setTsn(jo.getString("tsn"));
          cname.setLanguage(jo.getString("language"));

          tempList.add(cname);
        } else {
          JSONArray vernacular = jo.getJSONArray("vernacular"); // -> common names
          for (int j = 0; j < vernacular.length(); j++) {
            // for (Object o : vernacular) {
            String name = vernacular.getString(j);
            name = name.replace("$", ";");
            name = name.replaceFirst(";", "");
            String[] parts = name.split(";");
            cname.setCommonName(parts[0]);
            // cname.setTsn(parts[3]);
            cname.setLanguage(parts[1]);

            tempList.add(cname);
          }
        }
      }

    } catch (ClassCastException e) {
      LOGGER.error("JSON result set empty");
    }
    // SvcCommonName[] cmNameList = tempList.toArray(new SvcCommonName[tempList.size()]);
    return tempList.toArray(new SvcCommonName[tempList.size()]);
  }
}
