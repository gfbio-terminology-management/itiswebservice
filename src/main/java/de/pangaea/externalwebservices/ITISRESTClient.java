package de.pangaea.externalwebservices;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * 
 * 
 *
 */
public class ITISRESTClient {

  private static final Logger LOGGER = Logger.getLogger(ITISRESTClient.class);
  private static final String ITIS_SERVICE_URL = "https://www.itis.gov/ITISWebService/jsonservice";

  public static JSONObject query(String methodName, String requestValue) {
    return query(methodName, requestValue, 60000);
  }

  /**
   * 
   * @param methodName
   * @param requestValue
   * @return
   */
  public static JSONObject query(String methodName, String requestValue, int timeout) {
    JSONObject root = null;

    try {
      StringBuilder itisUri = new StringBuilder(ITIS_SERVICE_URL + "/" + methodName);
      itisUri.append(requestValue != null ? "?query=" + encodeValue(requestValue) : "");

      LOGGER.info("calling ITIS " + itisUri.toString());

      URI uri = new URI(itisUri.toString());
      URLConnection urlConn = uri.toURL().openConnection();
      urlConn.setConnectTimeout(timeout); // ms
      urlConn.setReadTimeout(timeout); // ms
      urlConn.setRequestProperty("Accept",
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");

      JSONTokener tokener = new JSONTokener(urlConn.getInputStream());
      root = new JSONObject(tokener);

      LOGGER.info("call completed");

      return root;

    } catch (JSONException e) {
      LOGGER.error("JSON parsing exception");
    } catch (MalformedURLException | URISyntaxException e) {
      LOGGER.error("URL malformed");
    } catch (SocketTimeoutException e) {
      LOGGER.error("URL request timed out");
    } catch (IOException e) {
      LOGGER.error("Stream could not be opened");
    }

    return root;
  }

  // Method to encode a string value using `UTF-8` encoding scheme
  private static String encodeValue(String value) {
    try {
      return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
    } catch (UnsupportedEncodingException ex) {
      throw new RuntimeException(ex.getCause());
    }
  }
}
