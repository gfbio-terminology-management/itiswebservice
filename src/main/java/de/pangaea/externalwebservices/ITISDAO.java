package de.pangaea.externalwebservices;

import gov.usgs.itis.itis_service.ITISServiceLocator;
import gov.usgs.itis.itis_service.ITISServicePortType;
import gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcCommonName;
import gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata;
import gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData;
import gov.usgs.itis.itis_service.data.xsd.SvcFullRecord;
import gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness;
import gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord;
import gov.usgs.itis.itis_service.data.xsd.SvcScientificName;
import gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonName;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo;
import gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData;

import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;

public class ITISDAO {

	private static final Logger LOGGER = Logger.getLogger(ITISDAO.class);
	private ITISServiceLocator locator;
	private ITISServicePortType port;
	public final int waitTime = 1000;

	public ITISDAO() {
		init();
	}

	private void init() {

		locator = new ITISServiceLocator();
		try {
			port = locator.getITISServiceHttpSoap11Endpoint();

		} catch (ServiceException e) {

			e.printStackTrace();
		}
	}

	public SvcScientificName[] searchByNameExact(String scientificname) {

		for (int tries = 0;; tries++) {
			try {
				return port.searchByScientificNameExact(scientificname);
			} catch (RemoteException re) {
				if (tries >= 3) {
					LOGGER.warn("match failed, ITIS or the connection to lame " + scientificname, re);
					return null; // dont return empty because the port returns
									// also null and
					// the next filter step checks for null
				}

				try {
					LOGGER.warn("a match try failed, wait and next try " + scientificname);
					Thread.sleep(waitTime);

				} catch (InterruptedException e) {
					LOGGER.warn(" external interrupted" + scientificname, e);
				}
			}
		}
	}
	
	public SvcScientificName[] searchByNameContaining(String scientificname) {

        for (int tries = 0;; tries++) {
            try {
                return port.searchByScientificNameContains(scientificname);
            } catch (RemoteException re) {
                if (tries >= 3) {
                    LOGGER.warn("match failed, ITIS or the connection to lame " + scientificname, re);
                    return null; // dont return empty because the port returns
                                    // also null and
                    // the next filter step checks for null
                }

                try {
                    LOGGER.warn("a match try failed, wait and next try " + scientificname);
                    Thread.sleep(waitTime);

                } catch (InterruptedException e) {
                    LOGGER.warn(" external interrupted" + scientificname, e);
                }
            }
        }
    }
	public SvcCommonNameList searchByCommonName(String name) {

        for (int tries = 0;; tries++) {
            try{ 
                return port.searchByCommonName(name);
            } catch (RemoteException re) {
                if (tries >= 3) {
                    LOGGER.warn("match failed, ITIS or the connection to lame " + name, re);
                    return null; // dont return empty because the port returns
                                    // also null and
                    // the next filter step checks for null
                }

                try {
                    LOGGER.warn("a match try failed, wait and next try " + name);
                    Thread.sleep(waitTime);

                } catch (InterruptedException e) {
                    LOGGER.warn(" external interrupted" + name, e);
                }
            }
        }
    }
	public SvcCommonNameList searchByCommonNameExact(String name) {

        for (int tries = 0;; tries++) {
            try{ 
                SvcCommonNameList beg = port.searchByCommonNameBeginsWith(name);
                SvcCommonNameList end = port.searchByCommonNameEndsWith(name);
                SvcCommonNameList r = new SvcCommonNameList();
                ArrayList<SvcCommonName> al = new ArrayList<SvcCommonName>();
                    for(SvcCommonName n : beg.getCommonNames()){
                        for(SvcCommonName m : end.getCommonNames()){
                            if(n != null && m != null && n.equals(m)){
                                al.add(m);
                            }
                        }
                    }
                    SvcCommonName[] nl = new SvcCommonName[al.size()];
                    int i = 0;
                    for(SvcCommonName a : al){
                        nl[i] = a;
                        i++;
                    }
                    r.setCommonNames(nl);
                    return r;
            } catch (RemoteException re) {
                if (tries >= 3) {
                    LOGGER.warn("match failed, ITIS or the connection to lame " + name, re);
                    return null; // dont return empty because the port returns
                                    // also null and
                    // the next filter step checks for null
                }

                try {
                    LOGGER.warn("a match try failed, wait and next try " + name);
                    Thread.sleep(waitTime);

                } catch (InterruptedException e) {
                    LOGGER.warn(" external interrupted" + name, e);
                }
            }
        }
    }
	// Taxonomic Serial Number (TSN) 
	public SvcTaxonRankInfo getTaxonRankInfoByTSN(String tsn) {
		try {
			return port.getTaxonomicRankNameFromTSN(tsn);
		} catch (RemoteException re) {
			
			LOGGER.warn(re);
			return null;
		}
		
	}
	
	// Taxonomic Serial Number (TSN) 
    public SvcSynonymNameList getTaxonSynonymsByTSN(String tsn) {
        try {
            return port.getSynonymNamesFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
        
    }
    
    // Taxonomic Serial Number (TSN) 
    public SvcScientificName getScientificNameByTSN(String tsn) {
        try {
            return port.getScientificNameFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
        
    }

	public SvcUnacceptData getUnacceptabilityReasonFromTSN(String tsn) {
		SvcUnacceptData ud;
		try {
			ud = port.getUnacceptabilityReasonFromTSN(tsn);
		} catch (RemoteException re) {
			LOGGER.warn(re);
			return null;
		}
		return ud;
	}
	
    public SvcHierarchyRecord[] getFullHierarchyByTSN(String tsn) {
        try {
            return port.getFullHierarchyFromTSN(tsn).getHierarchyList();
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
    }
    public SvcHierarchyRecord getHierarchyByTSN(String tsn) {
        try {
            return port.getHierarchyUpFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
    }
    
    public SvcCommonNameList getCommonNamesByTSN(String tsn) {
        try {
            return port.getCommonNamesFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
    }
    
    public SvcSynonymNameList getSynonymNamesByTSN(String tsn) {
        try {
            return port.getSynonymNamesFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
    }
    public SvcCoreMetadata getCoreMetadataByTSN(String tsn) {
        try {
            return port.getCoreMetadataFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
    }
    public SvcCredibilityData getCredibilityRatingsFromTSN(String tsn) {
        try {
            return port.getCredibilityRatingFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
    }
    public SvcTaxonOtherSourceList getOtherSourcesFromTSN(String tsn) {
        try {
            return port.getOtherSourcesFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
    }
    public SvcAcceptedNameList getAcceptedNames(String tsn) {
        try {
            return port.getAcceptedNamesFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
    }
    public SvcTaxonRankInfo getTaxonRank(String tsn) {
        try {
            return port.getTaxonomicRankNameFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
    }
    public SvcScientificName getScientificName(String tsn) {
        try {
            return port.getScientificNameFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
    }
    public SvcGlobalSpeciesCompleteness getGlobalSpeciesCompleteness(String tsn) {
        try {
            return port.getGlobalSpeciesCompletenessFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
    }
    public SvcFullRecord getFullRecordFromTSN(String tsn) {
        try {
            return port.getFullRecordFromTSN(tsn);
        } catch (RemoteException re) {
            
            LOGGER.warn(re);
            return null;
        }
    }
}