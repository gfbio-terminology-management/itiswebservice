package de.pangaea.externalwebservices;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * 
 * 
 *
 */
public class ITISSolrClient {
  private static final Logger LOGGER = Logger.getLogger(ITISSolrClient.class);
  private static final String ITIS_SOLR_URL = "https://services.itis.gov/?q=";
  private static final int TIMEOUT = 30000; // s

  public static JSONObject query(String fieldName, String requestValue) {
    JSONObject root = null;

    try {
      StringBuilder itisUri = new StringBuilder(ITIS_SOLR_URL);
      itisUri.append(fieldName);
      itisUri.append(encodeValue(requestValue));
      itisUri.append("&wt=json");

      LOGGER.info("calling ITIS " + itisUri.toString());

      URI uri = new URI(itisUri.toString());
      URLConnection urlConn = uri.toURL().openConnection();
      urlConn.setConnectTimeout(TIMEOUT); // ms
      urlConn.setReadTimeout(TIMEOUT); // ms
      urlConn.setRequestProperty("Accept",
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");

      JSONTokener tokener = new JSONTokener(urlConn.getInputStream());
      root = new JSONObject(tokener);

      return root.getJSONObject("response");

    } catch (JSONException e) {
      LOGGER.error(e.getMessage());
      LOGGER.error("JSON parsing exception");
    } catch (MalformedURLException | URISyntaxException e) {
      LOGGER.error(e.getMessage());
      LOGGER.error("URL malformed");
    } catch (SocketTimeoutException e) {
      LOGGER.error("URL request timed out");
    } catch (IOException e) {
      LOGGER.error("Stream could not be opened");
    }

    return root;
  }



  // Method to encode a string value using `UTF-8` encoding scheme
  private static String encodeValue(String value) {
    return value.replace(" ", "%5C%20");
  }
}
