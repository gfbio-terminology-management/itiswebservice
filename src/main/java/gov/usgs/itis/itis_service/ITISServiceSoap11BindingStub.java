/**
 * ITISServiceSoap11BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service;

public class ITISServiceSoap11BindingStub extends org.apache.axis.client.Stub implements gov.usgs.itis.itis_service.ITISServicePortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[55];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
        _initOperationDesc4();
        _initOperationDesc5();
        _initOperationDesc6();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getRankNames");
        oper.setReturnType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcRankNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.metadata.xsd.SvcRankNameList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFullRecordFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcFullRecord"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcFullRecord.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getKingdomNameFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcKingdomInfo"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcKingdomInfo.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getGeographicValues");
        oper.setReturnType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcGeographicValueList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.metadata.xsd.SvcGeographicValueList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getITISTerms");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcItisTermList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcItisTermList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getDescription");
        oper.setReturnType(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov/xsd", "SvcDescription"));
        oper.setReturnClass(gov.usgs.itis.itis_service.xsd.SvcDescription.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getTaxonAuthorshipFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonAuthorship"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcTaxonAuthorship.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("searchByCommonNameBeginsWith");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCommonNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getTaxonomicUsageFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonUsageData"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcTaxonUsageData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFullHierarchyFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcHierarchyRecordList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getITISTermsFromCommonName");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcItisTermList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcItisTermList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getRecordFromLSID");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "lsid"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcLSIDRecord"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcLSIDRecord.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getCurrencyFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCurrencyData"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcCurrencyData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("searchForAnyMatchPaged");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "pageSize"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), java.lang.Integer.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "pageNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), java.lang.Integer.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "ascend"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), java.lang.Boolean.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcAnyMatchList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "anyMatchList"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getUnacceptabilityReasonFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcUnacceptData"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getTsnByVernacularLanguage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "language"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcVernacularTsnList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularTsnList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getSynonymNamesFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcSynonymNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("searchByScientificNameExact");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcScientificNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcScientificName[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "scientificNames"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("searchByScientificNameBeginsWith");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcScientificNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcScientificName[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "scientificNames"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getLastChangeDate");
        oper.setReturnType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcDateData"));
        oper.setReturnClass(gov.usgs.itis.itis_service.metadata.xsd.SvcDateData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("searchByCommonName");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCommonNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getCommonNamesFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCommonNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getGlobalSpeciesCompletenessFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcGlobalSpeciesCompleteness"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getDateDataFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonDateData"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcTaxonDateData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("searchByCommonNameEndsWith");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCommonNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getScientificNameFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcScientificName"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcScientificName.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAnyMatchCount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        oper.setReturnClass(java.lang.Long.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getTaxonomicRankNameFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonRankInfo"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAcceptedNamesFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcAcceptedNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getJurisdictionalOriginFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonJurisdictionalOriginList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOriginList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[29] = oper;

    }

    private static void _initOperationDesc4(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("searchByScientificNameContains");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcScientificNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcScientificName[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "scientificNames"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[30] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPublicationsFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonPublicationList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublicationList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[31] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getHierarchyUpFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcHierarchyRecord"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[32] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getITISTermsFromScientificName");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcItisTermList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcItisTermList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[33] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getReviewYearFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcReviewYear"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcReviewYear.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[34] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getLSIDFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[35] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getJurisdictionalOriginValues");
        oper.setReturnType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcOriginValueList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValueList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[36] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getTSNFromLSID");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "lsid"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[37] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getKingdomNames");
        oper.setReturnType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcKingdomNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomNameList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[38] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getGeographicDivisionsFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonGeoDivisionList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivisionList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[39] = oper;

    }

    private static void _initOperationDesc5(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getOtherSourcesFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonOtherSourceList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[40] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getCredibilityRatingFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCredibilityData"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[41] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getCommentDetailFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonCommentList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcTaxonCommentList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[42] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getHierarchyDownFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcHierarchyRecordList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[43] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getVernacularLanguages");
        oper.setReturnType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcVernacularLanguageList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularLanguageList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[44] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getCredibilityRatings");
        oper.setReturnType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcCredibilityValueList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.metadata.xsd.SvcCredibilityValueList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[45] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getCoreMetadataFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCoreMetadata"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[46] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getParentTSNFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcParentTsn"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcParentTsn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[47] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getJurisdictionValues");
        oper.setReturnType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcJurisdictionValueList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.metadata.xsd.SvcJurisdictionValueList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[48] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("searchByScientificNameEndsWith");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcScientificNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcScientificName[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "scientificNames"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[49] = oper;

    }

    private static void _initOperationDesc6(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("searchForAnyMatch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcAnyMatchList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "anyMatchList"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[50] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFullRecordFromLSID");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "lsid"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcFullRecord"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcFullRecord.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[51] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getCoverageFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCoverageData"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcCoverageData.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[52] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("searchByScientificName");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "srchKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcScientificNameList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcScientificName[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "scientificNames"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[53] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getExpertsFromTSN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "tsn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonExpertList"));
        oper.setReturnClass(gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpertList.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[54] = oper;

    }

    public ITISServiceSoap11BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public ITISServiceSoap11BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public ITISServiceSoap11BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcAcceptedName");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcAcceptedName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcAcceptedNameList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcAnyMatch");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcAnyMatchList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcAnyMatch");
            qName2 = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "anyMatchList");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCommonName");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcCommonName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCommonNameList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCoreMetadata");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCoverageData");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcCoverageData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCredibilityData");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCurrencyData");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcCurrencyData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcFullRecord");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcFullRecord.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcGlobalSpeciesCompleteness");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcHierarchyRecord");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcHierarchyRecordList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcItisTerm");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcItisTerm.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcItisTermList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcItisTermList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcKingdomInfo");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcKingdomInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcListBase");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcListBase.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcLSIDRecord");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcLSIDRecord.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcParentTsn");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcParentTsn.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcReferenceForElement");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcReviewYear");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcReviewYear.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcScientificName");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcScientificName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcScientificNameList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcScientificName[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcScientificName");
            qName2 = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "scientificNames");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcSynonymNameList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonAuthorship");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonAuthorship.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonComment");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonComment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonCommentList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonCommentList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonDateData");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonDateData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonExpert");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpert.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonExpertList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpertList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonGeoDivision");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivision.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonGeoDivisionList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivisionList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonJurisdictionalOrigin");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOrigin.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonJurisdictionalOriginList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOriginList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonName");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonomicBase");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonomicBase.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonOtherSource");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSource.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonOtherSourceList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonPublication");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublication.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonPublicationList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublicationList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonRankInfo");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonUsageData");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcTaxonUsageData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcUnacceptData");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov/xsd", "SvcDescription");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.xsd.SvcDescription.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcCredibilityValueList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcCredibilityValueList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcDateData");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcDateData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcGeographicValueList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcGeographicValueList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcJurisdictionValueList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcJurisdictionValueList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcKingdomName");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcKingdomNameList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomNameList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcMetadataListBase");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcMetadataListBase.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcOriginValue");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValue.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcOriginValueList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValueList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcRankName");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcRankName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcRankNameList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcRankNameList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcVernacularLanguageList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularLanguageList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcVernacularTsnList");
            cachedSerQNames.add(qName);
            cls = gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularTsnList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public gov.usgs.itis.itis_service.metadata.xsd.SvcRankNameList getRankNames() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getRankNames");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getRankNames"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcRankNameList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcRankNameList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.metadata.xsd.SvcRankNameList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcFullRecord getFullRecordFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getFullRecordFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getFullRecordFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcFullRecord) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcFullRecord) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcFullRecord.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcKingdomInfo getKingdomNameFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getKingdomNameFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getKingdomNameFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcKingdomInfo) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcKingdomInfo) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcKingdomInfo.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.metadata.xsd.SvcGeographicValueList getGeographicValues() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getGeographicValues");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getGeographicValues"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcGeographicValueList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcGeographicValueList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.metadata.xsd.SvcGeographicValueList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcItisTermList getITISTerms(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getITISTerms");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getITISTerms"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcItisTermList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcItisTermList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcItisTermList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.xsd.SvcDescription getDescription() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getDescription");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getDescription"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.xsd.SvcDescription) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.xsd.SvcDescription) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.xsd.SvcDescription.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonAuthorship getTaxonAuthorshipFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getTaxonAuthorshipFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getTaxonAuthorshipFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonAuthorship) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonAuthorship) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcTaxonAuthorship.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList searchByCommonNameBeginsWith(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:searchByCommonNameBeginsWith");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "searchByCommonNameBeginsWith"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonUsageData getTaxonomicUsageFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getTaxonomicUsageFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getTaxonomicUsageFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonUsageData) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonUsageData) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcTaxonUsageData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList getFullHierarchyFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getFullHierarchyFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getFullHierarchyFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcItisTermList getITISTermsFromCommonName(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getITISTermsFromCommonName");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getITISTermsFromCommonName"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcItisTermList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcItisTermList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcItisTermList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcLSIDRecord getRecordFromLSID(java.lang.String lsid) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getRecordFromLSID");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getRecordFromLSID"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {lsid});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcLSIDRecord) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcLSIDRecord) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcLSIDRecord.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcCurrencyData getCurrencyFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getCurrencyFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getCurrencyFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCurrencyData) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCurrencyData) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcCurrencyData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[] searchForAnyMatchPaged(java.lang.String srchKey, java.lang.Integer pageSize, java.lang.Integer pageNum, java.lang.Boolean ascend) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:searchForAnyMatchPaged");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "searchForAnyMatchPaged"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey, pageSize, pageNum, ascend});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[]) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData getUnacceptabilityReasonFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getUnacceptabilityReasonFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getUnacceptabilityReasonFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularTsnList getTsnByVernacularLanguage(java.lang.String language) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getTsnByVernacularLanguage");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getTsnByVernacularLanguage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {language});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularTsnList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularTsnList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularTsnList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList getSynonymNamesFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getSynonymNamesFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getSynonymNamesFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificNameExact(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:searchByScientificNameExact");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "searchByScientificNameExact"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcScientificName[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcScientificName[]) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcScientificName[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificNameBeginsWith(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:searchByScientificNameBeginsWith");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "searchByScientificNameBeginsWith"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcScientificName[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcScientificName[]) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcScientificName[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.metadata.xsd.SvcDateData getLastChangeDate() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getLastChangeDate");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getLastChangeDate"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcDateData) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcDateData) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.metadata.xsd.SvcDateData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList searchByCommonName(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:searchByCommonName");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "searchByCommonName"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList getCommonNamesFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getCommonNamesFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getCommonNamesFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness getGlobalSpeciesCompletenessFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getGlobalSpeciesCompletenessFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getGlobalSpeciesCompletenessFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonDateData getDateDataFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getDateDataFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getDateDataFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonDateData) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonDateData) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcTaxonDateData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList searchByCommonNameEndsWith(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:searchByCommonNameEndsWith");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "searchByCommonNameEndsWith"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName getScientificNameFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getScientificNameFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getScientificNameFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcScientificName) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcScientificName) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcScientificName.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.Long getAnyMatchCount(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getAnyMatchCount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getAnyMatchCount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.Long) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.Long) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.Long.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo getTaxonomicRankNameFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getTaxonomicRankNameFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getTaxonomicRankNameFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList getAcceptedNamesFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getAcceptedNamesFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getAcceptedNamesFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOriginList getJurisdictionalOriginFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getJurisdictionalOriginFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getJurisdictionalOriginFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOriginList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOriginList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOriginList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificNameContains(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[30]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:searchByScientificNameContains");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "searchByScientificNameContains"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcScientificName[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcScientificName[]) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcScientificName[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublicationList getPublicationsFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[31]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getPublicationsFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getPublicationsFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublicationList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublicationList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublicationList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord getHierarchyUpFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[32]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getHierarchyUpFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getHierarchyUpFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcItisTermList getITISTermsFromScientificName(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[33]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getITISTermsFromScientificName");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getITISTermsFromScientificName"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcItisTermList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcItisTermList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcItisTermList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcReviewYear getReviewYearFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[34]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getReviewYearFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getReviewYearFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcReviewYear) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcReviewYear) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcReviewYear.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getLSIDFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[35]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getLSIDFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getLSIDFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValueList getJurisdictionalOriginValues() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[36]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getJurisdictionalOriginValues");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getJurisdictionalOriginValues"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValueList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValueList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValueList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public java.lang.String getTSNFromLSID(java.lang.String lsid) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[37]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getTSNFromLSID");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getTSNFromLSID"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {lsid});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomNameList getKingdomNames() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[38]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getKingdomNames");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getKingdomNames"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomNameList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomNameList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomNameList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivisionList getGeographicDivisionsFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[39]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getGeographicDivisionsFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getGeographicDivisionsFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivisionList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivisionList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivisionList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList getOtherSourcesFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[40]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getOtherSourcesFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getOtherSourcesFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData getCredibilityRatingFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[41]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getCredibilityRatingFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getCredibilityRatingFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonCommentList getCommentDetailFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[42]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getCommentDetailFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getCommentDetailFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonCommentList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonCommentList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcTaxonCommentList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList getHierarchyDownFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[43]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getHierarchyDownFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getHierarchyDownFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularLanguageList getVernacularLanguages() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[44]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getVernacularLanguages");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getVernacularLanguages"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularLanguageList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularLanguageList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularLanguageList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.metadata.xsd.SvcCredibilityValueList getCredibilityRatings() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[45]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getCredibilityRatings");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getCredibilityRatings"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcCredibilityValueList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcCredibilityValueList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.metadata.xsd.SvcCredibilityValueList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata getCoreMetadataFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[46]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getCoreMetadataFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getCoreMetadataFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcParentTsn getParentTSNFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[47]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getParentTSNFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getParentTSNFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcParentTsn) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcParentTsn) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcParentTsn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.metadata.xsd.SvcJurisdictionValueList getJurisdictionValues() throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[48]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getJurisdictionValues");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getJurisdictionValues"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcJurisdictionValueList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.metadata.xsd.SvcJurisdictionValueList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.metadata.xsd.SvcJurisdictionValueList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificNameEndsWith(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[49]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:searchByScientificNameEndsWith");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "searchByScientificNameEndsWith"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcScientificName[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcScientificName[]) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcScientificName[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[] searchForAnyMatch(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[50]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:searchForAnyMatch");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "searchForAnyMatch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[]) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcFullRecord getFullRecordFromLSID(java.lang.String lsid) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[51]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getFullRecordFromLSID");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getFullRecordFromLSID"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {lsid});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcFullRecord) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcFullRecord) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcFullRecord.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcCoverageData getCoverageFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[52]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getCoverageFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getCoverageFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCoverageData) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcCoverageData) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcCoverageData.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificName(java.lang.String srchKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[53]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:searchByScientificName");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "searchByScientificName"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {srchKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcScientificName[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcScientificName[]) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcScientificName[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpertList getExpertsFromTSN(java.lang.String tsn) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[54]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getExpertsFromTSN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://itis_service.itis.usgs.gov", "getExpertsFromTSN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {tsn});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpertList) _resp;
            } catch (java.lang.Exception _exception) {
                return (gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpertList) org.apache.axis.utils.JavaUtils.convert(_resp, gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpertList.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
