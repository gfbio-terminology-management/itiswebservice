/**
 * SvcTaxonComment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcTaxonComment  implements java.io.Serializable {
    private java.lang.String commentDetail;

    private java.lang.String commentId;

    private java.lang.String commentTimeStamp;

    private java.lang.String commentator;

    private java.lang.String updateDate;

    public SvcTaxonComment() {
    }

    public SvcTaxonComment(
           java.lang.String commentDetail,
           java.lang.String commentId,
           java.lang.String commentTimeStamp,
           java.lang.String commentator,
           java.lang.String updateDate) {
           this.commentDetail = commentDetail;
           this.commentId = commentId;
           this.commentTimeStamp = commentTimeStamp;
           this.commentator = commentator;
           this.updateDate = updateDate;
    }


    /**
     * Gets the commentDetail value for this SvcTaxonComment.
     * 
     * @return commentDetail
     */
    public java.lang.String getCommentDetail() {
        return commentDetail;
    }


    /**
     * Sets the commentDetail value for this SvcTaxonComment.
     * 
     * @param commentDetail
     */
    public void setCommentDetail(java.lang.String commentDetail) {
        this.commentDetail = commentDetail;
    }


    /**
     * Gets the commentId value for this SvcTaxonComment.
     * 
     * @return commentId
     */
    public java.lang.String getCommentId() {
        return commentId;
    }


    /**
     * Sets the commentId value for this SvcTaxonComment.
     * 
     * @param commentId
     */
    public void setCommentId(java.lang.String commentId) {
        this.commentId = commentId;
    }


    /**
     * Gets the commentTimeStamp value for this SvcTaxonComment.
     * 
     * @return commentTimeStamp
     */
    public java.lang.String getCommentTimeStamp() {
        return commentTimeStamp;
    }


    /**
     * Sets the commentTimeStamp value for this SvcTaxonComment.
     * 
     * @param commentTimeStamp
     */
    public void setCommentTimeStamp(java.lang.String commentTimeStamp) {
        this.commentTimeStamp = commentTimeStamp;
    }


    /**
     * Gets the commentator value for this SvcTaxonComment.
     * 
     * @return commentator
     */
    public java.lang.String getCommentator() {
        return commentator;
    }


    /**
     * Sets the commentator value for this SvcTaxonComment.
     * 
     * @param commentator
     */
    public void setCommentator(java.lang.String commentator) {
        this.commentator = commentator;
    }


    /**
     * Gets the updateDate value for this SvcTaxonComment.
     * 
     * @return updateDate
     */
    public java.lang.String getUpdateDate() {
        return updateDate;
    }


    /**
     * Sets the updateDate value for this SvcTaxonComment.
     * 
     * @param updateDate
     */
    public void setUpdateDate(java.lang.String updateDate) {
        this.updateDate = updateDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcTaxonComment)) return false;
        SvcTaxonComment other = (SvcTaxonComment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.commentDetail==null && other.getCommentDetail()==null) || 
             (this.commentDetail!=null &&
              this.commentDetail.equals(other.getCommentDetail()))) &&
            ((this.commentId==null && other.getCommentId()==null) || 
             (this.commentId!=null &&
              this.commentId.equals(other.getCommentId()))) &&
            ((this.commentTimeStamp==null && other.getCommentTimeStamp()==null) || 
             (this.commentTimeStamp!=null &&
              this.commentTimeStamp.equals(other.getCommentTimeStamp()))) &&
            ((this.commentator==null && other.getCommentator()==null) || 
             (this.commentator!=null &&
              this.commentator.equals(other.getCommentator()))) &&
            ((this.updateDate==null && other.getUpdateDate()==null) || 
             (this.updateDate!=null &&
              this.updateDate.equals(other.getUpdateDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCommentDetail() != null) {
            _hashCode += getCommentDetail().hashCode();
        }
        if (getCommentId() != null) {
            _hashCode += getCommentId().hashCode();
        }
        if (getCommentTimeStamp() != null) {
            _hashCode += getCommentTimeStamp().hashCode();
        }
        if (getCommentator() != null) {
            _hashCode += getCommentator().hashCode();
        }
        if (getUpdateDate() != null) {
            _hashCode += getUpdateDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcTaxonComment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonComment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "commentDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "commentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentTimeStamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "commentTimeStamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentator");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "commentator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "updateDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
