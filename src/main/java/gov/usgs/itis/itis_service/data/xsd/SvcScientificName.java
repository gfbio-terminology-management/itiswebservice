/**
 * SvcScientificName.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcScientificName  extends gov.usgs.itis.itis_service.data.xsd.SvcTaxonomicBase  implements java.io.Serializable {
    private java.lang.String author;

    private java.lang.String combinedName;

    private java.lang.String kingdom;

    private java.lang.String unitInd1;

    private java.lang.String unitInd2;

    private java.lang.String unitInd3;

    private java.lang.String unitInd4;

    private java.lang.String unitName1;

    private java.lang.String unitName2;

    private java.lang.String unitName3;

    private java.lang.String unitName4;

    public SvcScientificName() {
    }

    public SvcScientificName(
           java.lang.String tsn,
           java.lang.String author,
           java.lang.String combinedName,
           java.lang.String kingdom,
           java.lang.String unitInd1,
           java.lang.String unitInd2,
           java.lang.String unitInd3,
           java.lang.String unitInd4,
           java.lang.String unitName1,
           java.lang.String unitName2,
           java.lang.String unitName3,
           java.lang.String unitName4) {
        super(
            tsn);
        this.author = author;
        this.combinedName = combinedName;
        this.kingdom = kingdom;
        this.unitInd1 = unitInd1;
        this.unitInd2 = unitInd2;
        this.unitInd3 = unitInd3;
        this.unitInd4 = unitInd4;
        this.unitName1 = unitName1;
        this.unitName2 = unitName2;
        this.unitName3 = unitName3;
        this.unitName4 = unitName4;
    }


    /**
     * Gets the author value for this SvcScientificName.
     * 
     * @return author
     */
    public java.lang.String getAuthor() {
        return author;
    }


    /**
     * Sets the author value for this SvcScientificName.
     * 
     * @param author
     */
    public void setAuthor(java.lang.String author) {
        this.author = author;
    }


    /**
     * Gets the combinedName value for this SvcScientificName.
     * 
     * @return combinedName
     */
    public java.lang.String getCombinedName() {
        return combinedName;
    }


    /**
     * Sets the combinedName value for this SvcScientificName.
     * 
     * @param combinedName
     */
    public void setCombinedName(java.lang.String combinedName) {
        this.combinedName = combinedName;
    }


    /**
     * Gets the kingdom value for this SvcScientificName.
     * 
     * @return kingdom
     */
    public java.lang.String getKingdom() {
        return kingdom;
    }


    /**
     * Sets the kingdom value for this SvcScientificName.
     * 
     * @param kingdom
     */
    public void setKingdom(java.lang.String kingdom) {
        this.kingdom = kingdom;
    }


    /**
     * Gets the unitInd1 value for this SvcScientificName.
     * 
     * @return unitInd1
     */
    public java.lang.String getUnitInd1() {
        return unitInd1;
    }


    /**
     * Sets the unitInd1 value for this SvcScientificName.
     * 
     * @param unitInd1
     */
    public void setUnitInd1(java.lang.String unitInd1) {
        this.unitInd1 = unitInd1;
    }


    /**
     * Gets the unitInd2 value for this SvcScientificName.
     * 
     * @return unitInd2
     */
    public java.lang.String getUnitInd2() {
        return unitInd2;
    }


    /**
     * Sets the unitInd2 value for this SvcScientificName.
     * 
     * @param unitInd2
     */
    public void setUnitInd2(java.lang.String unitInd2) {
        this.unitInd2 = unitInd2;
    }


    /**
     * Gets the unitInd3 value for this SvcScientificName.
     * 
     * @return unitInd3
     */
    public java.lang.String getUnitInd3() {
        return unitInd3;
    }


    /**
     * Sets the unitInd3 value for this SvcScientificName.
     * 
     * @param unitInd3
     */
    public void setUnitInd3(java.lang.String unitInd3) {
        this.unitInd3 = unitInd3;
    }


    /**
     * Gets the unitInd4 value for this SvcScientificName.
     * 
     * @return unitInd4
     */
    public java.lang.String getUnitInd4() {
        return unitInd4;
    }


    /**
     * Sets the unitInd4 value for this SvcScientificName.
     * 
     * @param unitInd4
     */
    public void setUnitInd4(java.lang.String unitInd4) {
        this.unitInd4 = unitInd4;
    }


    /**
     * Gets the unitName1 value for this SvcScientificName.
     * 
     * @return unitName1
     */
    public java.lang.String getUnitName1() {
        return unitName1;
    }


    /**
     * Sets the unitName1 value for this SvcScientificName.
     * 
     * @param unitName1
     */
    public void setUnitName1(java.lang.String unitName1) {
        this.unitName1 = unitName1;
    }


    /**
     * Gets the unitName2 value for this SvcScientificName.
     * 
     * @return unitName2
     */
    public java.lang.String getUnitName2() {
        return unitName2;
    }


    /**
     * Sets the unitName2 value for this SvcScientificName.
     * 
     * @param unitName2
     */
    public void setUnitName2(java.lang.String unitName2) {
        this.unitName2 = unitName2;
    }


    /**
     * Gets the unitName3 value for this SvcScientificName.
     * 
     * @return unitName3
     */
    public java.lang.String getUnitName3() {
        return unitName3;
    }


    /**
     * Sets the unitName3 value for this SvcScientificName.
     * 
     * @param unitName3
     */
    public void setUnitName3(java.lang.String unitName3) {
        this.unitName3 = unitName3;
    }


    /**
     * Gets the unitName4 value for this SvcScientificName.
     * 
     * @return unitName4
     */
    public java.lang.String getUnitName4() {
        return unitName4;
    }


    /**
     * Sets the unitName4 value for this SvcScientificName.
     * 
     * @param unitName4
     */
    public void setUnitName4(java.lang.String unitName4) {
        this.unitName4 = unitName4;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcScientificName)) return false;
        SvcScientificName other = (SvcScientificName) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.author==null && other.getAuthor()==null) || 
             (this.author!=null &&
              this.author.equals(other.getAuthor()))) &&
            ((this.combinedName==null && other.getCombinedName()==null) || 
             (this.combinedName!=null &&
              this.combinedName.equals(other.getCombinedName()))) &&
            ((this.kingdom==null && other.getKingdom()==null) || 
             (this.kingdom!=null &&
              this.kingdom.equals(other.getKingdom()))) &&
            ((this.unitInd1==null && other.getUnitInd1()==null) || 
             (this.unitInd1!=null &&
              this.unitInd1.equals(other.getUnitInd1()))) &&
            ((this.unitInd2==null && other.getUnitInd2()==null) || 
             (this.unitInd2!=null &&
              this.unitInd2.equals(other.getUnitInd2()))) &&
            ((this.unitInd3==null && other.getUnitInd3()==null) || 
             (this.unitInd3!=null &&
              this.unitInd3.equals(other.getUnitInd3()))) &&
            ((this.unitInd4==null && other.getUnitInd4()==null) || 
             (this.unitInd4!=null &&
              this.unitInd4.equals(other.getUnitInd4()))) &&
            ((this.unitName1==null && other.getUnitName1()==null) || 
             (this.unitName1!=null &&
              this.unitName1.equals(other.getUnitName1()))) &&
            ((this.unitName2==null && other.getUnitName2()==null) || 
             (this.unitName2!=null &&
              this.unitName2.equals(other.getUnitName2()))) &&
            ((this.unitName3==null && other.getUnitName3()==null) || 
             (this.unitName3!=null &&
              this.unitName3.equals(other.getUnitName3()))) &&
            ((this.unitName4==null && other.getUnitName4()==null) || 
             (this.unitName4!=null &&
              this.unitName4.equals(other.getUnitName4())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAuthor() != null) {
            _hashCode += getAuthor().hashCode();
        }
        if (getCombinedName() != null) {
            _hashCode += getCombinedName().hashCode();
        }
        if (getKingdom() != null) {
            _hashCode += getKingdom().hashCode();
        }
        if (getUnitInd1() != null) {
            _hashCode += getUnitInd1().hashCode();
        }
        if (getUnitInd2() != null) {
            _hashCode += getUnitInd2().hashCode();
        }
        if (getUnitInd3() != null) {
            _hashCode += getUnitInd3().hashCode();
        }
        if (getUnitInd4() != null) {
            _hashCode += getUnitInd4().hashCode();
        }
        if (getUnitName1() != null) {
            _hashCode += getUnitName1().hashCode();
        }
        if (getUnitName2() != null) {
            _hashCode += getUnitName2().hashCode();
        }
        if (getUnitName3() != null) {
            _hashCode += getUnitName3().hashCode();
        }
        if (getUnitName4() != null) {
            _hashCode += getUnitName4().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcScientificName.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcScientificName"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("author");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "author"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("combinedName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "combinedName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kingdom");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "kingdom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitInd1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "unitInd1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitInd2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "unitInd2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitInd3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "unitInd3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitInd4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "unitInd4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitName1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "unitName1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitName2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "unitName2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitName3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "unitName3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitName4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "unitName4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
