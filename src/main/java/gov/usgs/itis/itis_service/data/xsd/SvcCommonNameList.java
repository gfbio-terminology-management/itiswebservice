/**
 * SvcCommonNameList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcCommonNameList  extends gov.usgs.itis.itis_service.data.xsd.SvcListBase  implements java.io.Serializable {
    private gov.usgs.itis.itis_service.data.xsd.SvcCommonName[] commonNames;

    public SvcCommonNameList() {
    }

    public SvcCommonNameList(
           java.lang.String tsn,
           gov.usgs.itis.itis_service.data.xsd.SvcCommonName[] commonNames) {
        super(
            tsn);
        this.commonNames = commonNames;
    }


    /**
     * Gets the commonNames value for this SvcCommonNameList.
     * 
     * @return commonNames
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcCommonName[] getCommonNames() {
        return commonNames;
    }


    /**
     * Sets the commonNames value for this SvcCommonNameList.
     * 
     * @param commonNames
     */
    public void setCommonNames(gov.usgs.itis.itis_service.data.xsd.SvcCommonName[] commonNames) {
        this.commonNames = commonNames;
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcCommonName getCommonNames(int i) {
        return this.commonNames[i];
    }

    public void setCommonNames(int i, gov.usgs.itis.itis_service.data.xsd.SvcCommonName _value) {
        this.commonNames[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcCommonNameList)) return false;
        SvcCommonNameList other = (SvcCommonNameList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.commonNames==null && other.getCommonNames()==null) || 
             (this.commonNames!=null &&
              java.util.Arrays.equals(this.commonNames, other.getCommonNames())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCommonNames() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCommonNames());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCommonNames(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcCommonNameList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCommonNameList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commonNames");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "commonNames"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCommonName"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
