/**
 * SvcTaxonRankInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcTaxonRankInfo  extends gov.usgs.itis.itis_service.data.xsd.SvcTaxonomicBase  implements java.io.Serializable {
    private java.lang.String kingdomId;

    private java.lang.String kingdomName;

    private java.lang.String rankId;

    private java.lang.String rankName;

    public SvcTaxonRankInfo() {
    }

    public SvcTaxonRankInfo(
           java.lang.String tsn,
           java.lang.String kingdomId,
           java.lang.String kingdomName,
           java.lang.String rankId,
           java.lang.String rankName) {
        super(
            tsn);
        this.kingdomId = kingdomId;
        this.kingdomName = kingdomName;
        this.rankId = rankId;
        this.rankName = rankName;
    }


    /**
     * Gets the kingdomId value for this SvcTaxonRankInfo.
     * 
     * @return kingdomId
     */
    public java.lang.String getKingdomId() {
        return kingdomId;
    }


    /**
     * Sets the kingdomId value for this SvcTaxonRankInfo.
     * 
     * @param kingdomId
     */
    public void setKingdomId(java.lang.String kingdomId) {
        this.kingdomId = kingdomId;
    }


    /**
     * Gets the kingdomName value for this SvcTaxonRankInfo.
     * 
     * @return kingdomName
     */
    public java.lang.String getKingdomName() {
        return kingdomName;
    }


    /**
     * Sets the kingdomName value for this SvcTaxonRankInfo.
     * 
     * @param kingdomName
     */
    public void setKingdomName(java.lang.String kingdomName) {
        this.kingdomName = kingdomName;
    }


    /**
     * Gets the rankId value for this SvcTaxonRankInfo.
     * 
     * @return rankId
     */
    public java.lang.String getRankId() {
        return rankId;
    }


    /**
     * Sets the rankId value for this SvcTaxonRankInfo.
     * 
     * @param rankId
     */
    public void setRankId(java.lang.String rankId) {
        this.rankId = rankId;
    }


    /**
     * Gets the rankName value for this SvcTaxonRankInfo.
     * 
     * @return rankName
     */
    public java.lang.String getRankName() {
        return rankName;
    }


    /**
     * Sets the rankName value for this SvcTaxonRankInfo.
     * 
     * @param rankName
     */
    public void setRankName(java.lang.String rankName) {
        this.rankName = rankName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcTaxonRankInfo)) return false;
        SvcTaxonRankInfo other = (SvcTaxonRankInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.kingdomId==null && other.getKingdomId()==null) || 
             (this.kingdomId!=null &&
              this.kingdomId.equals(other.getKingdomId()))) &&
            ((this.kingdomName==null && other.getKingdomName()==null) || 
             (this.kingdomName!=null &&
              this.kingdomName.equals(other.getKingdomName()))) &&
            ((this.rankId==null && other.getRankId()==null) || 
             (this.rankId!=null &&
              this.rankId.equals(other.getRankId()))) &&
            ((this.rankName==null && other.getRankName()==null) || 
             (this.rankName!=null &&
              this.rankName.equals(other.getRankName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getKingdomId() != null) {
            _hashCode += getKingdomId().hashCode();
        }
        if (getKingdomName() != null) {
            _hashCode += getKingdomName().hashCode();
        }
        if (getRankId() != null) {
            _hashCode += getRankId().hashCode();
        }
        if (getRankName() != null) {
            _hashCode += getRankName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcTaxonRankInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonRankInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kingdomId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "kingdomId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kingdomName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "kingdomName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "rankId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "rankName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
