/**
 * SvcLSIDRecord.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcLSIDRecord  extends gov.usgs.itis.itis_service.data.xsd.SvcTaxonomicBase  implements java.io.Serializable {
    private java.lang.String authorship;

    private java.lang.String genusPart;

    private java.lang.String infragenericEpithet;

    private java.lang.String infraspecificEpithet;

    private java.lang.String lsid;

    private java.lang.String nameComplete;

    private java.lang.String nomenclaturalCode;

    private java.lang.String rank;

    private java.lang.String rankString;

    private java.lang.String specificEpithet;

    private java.lang.String uninomial;

    public SvcLSIDRecord() {
    }

    public SvcLSIDRecord(
           java.lang.String tsn,
           java.lang.String authorship,
           java.lang.String genusPart,
           java.lang.String infragenericEpithet,
           java.lang.String infraspecificEpithet,
           java.lang.String lsid,
           java.lang.String nameComplete,
           java.lang.String nomenclaturalCode,
           java.lang.String rank,
           java.lang.String rankString,
           java.lang.String specificEpithet,
           java.lang.String uninomial) {
        super(
            tsn);
        this.authorship = authorship;
        this.genusPart = genusPart;
        this.infragenericEpithet = infragenericEpithet;
        this.infraspecificEpithet = infraspecificEpithet;
        this.lsid = lsid;
        this.nameComplete = nameComplete;
        this.nomenclaturalCode = nomenclaturalCode;
        this.rank = rank;
        this.rankString = rankString;
        this.specificEpithet = specificEpithet;
        this.uninomial = uninomial;
    }


    /**
     * Gets the authorship value for this SvcLSIDRecord.
     * 
     * @return authorship
     */
    public java.lang.String getAuthorship() {
        return authorship;
    }


    /**
     * Sets the authorship value for this SvcLSIDRecord.
     * 
     * @param authorship
     */
    public void setAuthorship(java.lang.String authorship) {
        this.authorship = authorship;
    }


    /**
     * Gets the genusPart value for this SvcLSIDRecord.
     * 
     * @return genusPart
     */
    public java.lang.String getGenusPart() {
        return genusPart;
    }


    /**
     * Sets the genusPart value for this SvcLSIDRecord.
     * 
     * @param genusPart
     */
    public void setGenusPart(java.lang.String genusPart) {
        this.genusPart = genusPart;
    }


    /**
     * Gets the infragenericEpithet value for this SvcLSIDRecord.
     * 
     * @return infragenericEpithet
     */
    public java.lang.String getInfragenericEpithet() {
        return infragenericEpithet;
    }


    /**
     * Sets the infragenericEpithet value for this SvcLSIDRecord.
     * 
     * @param infragenericEpithet
     */
    public void setInfragenericEpithet(java.lang.String infragenericEpithet) {
        this.infragenericEpithet = infragenericEpithet;
    }


    /**
     * Gets the infraspecificEpithet value for this SvcLSIDRecord.
     * 
     * @return infraspecificEpithet
     */
    public java.lang.String getInfraspecificEpithet() {
        return infraspecificEpithet;
    }


    /**
     * Sets the infraspecificEpithet value for this SvcLSIDRecord.
     * 
     * @param infraspecificEpithet
     */
    public void setInfraspecificEpithet(java.lang.String infraspecificEpithet) {
        this.infraspecificEpithet = infraspecificEpithet;
    }


    /**
     * Gets the lsid value for this SvcLSIDRecord.
     * 
     * @return lsid
     */
    public java.lang.String getLsid() {
        return lsid;
    }


    /**
     * Sets the lsid value for this SvcLSIDRecord.
     * 
     * @param lsid
     */
    public void setLsid(java.lang.String lsid) {
        this.lsid = lsid;
    }


    /**
     * Gets the nameComplete value for this SvcLSIDRecord.
     * 
     * @return nameComplete
     */
    public java.lang.String getNameComplete() {
        return nameComplete;
    }


    /**
     * Sets the nameComplete value for this SvcLSIDRecord.
     * 
     * @param nameComplete
     */
    public void setNameComplete(java.lang.String nameComplete) {
        this.nameComplete = nameComplete;
    }


    /**
     * Gets the nomenclaturalCode value for this SvcLSIDRecord.
     * 
     * @return nomenclaturalCode
     */
    public java.lang.String getNomenclaturalCode() {
        return nomenclaturalCode;
    }


    /**
     * Sets the nomenclaturalCode value for this SvcLSIDRecord.
     * 
     * @param nomenclaturalCode
     */
    public void setNomenclaturalCode(java.lang.String nomenclaturalCode) {
        this.nomenclaturalCode = nomenclaturalCode;
    }


    /**
     * Gets the rank value for this SvcLSIDRecord.
     * 
     * @return rank
     */
    public java.lang.String getRank() {
        return rank;
    }


    /**
     * Sets the rank value for this SvcLSIDRecord.
     * 
     * @param rank
     */
    public void setRank(java.lang.String rank) {
        this.rank = rank;
    }


    /**
     * Gets the rankString value for this SvcLSIDRecord.
     * 
     * @return rankString
     */
    public java.lang.String getRankString() {
        return rankString;
    }


    /**
     * Sets the rankString value for this SvcLSIDRecord.
     * 
     * @param rankString
     */
    public void setRankString(java.lang.String rankString) {
        this.rankString = rankString;
    }


    /**
     * Gets the specificEpithet value for this SvcLSIDRecord.
     * 
     * @return specificEpithet
     */
    public java.lang.String getSpecificEpithet() {
        return specificEpithet;
    }


    /**
     * Sets the specificEpithet value for this SvcLSIDRecord.
     * 
     * @param specificEpithet
     */
    public void setSpecificEpithet(java.lang.String specificEpithet) {
        this.specificEpithet = specificEpithet;
    }


    /**
     * Gets the uninomial value for this SvcLSIDRecord.
     * 
     * @return uninomial
     */
    public java.lang.String getUninomial() {
        return uninomial;
    }


    /**
     * Sets the uninomial value for this SvcLSIDRecord.
     * 
     * @param uninomial
     */
    public void setUninomial(java.lang.String uninomial) {
        this.uninomial = uninomial;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcLSIDRecord)) return false;
        SvcLSIDRecord other = (SvcLSIDRecord) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.authorship==null && other.getAuthorship()==null) || 
             (this.authorship!=null &&
              this.authorship.equals(other.getAuthorship()))) &&
            ((this.genusPart==null && other.getGenusPart()==null) || 
             (this.genusPart!=null &&
              this.genusPart.equals(other.getGenusPart()))) &&
            ((this.infragenericEpithet==null && other.getInfragenericEpithet()==null) || 
             (this.infragenericEpithet!=null &&
              this.infragenericEpithet.equals(other.getInfragenericEpithet()))) &&
            ((this.infraspecificEpithet==null && other.getInfraspecificEpithet()==null) || 
             (this.infraspecificEpithet!=null &&
              this.infraspecificEpithet.equals(other.getInfraspecificEpithet()))) &&
            ((this.lsid==null && other.getLsid()==null) || 
             (this.lsid!=null &&
              this.lsid.equals(other.getLsid()))) &&
            ((this.nameComplete==null && other.getNameComplete()==null) || 
             (this.nameComplete!=null &&
              this.nameComplete.equals(other.getNameComplete()))) &&
            ((this.nomenclaturalCode==null && other.getNomenclaturalCode()==null) || 
             (this.nomenclaturalCode!=null &&
              this.nomenclaturalCode.equals(other.getNomenclaturalCode()))) &&
            ((this.rank==null && other.getRank()==null) || 
             (this.rank!=null &&
              this.rank.equals(other.getRank()))) &&
            ((this.rankString==null && other.getRankString()==null) || 
             (this.rankString!=null &&
              this.rankString.equals(other.getRankString()))) &&
            ((this.specificEpithet==null && other.getSpecificEpithet()==null) || 
             (this.specificEpithet!=null &&
              this.specificEpithet.equals(other.getSpecificEpithet()))) &&
            ((this.uninomial==null && other.getUninomial()==null) || 
             (this.uninomial!=null &&
              this.uninomial.equals(other.getUninomial())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAuthorship() != null) {
            _hashCode += getAuthorship().hashCode();
        }
        if (getGenusPart() != null) {
            _hashCode += getGenusPart().hashCode();
        }
        if (getInfragenericEpithet() != null) {
            _hashCode += getInfragenericEpithet().hashCode();
        }
        if (getInfraspecificEpithet() != null) {
            _hashCode += getInfraspecificEpithet().hashCode();
        }
        if (getLsid() != null) {
            _hashCode += getLsid().hashCode();
        }
        if (getNameComplete() != null) {
            _hashCode += getNameComplete().hashCode();
        }
        if (getNomenclaturalCode() != null) {
            _hashCode += getNomenclaturalCode().hashCode();
        }
        if (getRank() != null) {
            _hashCode += getRank().hashCode();
        }
        if (getRankString() != null) {
            _hashCode += getRankString().hashCode();
        }
        if (getSpecificEpithet() != null) {
            _hashCode += getSpecificEpithet().hashCode();
        }
        if (getUninomial() != null) {
            _hashCode += getUninomial().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcLSIDRecord.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcLSIDRecord"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorship");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "authorship"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("genusPart");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "genusPart"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infragenericEpithet");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "infragenericEpithet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infraspecificEpithet");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "infraspecificEpithet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lsid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "lsid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameComplete");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "nameComplete"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomenclaturalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "nomenclaturalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rank");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "rank"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankString");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "rankString"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("specificEpithet");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "specificEpithet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uninomial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "uninomial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
