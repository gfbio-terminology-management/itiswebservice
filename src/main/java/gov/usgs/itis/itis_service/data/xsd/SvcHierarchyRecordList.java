/**
 * SvcHierarchyRecordList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcHierarchyRecordList  extends gov.usgs.itis.itis_service.data.xsd.SvcListBase  implements java.io.Serializable {
    private java.lang.String author;

    private gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord[] hierarchyList;

    private java.lang.String rankName;

    private java.lang.String sciName;

    public SvcHierarchyRecordList() {
    }

    public SvcHierarchyRecordList(
           java.lang.String tsn,
           java.lang.String author,
           gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord[] hierarchyList,
           java.lang.String rankName,
           java.lang.String sciName) {
        super(
            tsn);
        this.author = author;
        this.hierarchyList = hierarchyList;
        this.rankName = rankName;
        this.sciName = sciName;
    }


    /**
     * Gets the author value for this SvcHierarchyRecordList.
     * 
     * @return author
     */
    public java.lang.String getAuthor() {
        return author;
    }


    /**
     * Sets the author value for this SvcHierarchyRecordList.
     * 
     * @param author
     */
    public void setAuthor(java.lang.String author) {
        this.author = author;
    }


    /**
     * Gets the hierarchyList value for this SvcHierarchyRecordList.
     * 
     * @return hierarchyList
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord[] getHierarchyList() {
        return hierarchyList;
    }


    /**
     * Sets the hierarchyList value for this SvcHierarchyRecordList.
     * 
     * @param hierarchyList
     */
    public void setHierarchyList(gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord[] hierarchyList) {
        this.hierarchyList = hierarchyList;
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord getHierarchyList(int i) {
        return this.hierarchyList[i];
    }

    public void setHierarchyList(int i, gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord _value) {
        this.hierarchyList[i] = _value;
    }


    /**
     * Gets the rankName value for this SvcHierarchyRecordList.
     * 
     * @return rankName
     */
    public java.lang.String getRankName() {
        return rankName;
    }


    /**
     * Sets the rankName value for this SvcHierarchyRecordList.
     * 
     * @param rankName
     */
    public void setRankName(java.lang.String rankName) {
        this.rankName = rankName;
    }


    /**
     * Gets the sciName value for this SvcHierarchyRecordList.
     * 
     * @return sciName
     */
    public java.lang.String getSciName() {
        return sciName;
    }


    /**
     * Sets the sciName value for this SvcHierarchyRecordList.
     * 
     * @param sciName
     */
    public void setSciName(java.lang.String sciName) {
        this.sciName = sciName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcHierarchyRecordList)) return false;
        SvcHierarchyRecordList other = (SvcHierarchyRecordList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.author==null && other.getAuthor()==null) || 
             (this.author!=null &&
              this.author.equals(other.getAuthor()))) &&
            ((this.hierarchyList==null && other.getHierarchyList()==null) || 
             (this.hierarchyList!=null &&
              java.util.Arrays.equals(this.hierarchyList, other.getHierarchyList()))) &&
            ((this.rankName==null && other.getRankName()==null) || 
             (this.rankName!=null &&
              this.rankName.equals(other.getRankName()))) &&
            ((this.sciName==null && other.getSciName()==null) || 
             (this.sciName!=null &&
              this.sciName.equals(other.getSciName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAuthor() != null) {
            _hashCode += getAuthor().hashCode();
        }
        if (getHierarchyList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getHierarchyList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getHierarchyList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRankName() != null) {
            _hashCode += getRankName().hashCode();
        }
        if (getSciName() != null) {
            _hashCode += getSciName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcHierarchyRecordList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcHierarchyRecordList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("author");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "author"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hierarchyList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "hierarchyList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcHierarchyRecord"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "rankName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sciName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "sciName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
