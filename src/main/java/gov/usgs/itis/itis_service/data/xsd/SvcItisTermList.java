/**
 * SvcItisTermList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcItisTermList  implements java.io.Serializable {
    private gov.usgs.itis.itis_service.data.xsd.SvcItisTerm[] itisTerms;

    private java.lang.String requestedName;

    public SvcItisTermList() {
    }

    public SvcItisTermList(
           gov.usgs.itis.itis_service.data.xsd.SvcItisTerm[] itisTerms,
           java.lang.String requestedName) {
           this.itisTerms = itisTerms;
           this.requestedName = requestedName;
    }


    /**
     * Gets the itisTerms value for this SvcItisTermList.
     * 
     * @return itisTerms
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcItisTerm[] getItisTerms() {
        return itisTerms;
    }


    /**
     * Sets the itisTerms value for this SvcItisTermList.
     * 
     * @param itisTerms
     */
    public void setItisTerms(gov.usgs.itis.itis_service.data.xsd.SvcItisTerm[] itisTerms) {
        this.itisTerms = itisTerms;
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcItisTerm getItisTerms(int i) {
        return this.itisTerms[i];
    }

    public void setItisTerms(int i, gov.usgs.itis.itis_service.data.xsd.SvcItisTerm _value) {
        this.itisTerms[i] = _value;
    }


    /**
     * Gets the requestedName value for this SvcItisTermList.
     * 
     * @return requestedName
     */
    public java.lang.String getRequestedName() {
        return requestedName;
    }


    /**
     * Sets the requestedName value for this SvcItisTermList.
     * 
     * @param requestedName
     */
    public void setRequestedName(java.lang.String requestedName) {
        this.requestedName = requestedName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcItisTermList)) return false;
        SvcItisTermList other = (SvcItisTermList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.itisTerms==null && other.getItisTerms()==null) || 
             (this.itisTerms!=null &&
              java.util.Arrays.equals(this.itisTerms, other.getItisTerms()))) &&
            ((this.requestedName==null && other.getRequestedName()==null) || 
             (this.requestedName!=null &&
              this.requestedName.equals(other.getRequestedName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getItisTerms() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItisTerms());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItisTerms(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRequestedName() != null) {
            _hashCode += getRequestedName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcItisTermList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcItisTermList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itisTerms");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "itisTerms"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcItisTerm"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestedName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "requestedName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
