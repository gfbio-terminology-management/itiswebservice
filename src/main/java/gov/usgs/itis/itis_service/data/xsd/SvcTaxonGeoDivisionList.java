/**
 * SvcTaxonGeoDivisionList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcTaxonGeoDivisionList  extends gov.usgs.itis.itis_service.data.xsd.SvcListBase  implements java.io.Serializable {
    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivision[] geoDivisions;

    public SvcTaxonGeoDivisionList() {
    }

    public SvcTaxonGeoDivisionList(
           java.lang.String tsn,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivision[] geoDivisions) {
        super(
            tsn);
        this.geoDivisions = geoDivisions;
    }


    /**
     * Gets the geoDivisions value for this SvcTaxonGeoDivisionList.
     * 
     * @return geoDivisions
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivision[] getGeoDivisions() {
        return geoDivisions;
    }


    /**
     * Sets the geoDivisions value for this SvcTaxonGeoDivisionList.
     * 
     * @param geoDivisions
     */
    public void setGeoDivisions(gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivision[] geoDivisions) {
        this.geoDivisions = geoDivisions;
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivision getGeoDivisions(int i) {
        return this.geoDivisions[i];
    }

    public void setGeoDivisions(int i, gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivision _value) {
        this.geoDivisions[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcTaxonGeoDivisionList)) return false;
        SvcTaxonGeoDivisionList other = (SvcTaxonGeoDivisionList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.geoDivisions==null && other.getGeoDivisions()==null) || 
             (this.geoDivisions!=null &&
              java.util.Arrays.equals(this.geoDivisions, other.getGeoDivisions())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getGeoDivisions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGeoDivisions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGeoDivisions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcTaxonGeoDivisionList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonGeoDivisionList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("geoDivisions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "geoDivisions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonGeoDivision"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
