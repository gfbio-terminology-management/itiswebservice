/**
 * SvcFullRecord.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcFullRecord  extends gov.usgs.itis.itis_service.data.xsd.SvcTaxonomicBase  implements java.io.Serializable {
    private gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList acceptedNameList;

    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonCommentList commentList;

    private gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList commonNameList;

    private gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness completenessRating;

    private gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata coreMetadata;

    private gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData credibilityRating;

    private gov.usgs.itis.itis_service.data.xsd.SvcCurrencyData currencyRating;

    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonDateData dateData;

    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpertList expertList;

    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivisionList geographicDivisionList;

    private gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord hierarchyUp;

    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOriginList jurisdictionalOriginList;

    private gov.usgs.itis.itis_service.data.xsd.SvcKingdomInfo kingdom;

    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList otherSourceList;

    private gov.usgs.itis.itis_service.data.xsd.SvcParentTsn parentTSN;

    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublicationList publicationList;

    private gov.usgs.itis.itis_service.data.xsd.SvcScientificName scientificName;

    private gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList synonymList;

    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo taxRank;

    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonAuthorship taxonAuthor;

    private gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData unacceptReason;

    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonUsageData usage;

    public SvcFullRecord() {
    }

    public SvcFullRecord(
           java.lang.String tsn,
           gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList acceptedNameList,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonCommentList commentList,
           gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList commonNameList,
           gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness completenessRating,
           gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata coreMetadata,
           gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData credibilityRating,
           gov.usgs.itis.itis_service.data.xsd.SvcCurrencyData currencyRating,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonDateData dateData,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpertList expertList,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivisionList geographicDivisionList,
           gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord hierarchyUp,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOriginList jurisdictionalOriginList,
           gov.usgs.itis.itis_service.data.xsd.SvcKingdomInfo kingdom,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList otherSourceList,
           gov.usgs.itis.itis_service.data.xsd.SvcParentTsn parentTSN,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublicationList publicationList,
           gov.usgs.itis.itis_service.data.xsd.SvcScientificName scientificName,
           gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList synonymList,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo taxRank,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonAuthorship taxonAuthor,
           gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData unacceptReason,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonUsageData usage) {
        super(
            tsn);
        this.acceptedNameList = acceptedNameList;
        this.commentList = commentList;
        this.commonNameList = commonNameList;
        this.completenessRating = completenessRating;
        this.coreMetadata = coreMetadata;
        this.credibilityRating = credibilityRating;
        this.currencyRating = currencyRating;
        this.dateData = dateData;
        this.expertList = expertList;
        this.geographicDivisionList = geographicDivisionList;
        this.hierarchyUp = hierarchyUp;
        this.jurisdictionalOriginList = jurisdictionalOriginList;
        this.kingdom = kingdom;
        this.otherSourceList = otherSourceList;
        this.parentTSN = parentTSN;
        this.publicationList = publicationList;
        this.scientificName = scientificName;
        this.synonymList = synonymList;
        this.taxRank = taxRank;
        this.taxonAuthor = taxonAuthor;
        this.unacceptReason = unacceptReason;
        this.usage = usage;
    }


    /**
     * Gets the acceptedNameList value for this SvcFullRecord.
     * 
     * @return acceptedNameList
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList getAcceptedNameList() {
        return acceptedNameList;
    }


    /**
     * Sets the acceptedNameList value for this SvcFullRecord.
     * 
     * @param acceptedNameList
     */
    public void setAcceptedNameList(gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList acceptedNameList) {
        this.acceptedNameList = acceptedNameList;
    }


    /**
     * Gets the commentList value for this SvcFullRecord.
     * 
     * @return commentList
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonCommentList getCommentList() {
        return commentList;
    }


    /**
     * Sets the commentList value for this SvcFullRecord.
     * 
     * @param commentList
     */
    public void setCommentList(gov.usgs.itis.itis_service.data.xsd.SvcTaxonCommentList commentList) {
        this.commentList = commentList;
    }


    /**
     * Gets the commonNameList value for this SvcFullRecord.
     * 
     * @return commonNameList
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList getCommonNameList() {
        return commonNameList;
    }


    /**
     * Sets the commonNameList value for this SvcFullRecord.
     * 
     * @param commonNameList
     */
    public void setCommonNameList(gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList commonNameList) {
        this.commonNameList = commonNameList;
    }


    /**
     * Gets the completenessRating value for this SvcFullRecord.
     * 
     * @return completenessRating
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness getCompletenessRating() {
        return completenessRating;
    }


    /**
     * Sets the completenessRating value for this SvcFullRecord.
     * 
     * @param completenessRating
     */
    public void setCompletenessRating(gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness completenessRating) {
        this.completenessRating = completenessRating;
    }


    /**
     * Gets the coreMetadata value for this SvcFullRecord.
     * 
     * @return coreMetadata
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata getCoreMetadata() {
        return coreMetadata;
    }


    /**
     * Sets the coreMetadata value for this SvcFullRecord.
     * 
     * @param coreMetadata
     */
    public void setCoreMetadata(gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata coreMetadata) {
        this.coreMetadata = coreMetadata;
    }


    /**
     * Gets the credibilityRating value for this SvcFullRecord.
     * 
     * @return credibilityRating
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData getCredibilityRating() {
        return credibilityRating;
    }


    /**
     * Sets the credibilityRating value for this SvcFullRecord.
     * 
     * @param credibilityRating
     */
    public void setCredibilityRating(gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData credibilityRating) {
        this.credibilityRating = credibilityRating;
    }


    /**
     * Gets the currencyRating value for this SvcFullRecord.
     * 
     * @return currencyRating
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcCurrencyData getCurrencyRating() {
        return currencyRating;
    }


    /**
     * Sets the currencyRating value for this SvcFullRecord.
     * 
     * @param currencyRating
     */
    public void setCurrencyRating(gov.usgs.itis.itis_service.data.xsd.SvcCurrencyData currencyRating) {
        this.currencyRating = currencyRating;
    }


    /**
     * Gets the dateData value for this SvcFullRecord.
     * 
     * @return dateData
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonDateData getDateData() {
        return dateData;
    }


    /**
     * Sets the dateData value for this SvcFullRecord.
     * 
     * @param dateData
     */
    public void setDateData(gov.usgs.itis.itis_service.data.xsd.SvcTaxonDateData dateData) {
        this.dateData = dateData;
    }


    /**
     * Gets the expertList value for this SvcFullRecord.
     * 
     * @return expertList
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpertList getExpertList() {
        return expertList;
    }


    /**
     * Sets the expertList value for this SvcFullRecord.
     * 
     * @param expertList
     */
    public void setExpertList(gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpertList expertList) {
        this.expertList = expertList;
    }


    /**
     * Gets the geographicDivisionList value for this SvcFullRecord.
     * 
     * @return geographicDivisionList
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivisionList getGeographicDivisionList() {
        return geographicDivisionList;
    }


    /**
     * Sets the geographicDivisionList value for this SvcFullRecord.
     * 
     * @param geographicDivisionList
     */
    public void setGeographicDivisionList(gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivisionList geographicDivisionList) {
        this.geographicDivisionList = geographicDivisionList;
    }


    /**
     * Gets the hierarchyUp value for this SvcFullRecord.
     * 
     * @return hierarchyUp
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord getHierarchyUp() {
        return hierarchyUp;
    }


    /**
     * Sets the hierarchyUp value for this SvcFullRecord.
     * 
     * @param hierarchyUp
     */
    public void setHierarchyUp(gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord hierarchyUp) {
        this.hierarchyUp = hierarchyUp;
    }


    /**
     * Gets the jurisdictionalOriginList value for this SvcFullRecord.
     * 
     * @return jurisdictionalOriginList
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOriginList getJurisdictionalOriginList() {
        return jurisdictionalOriginList;
    }


    /**
     * Sets the jurisdictionalOriginList value for this SvcFullRecord.
     * 
     * @param jurisdictionalOriginList
     */
    public void setJurisdictionalOriginList(gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOriginList jurisdictionalOriginList) {
        this.jurisdictionalOriginList = jurisdictionalOriginList;
    }


    /**
     * Gets the kingdom value for this SvcFullRecord.
     * 
     * @return kingdom
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcKingdomInfo getKingdom() {
        return kingdom;
    }


    /**
     * Sets the kingdom value for this SvcFullRecord.
     * 
     * @param kingdom
     */
    public void setKingdom(gov.usgs.itis.itis_service.data.xsd.SvcKingdomInfo kingdom) {
        this.kingdom = kingdom;
    }


    /**
     * Gets the otherSourceList value for this SvcFullRecord.
     * 
     * @return otherSourceList
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList getOtherSourceList() {
        return otherSourceList;
    }


    /**
     * Sets the otherSourceList value for this SvcFullRecord.
     * 
     * @param otherSourceList
     */
    public void setOtherSourceList(gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList otherSourceList) {
        this.otherSourceList = otherSourceList;
    }


    /**
     * Gets the parentTSN value for this SvcFullRecord.
     * 
     * @return parentTSN
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcParentTsn getParentTSN() {
        return parentTSN;
    }


    /**
     * Sets the parentTSN value for this SvcFullRecord.
     * 
     * @param parentTSN
     */
    public void setParentTSN(gov.usgs.itis.itis_service.data.xsd.SvcParentTsn parentTSN) {
        this.parentTSN = parentTSN;
    }


    /**
     * Gets the publicationList value for this SvcFullRecord.
     * 
     * @return publicationList
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublicationList getPublicationList() {
        return publicationList;
    }


    /**
     * Sets the publicationList value for this SvcFullRecord.
     * 
     * @param publicationList
     */
    public void setPublicationList(gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublicationList publicationList) {
        this.publicationList = publicationList;
    }


    /**
     * Gets the scientificName value for this SvcFullRecord.
     * 
     * @return scientificName
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName getScientificName() {
        return scientificName;
    }


    /**
     * Sets the scientificName value for this SvcFullRecord.
     * 
     * @param scientificName
     */
    public void setScientificName(gov.usgs.itis.itis_service.data.xsd.SvcScientificName scientificName) {
        this.scientificName = scientificName;
    }


    /**
     * Gets the synonymList value for this SvcFullRecord.
     * 
     * @return synonymList
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList getSynonymList() {
        return synonymList;
    }


    /**
     * Sets the synonymList value for this SvcFullRecord.
     * 
     * @param synonymList
     */
    public void setSynonymList(gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList synonymList) {
        this.synonymList = synonymList;
    }


    /**
     * Gets the taxRank value for this SvcFullRecord.
     * 
     * @return taxRank
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo getTaxRank() {
        return taxRank;
    }


    /**
     * Sets the taxRank value for this SvcFullRecord.
     * 
     * @param taxRank
     */
    public void setTaxRank(gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo taxRank) {
        this.taxRank = taxRank;
    }


    /**
     * Gets the taxonAuthor value for this SvcFullRecord.
     * 
     * @return taxonAuthor
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonAuthorship getTaxonAuthor() {
        return taxonAuthor;
    }


    /**
     * Sets the taxonAuthor value for this SvcFullRecord.
     * 
     * @param taxonAuthor
     */
    public void setTaxonAuthor(gov.usgs.itis.itis_service.data.xsd.SvcTaxonAuthorship taxonAuthor) {
        this.taxonAuthor = taxonAuthor;
    }


    /**
     * Gets the unacceptReason value for this SvcFullRecord.
     * 
     * @return unacceptReason
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData getUnacceptReason() {
        return unacceptReason;
    }


    /**
     * Sets the unacceptReason value for this SvcFullRecord.
     * 
     * @param unacceptReason
     */
    public void setUnacceptReason(gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData unacceptReason) {
        this.unacceptReason = unacceptReason;
    }


    /**
     * Gets the usage value for this SvcFullRecord.
     * 
     * @return usage
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonUsageData getUsage() {
        return usage;
    }


    /**
     * Sets the usage value for this SvcFullRecord.
     * 
     * @param usage
     */
    public void setUsage(gov.usgs.itis.itis_service.data.xsd.SvcTaxonUsageData usage) {
        this.usage = usage;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcFullRecord)) return false;
        SvcFullRecord other = (SvcFullRecord) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.acceptedNameList==null && other.getAcceptedNameList()==null) || 
             (this.acceptedNameList!=null &&
              this.acceptedNameList.equals(other.getAcceptedNameList()))) &&
            ((this.commentList==null && other.getCommentList()==null) || 
             (this.commentList!=null &&
              this.commentList.equals(other.getCommentList()))) &&
            ((this.commonNameList==null && other.getCommonNameList()==null) || 
             (this.commonNameList!=null &&
              this.commonNameList.equals(other.getCommonNameList()))) &&
            ((this.completenessRating==null && other.getCompletenessRating()==null) || 
             (this.completenessRating!=null &&
              this.completenessRating.equals(other.getCompletenessRating()))) &&
            ((this.coreMetadata==null && other.getCoreMetadata()==null) || 
             (this.coreMetadata!=null &&
              this.coreMetadata.equals(other.getCoreMetadata()))) &&
            ((this.credibilityRating==null && other.getCredibilityRating()==null) || 
             (this.credibilityRating!=null &&
              this.credibilityRating.equals(other.getCredibilityRating()))) &&
            ((this.currencyRating==null && other.getCurrencyRating()==null) || 
             (this.currencyRating!=null &&
              this.currencyRating.equals(other.getCurrencyRating()))) &&
            ((this.dateData==null && other.getDateData()==null) || 
             (this.dateData!=null &&
              this.dateData.equals(other.getDateData()))) &&
            ((this.expertList==null && other.getExpertList()==null) || 
             (this.expertList!=null &&
              this.expertList.equals(other.getExpertList()))) &&
            ((this.geographicDivisionList==null && other.getGeographicDivisionList()==null) || 
             (this.geographicDivisionList!=null &&
              this.geographicDivisionList.equals(other.getGeographicDivisionList()))) &&
            ((this.hierarchyUp==null && other.getHierarchyUp()==null) || 
             (this.hierarchyUp!=null &&
              this.hierarchyUp.equals(other.getHierarchyUp()))) &&
            ((this.jurisdictionalOriginList==null && other.getJurisdictionalOriginList()==null) || 
             (this.jurisdictionalOriginList!=null &&
              this.jurisdictionalOriginList.equals(other.getJurisdictionalOriginList()))) &&
            ((this.kingdom==null && other.getKingdom()==null) || 
             (this.kingdom!=null &&
              this.kingdom.equals(other.getKingdom()))) &&
            ((this.otherSourceList==null && other.getOtherSourceList()==null) || 
             (this.otherSourceList!=null &&
              this.otherSourceList.equals(other.getOtherSourceList()))) &&
            ((this.parentTSN==null && other.getParentTSN()==null) || 
             (this.parentTSN!=null &&
              this.parentTSN.equals(other.getParentTSN()))) &&
            ((this.publicationList==null && other.getPublicationList()==null) || 
             (this.publicationList!=null &&
              this.publicationList.equals(other.getPublicationList()))) &&
            ((this.scientificName==null && other.getScientificName()==null) || 
             (this.scientificName!=null &&
              this.scientificName.equals(other.getScientificName()))) &&
            ((this.synonymList==null && other.getSynonymList()==null) || 
             (this.synonymList!=null &&
              this.synonymList.equals(other.getSynonymList()))) &&
            ((this.taxRank==null && other.getTaxRank()==null) || 
             (this.taxRank!=null &&
              this.taxRank.equals(other.getTaxRank()))) &&
            ((this.taxonAuthor==null && other.getTaxonAuthor()==null) || 
             (this.taxonAuthor!=null &&
              this.taxonAuthor.equals(other.getTaxonAuthor()))) &&
            ((this.unacceptReason==null && other.getUnacceptReason()==null) || 
             (this.unacceptReason!=null &&
              this.unacceptReason.equals(other.getUnacceptReason()))) &&
            ((this.usage==null && other.getUsage()==null) || 
             (this.usage!=null &&
              this.usage.equals(other.getUsage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAcceptedNameList() != null) {
            _hashCode += getAcceptedNameList().hashCode();
        }
        if (getCommentList() != null) {
            _hashCode += getCommentList().hashCode();
        }
        if (getCommonNameList() != null) {
            _hashCode += getCommonNameList().hashCode();
        }
        if (getCompletenessRating() != null) {
            _hashCode += getCompletenessRating().hashCode();
        }
        if (getCoreMetadata() != null) {
            _hashCode += getCoreMetadata().hashCode();
        }
        if (getCredibilityRating() != null) {
            _hashCode += getCredibilityRating().hashCode();
        }
        if (getCurrencyRating() != null) {
            _hashCode += getCurrencyRating().hashCode();
        }
        if (getDateData() != null) {
            _hashCode += getDateData().hashCode();
        }
        if (getExpertList() != null) {
            _hashCode += getExpertList().hashCode();
        }
        if (getGeographicDivisionList() != null) {
            _hashCode += getGeographicDivisionList().hashCode();
        }
        if (getHierarchyUp() != null) {
            _hashCode += getHierarchyUp().hashCode();
        }
        if (getJurisdictionalOriginList() != null) {
            _hashCode += getJurisdictionalOriginList().hashCode();
        }
        if (getKingdom() != null) {
            _hashCode += getKingdom().hashCode();
        }
        if (getOtherSourceList() != null) {
            _hashCode += getOtherSourceList().hashCode();
        }
        if (getParentTSN() != null) {
            _hashCode += getParentTSN().hashCode();
        }
        if (getPublicationList() != null) {
            _hashCode += getPublicationList().hashCode();
        }
        if (getScientificName() != null) {
            _hashCode += getScientificName().hashCode();
        }
        if (getSynonymList() != null) {
            _hashCode += getSynonymList().hashCode();
        }
        if (getTaxRank() != null) {
            _hashCode += getTaxRank().hashCode();
        }
        if (getTaxonAuthor() != null) {
            _hashCode += getTaxonAuthor().hashCode();
        }
        if (getUnacceptReason() != null) {
            _hashCode += getUnacceptReason().hashCode();
        }
        if (getUsage() != null) {
            _hashCode += getUsage().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcFullRecord.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcFullRecord"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acceptedNameList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "acceptedNameList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcAcceptedNameList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commentList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "commentList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonCommentList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commonNameList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "commonNameList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCommonNameList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("completenessRating");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "completenessRating"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcGlobalSpeciesCompleteness"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coreMetadata");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "coreMetadata"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCoreMetadata"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("credibilityRating");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "credibilityRating"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCredibilityData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyRating");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "currencyRating"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCurrencyData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateData");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "dateData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonDateData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expertList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "expertList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonExpertList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("geographicDivisionList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "geographicDivisionList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonGeoDivisionList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hierarchyUp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "hierarchyUp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcHierarchyRecord"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jurisdictionalOriginList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "jurisdictionalOriginList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonJurisdictionalOriginList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kingdom");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "kingdom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcKingdomInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherSourceList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "otherSourceList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonOtherSourceList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentTSN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "parentTSN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcParentTsn"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("publicationList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "publicationList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonPublicationList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scientificName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "scientificName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcScientificName"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("synonymList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "synonymList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcSynonymNameList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxRank");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "taxRank"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonRankInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxonAuthor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "taxonAuthor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonAuthorship"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unacceptReason");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "unacceptReason"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcUnacceptData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "usage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonUsageData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
