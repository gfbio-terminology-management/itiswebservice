/**
 * SvcHierarchyRecord.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcHierarchyRecord  extends gov.usgs.itis.itis_service.data.xsd.SvcTaxonomicBase  implements java.io.Serializable {
    private java.lang.String author;

    private java.lang.String parentName;

    private java.lang.String parentTsn;

    private java.lang.String rankName;

    private java.lang.String taxonName;

    public SvcHierarchyRecord() {
    }

    public SvcHierarchyRecord(
           java.lang.String tsn,
           java.lang.String author,
           java.lang.String parentName,
           java.lang.String parentTsn,
           java.lang.String rankName,
           java.lang.String taxonName) {
        super(
            tsn);
        this.author = author;
        this.parentName = parentName;
        this.parentTsn = parentTsn;
        this.rankName = rankName;
        this.taxonName = taxonName;
    }


    /**
     * Gets the author value for this SvcHierarchyRecord.
     * 
     * @return author
     */
    public java.lang.String getAuthor() {
        return author;
    }


    /**
     * Sets the author value for this SvcHierarchyRecord.
     * 
     * @param author
     */
    public void setAuthor(java.lang.String author) {
        this.author = author;
    }


    /**
     * Gets the parentName value for this SvcHierarchyRecord.
     * 
     * @return parentName
     */
    public java.lang.String getParentName() {
        return parentName;
    }


    /**
     * Sets the parentName value for this SvcHierarchyRecord.
     * 
     * @param parentName
     */
    public void setParentName(java.lang.String parentName) {
        this.parentName = parentName;
    }


    /**
     * Gets the parentTsn value for this SvcHierarchyRecord.
     * 
     * @return parentTsn
     */
    public java.lang.String getParentTsn() {
        return parentTsn;
    }


    /**
     * Sets the parentTsn value for this SvcHierarchyRecord.
     * 
     * @param parentTsn
     */
    public void setParentTsn(java.lang.String parentTsn) {
        this.parentTsn = parentTsn;
    }


    /**
     * Gets the rankName value for this SvcHierarchyRecord.
     * 
     * @return rankName
     */
    public java.lang.String getRankName() {
        return rankName;
    }


    /**
     * Sets the rankName value for this SvcHierarchyRecord.
     * 
     * @param rankName
     */
    public void setRankName(java.lang.String rankName) {
        this.rankName = rankName;
    }


    /**
     * Gets the taxonName value for this SvcHierarchyRecord.
     * 
     * @return taxonName
     */
    public java.lang.String getTaxonName() {
        return taxonName;
    }


    /**
     * Sets the taxonName value for this SvcHierarchyRecord.
     * 
     * @param taxonName
     */
    public void setTaxonName(java.lang.String taxonName) {
        this.taxonName = taxonName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcHierarchyRecord)) return false;
        SvcHierarchyRecord other = (SvcHierarchyRecord) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.author==null && other.getAuthor()==null) || 
             (this.author!=null &&
              this.author.equals(other.getAuthor()))) &&
            ((this.parentName==null && other.getParentName()==null) || 
             (this.parentName!=null &&
              this.parentName.equals(other.getParentName()))) &&
            ((this.parentTsn==null && other.getParentTsn()==null) || 
             (this.parentTsn!=null &&
              this.parentTsn.equals(other.getParentTsn()))) &&
            ((this.rankName==null && other.getRankName()==null) || 
             (this.rankName!=null &&
              this.rankName.equals(other.getRankName()))) &&
            ((this.taxonName==null && other.getTaxonName()==null) || 
             (this.taxonName!=null &&
              this.taxonName.equals(other.getTaxonName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAuthor() != null) {
            _hashCode += getAuthor().hashCode();
        }
        if (getParentName() != null) {
            _hashCode += getParentName().hashCode();
        }
        if (getParentTsn() != null) {
            _hashCode += getParentTsn().hashCode();
        }
        if (getRankName() != null) {
            _hashCode += getRankName().hashCode();
        }
        if (getTaxonName() != null) {
            _hashCode += getTaxonName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcHierarchyRecord.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcHierarchyRecord"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("author");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "author"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "parentName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentTsn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "parentTsn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "rankName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxonName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "taxonName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
