/**
 * SvcItisTerm.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcItisTerm  extends gov.usgs.itis.itis_service.data.xsd.SvcTaxonomicBase  implements java.io.Serializable {
    private java.lang.String author;

    private java.lang.String[] commonNames;

    private java.lang.String nameUsage;

    private java.lang.String scientificName;

    public SvcItisTerm() {
    }

    public SvcItisTerm(
           java.lang.String tsn,
           java.lang.String author,
           java.lang.String[] commonNames,
           java.lang.String nameUsage,
           java.lang.String scientificName) {
        super(
            tsn);
        this.author = author;
        this.commonNames = commonNames;
        this.nameUsage = nameUsage;
        this.scientificName = scientificName;
    }


    /**
     * Gets the author value for this SvcItisTerm.
     * 
     * @return author
     */
    public java.lang.String getAuthor() {
        return author;
    }


    /**
     * Sets the author value for this SvcItisTerm.
     * 
     * @param author
     */
    public void setAuthor(java.lang.String author) {
        this.author = author;
    }


    /**
     * Gets the commonNames value for this SvcItisTerm.
     * 
     * @return commonNames
     */
    public java.lang.String[] getCommonNames() {
        return commonNames;
    }


    /**
     * Sets the commonNames value for this SvcItisTerm.
     * 
     * @param commonNames
     */
    public void setCommonNames(java.lang.String[] commonNames) {
        this.commonNames = commonNames;
    }

    public java.lang.String getCommonNames(int i) {
        return this.commonNames[i];
    }

    public void setCommonNames(int i, java.lang.String _value) {
        this.commonNames[i] = _value;
    }


    /**
     * Gets the nameUsage value for this SvcItisTerm.
     * 
     * @return nameUsage
     */
    public java.lang.String getNameUsage() {
        return nameUsage;
    }


    /**
     * Sets the nameUsage value for this SvcItisTerm.
     * 
     * @param nameUsage
     */
    public void setNameUsage(java.lang.String nameUsage) {
        this.nameUsage = nameUsage;
    }


    /**
     * Gets the scientificName value for this SvcItisTerm.
     * 
     * @return scientificName
     */
    public java.lang.String getScientificName() {
        return scientificName;
    }


    /**
     * Sets the scientificName value for this SvcItisTerm.
     * 
     * @param scientificName
     */
    public void setScientificName(java.lang.String scientificName) {
        this.scientificName = scientificName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcItisTerm)) return false;
        SvcItisTerm other = (SvcItisTerm) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.author==null && other.getAuthor()==null) || 
             (this.author!=null &&
              this.author.equals(other.getAuthor()))) &&
            ((this.commonNames==null && other.getCommonNames()==null) || 
             (this.commonNames!=null &&
              java.util.Arrays.equals(this.commonNames, other.getCommonNames()))) &&
            ((this.nameUsage==null && other.getNameUsage()==null) || 
             (this.nameUsage!=null &&
              this.nameUsage.equals(other.getNameUsage()))) &&
            ((this.scientificName==null && other.getScientificName()==null) || 
             (this.scientificName!=null &&
              this.scientificName.equals(other.getScientificName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAuthor() != null) {
            _hashCode += getAuthor().hashCode();
        }
        if (getCommonNames() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCommonNames());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCommonNames(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNameUsage() != null) {
            _hashCode += getNameUsage().hashCode();
        }
        if (getScientificName() != null) {
            _hashCode += getScientificName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcItisTerm.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcItisTerm"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("author");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "author"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commonNames");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "commonNames"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameUsage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "nameUsage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scientificName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "scientificName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
