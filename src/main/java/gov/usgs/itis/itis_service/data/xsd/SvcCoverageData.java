/**
 * SvcCoverageData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcCoverageData  extends gov.usgs.itis.itis_service.data.xsd.SvcTaxonomicBase  implements java.io.Serializable {
    private java.lang.Integer rankId;

    private java.lang.String taxonCoverage;

    public SvcCoverageData() {
    }

    public SvcCoverageData(
           java.lang.String tsn,
           java.lang.Integer rankId,
           java.lang.String taxonCoverage) {
        super(
            tsn);
        this.rankId = rankId;
        this.taxonCoverage = taxonCoverage;
    }


    /**
     * Gets the rankId value for this SvcCoverageData.
     * 
     * @return rankId
     */
    public java.lang.Integer getRankId() {
        return rankId;
    }


    /**
     * Sets the rankId value for this SvcCoverageData.
     * 
     * @param rankId
     */
    public void setRankId(java.lang.Integer rankId) {
        this.rankId = rankId;
    }


    /**
     * Gets the taxonCoverage value for this SvcCoverageData.
     * 
     * @return taxonCoverage
     */
    public java.lang.String getTaxonCoverage() {
        return taxonCoverage;
    }


    /**
     * Sets the taxonCoverage value for this SvcCoverageData.
     * 
     * @param taxonCoverage
     */
    public void setTaxonCoverage(java.lang.String taxonCoverage) {
        this.taxonCoverage = taxonCoverage;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcCoverageData)) return false;
        SvcCoverageData other = (SvcCoverageData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.rankId==null && other.getRankId()==null) || 
             (this.rankId!=null &&
              this.rankId.equals(other.getRankId()))) &&
            ((this.taxonCoverage==null && other.getTaxonCoverage()==null) || 
             (this.taxonCoverage!=null &&
              this.taxonCoverage.equals(other.getTaxonCoverage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRankId() != null) {
            _hashCode += getRankId().hashCode();
        }
        if (getTaxonCoverage() != null) {
            _hashCode += getTaxonCoverage().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcCoverageData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCoverageData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "rankId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxonCoverage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "taxonCoverage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
