/**
 * SvcReviewYear.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcReviewYear  extends gov.usgs.itis.itis_service.data.xsd.SvcTaxonomicBase  implements java.io.Serializable {
    private java.lang.Integer rankId;

    private java.lang.String reviewYear;

    public SvcReviewYear() {
    }

    public SvcReviewYear(
           java.lang.String tsn,
           java.lang.Integer rankId,
           java.lang.String reviewYear) {
        super(
            tsn);
        this.rankId = rankId;
        this.reviewYear = reviewYear;
    }


    /**
     * Gets the rankId value for this SvcReviewYear.
     * 
     * @return rankId
     */
    public java.lang.Integer getRankId() {
        return rankId;
    }


    /**
     * Sets the rankId value for this SvcReviewYear.
     * 
     * @param rankId
     */
    public void setRankId(java.lang.Integer rankId) {
        this.rankId = rankId;
    }


    /**
     * Gets the reviewYear value for this SvcReviewYear.
     * 
     * @return reviewYear
     */
    public java.lang.String getReviewYear() {
        return reviewYear;
    }


    /**
     * Sets the reviewYear value for this SvcReviewYear.
     * 
     * @param reviewYear
     */
    public void setReviewYear(java.lang.String reviewYear) {
        this.reviewYear = reviewYear;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcReviewYear)) return false;
        SvcReviewYear other = (SvcReviewYear) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.rankId==null && other.getRankId()==null) || 
             (this.rankId!=null &&
              this.rankId.equals(other.getRankId()))) &&
            ((this.reviewYear==null && other.getReviewYear()==null) || 
             (this.reviewYear!=null &&
              this.reviewYear.equals(other.getReviewYear())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRankId() != null) {
            _hashCode += getRankId().hashCode();
        }
        if (getReviewYear() != null) {
            _hashCode += getReviewYear().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcReviewYear.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcReviewYear"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "rankId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reviewYear");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "reviewYear"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
