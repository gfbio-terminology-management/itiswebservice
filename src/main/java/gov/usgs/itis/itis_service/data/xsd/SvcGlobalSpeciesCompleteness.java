/**
 * SvcGlobalSpeciesCompleteness.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcGlobalSpeciesCompleteness  extends gov.usgs.itis.itis_service.data.xsd.SvcTaxonomicBase  implements java.io.Serializable {
    private java.lang.String completeness;

    private java.lang.Integer rankId;

    public SvcGlobalSpeciesCompleteness() {
    }

    public SvcGlobalSpeciesCompleteness(
           java.lang.String tsn,
           java.lang.String completeness,
           java.lang.Integer rankId) {
        super(
            tsn);
        this.completeness = completeness;
        this.rankId = rankId;
    }


    /**
     * Gets the completeness value for this SvcGlobalSpeciesCompleteness.
     * 
     * @return completeness
     */
    public java.lang.String getCompleteness() {
        return completeness;
    }


    /**
     * Sets the completeness value for this SvcGlobalSpeciesCompleteness.
     * 
     * @param completeness
     */
    public void setCompleteness(java.lang.String completeness) {
        this.completeness = completeness;
    }


    /**
     * Gets the rankId value for this SvcGlobalSpeciesCompleteness.
     * 
     * @return rankId
     */
    public java.lang.Integer getRankId() {
        return rankId;
    }


    /**
     * Sets the rankId value for this SvcGlobalSpeciesCompleteness.
     * 
     * @param rankId
     */
    public void setRankId(java.lang.Integer rankId) {
        this.rankId = rankId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcGlobalSpeciesCompleteness)) return false;
        SvcGlobalSpeciesCompleteness other = (SvcGlobalSpeciesCompleteness) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.completeness==null && other.getCompleteness()==null) || 
             (this.completeness!=null &&
              this.completeness.equals(other.getCompleteness()))) &&
            ((this.rankId==null && other.getRankId()==null) || 
             (this.rankId!=null &&
              this.rankId.equals(other.getRankId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCompleteness() != null) {
            _hashCode += getCompleteness().hashCode();
        }
        if (getRankId() != null) {
            _hashCode += getRankId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcGlobalSpeciesCompleteness.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcGlobalSpeciesCompleteness"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("completeness");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "completeness"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "rankId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
