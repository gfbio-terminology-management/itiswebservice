/**
 * SvcAcceptedName.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcAcceptedName  implements java.io.Serializable {
    private java.lang.String acceptedName;

    private java.lang.String acceptedTsn;

    private java.lang.String author;

    public SvcAcceptedName() {
    }

    public SvcAcceptedName(
           java.lang.String acceptedName,
           java.lang.String acceptedTsn,
           java.lang.String author) {
           this.acceptedName = acceptedName;
           this.acceptedTsn = acceptedTsn;
           this.author = author;
    }


    /**
     * Gets the acceptedName value for this SvcAcceptedName.
     * 
     * @return acceptedName
     */
    public java.lang.String getAcceptedName() {
        return acceptedName;
    }


    /**
     * Sets the acceptedName value for this SvcAcceptedName.
     * 
     * @param acceptedName
     */
    public void setAcceptedName(java.lang.String acceptedName) {
        this.acceptedName = acceptedName;
    }


    /**
     * Gets the acceptedTsn value for this SvcAcceptedName.
     * 
     * @return acceptedTsn
     */
    public java.lang.String getAcceptedTsn() {
        return acceptedTsn;
    }


    /**
     * Sets the acceptedTsn value for this SvcAcceptedName.
     * 
     * @param acceptedTsn
     */
    public void setAcceptedTsn(java.lang.String acceptedTsn) {
        this.acceptedTsn = acceptedTsn;
    }


    /**
     * Gets the author value for this SvcAcceptedName.
     * 
     * @return author
     */
    public java.lang.String getAuthor() {
        return author;
    }


    /**
     * Sets the author value for this SvcAcceptedName.
     * 
     * @param author
     */
    public void setAuthor(java.lang.String author) {
        this.author = author;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcAcceptedName)) return false;
        SvcAcceptedName other = (SvcAcceptedName) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.acceptedName==null && other.getAcceptedName()==null) || 
             (this.acceptedName!=null &&
              this.acceptedName.equals(other.getAcceptedName()))) &&
            ((this.acceptedTsn==null && other.getAcceptedTsn()==null) || 
             (this.acceptedTsn!=null &&
              this.acceptedTsn.equals(other.getAcceptedTsn()))) &&
            ((this.author==null && other.getAuthor()==null) || 
             (this.author!=null &&
              this.author.equals(other.getAuthor())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAcceptedName() != null) {
            _hashCode += getAcceptedName().hashCode();
        }
        if (getAcceptedTsn() != null) {
            _hashCode += getAcceptedTsn().hashCode();
        }
        if (getAuthor() != null) {
            _hashCode += getAuthor().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcAcceptedName.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcAcceptedName"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acceptedName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "acceptedName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acceptedTsn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "acceptedTsn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("author");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "author"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
