/**
 * SvcAnyMatch.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcAnyMatch  implements java.io.Serializable {
    private java.lang.String author;

    private gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList commonNameList;

    private java.lang.String matchType;

    private java.lang.String sciName;

    private java.lang.String tsn;

    public SvcAnyMatch() {
    }

    public SvcAnyMatch(
           java.lang.String author,
           gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList commonNameList,
           java.lang.String matchType,
           java.lang.String sciName,
           java.lang.String tsn) {
           this.author = author;
           this.commonNameList = commonNameList;
           this.matchType = matchType;
           this.sciName = sciName;
           this.tsn = tsn;
    }


    /**
     * Gets the author value for this SvcAnyMatch.
     * 
     * @return author
     */
    public java.lang.String getAuthor() {
        return author;
    }


    /**
     * Sets the author value for this SvcAnyMatch.
     * 
     * @param author
     */
    public void setAuthor(java.lang.String author) {
        this.author = author;
    }


    /**
     * Gets the commonNameList value for this SvcAnyMatch.
     * 
     * @return commonNameList
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList getCommonNameList() {
        return commonNameList;
    }


    /**
     * Sets the commonNameList value for this SvcAnyMatch.
     * 
     * @param commonNameList
     */
    public void setCommonNameList(gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList commonNameList) {
        this.commonNameList = commonNameList;
    }


    /**
     * Gets the matchType value for this SvcAnyMatch.
     * 
     * @return matchType
     */
    public java.lang.String getMatchType() {
        return matchType;
    }


    /**
     * Sets the matchType value for this SvcAnyMatch.
     * 
     * @param matchType
     */
    public void setMatchType(java.lang.String matchType) {
        this.matchType = matchType;
    }


    /**
     * Gets the sciName value for this SvcAnyMatch.
     * 
     * @return sciName
     */
    public java.lang.String getSciName() {
        return sciName;
    }


    /**
     * Sets the sciName value for this SvcAnyMatch.
     * 
     * @param sciName
     */
    public void setSciName(java.lang.String sciName) {
        this.sciName = sciName;
    }


    /**
     * Gets the tsn value for this SvcAnyMatch.
     * 
     * @return tsn
     */
    public java.lang.String getTsn() {
        return tsn;
    }


    /**
     * Sets the tsn value for this SvcAnyMatch.
     * 
     * @param tsn
     */
    public void setTsn(java.lang.String tsn) {
        this.tsn = tsn;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcAnyMatch)) return false;
        SvcAnyMatch other = (SvcAnyMatch) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.author==null && other.getAuthor()==null) || 
             (this.author!=null &&
              this.author.equals(other.getAuthor()))) &&
            ((this.commonNameList==null && other.getCommonNameList()==null) || 
             (this.commonNameList!=null &&
              this.commonNameList.equals(other.getCommonNameList()))) &&
            ((this.matchType==null && other.getMatchType()==null) || 
             (this.matchType!=null &&
              this.matchType.equals(other.getMatchType()))) &&
            ((this.sciName==null && other.getSciName()==null) || 
             (this.sciName!=null &&
              this.sciName.equals(other.getSciName()))) &&
            ((this.tsn==null && other.getTsn()==null) || 
             (this.tsn!=null &&
              this.tsn.equals(other.getTsn())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAuthor() != null) {
            _hashCode += getAuthor().hashCode();
        }
        if (getCommonNameList() != null) {
            _hashCode += getCommonNameList().hashCode();
        }
        if (getMatchType() != null) {
            _hashCode += getMatchType().hashCode();
        }
        if (getSciName() != null) {
            _hashCode += getSciName().hashCode();
        }
        if (getTsn() != null) {
            _hashCode += getTsn().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcAnyMatch.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcAnyMatch"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("author");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "author"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commonNameList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "commonNameList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCommonNameList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matchType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "matchType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sciName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "sciName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tsn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "tsn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
