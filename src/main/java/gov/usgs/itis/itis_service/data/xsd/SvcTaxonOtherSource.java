/**
 * SvcTaxonOtherSource.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcTaxonOtherSource  implements java.io.Serializable {
    private java.lang.String acquisitionDate;

    private gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement[] referenceFor;

    private java.lang.String source;

    private java.lang.String sourceComment;

    private java.lang.String sourceType;

    private java.lang.String updateDate;

    private java.lang.String version;

    public SvcTaxonOtherSource() {
    }

    public SvcTaxonOtherSource(
           java.lang.String acquisitionDate,
           gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement[] referenceFor,
           java.lang.String source,
           java.lang.String sourceComment,
           java.lang.String sourceType,
           java.lang.String updateDate,
           java.lang.String version) {
           this.acquisitionDate = acquisitionDate;
           this.referenceFor = referenceFor;
           this.source = source;
           this.sourceComment = sourceComment;
           this.sourceType = sourceType;
           this.updateDate = updateDate;
           this.version = version;
    }


    /**
     * Gets the acquisitionDate value for this SvcTaxonOtherSource.
     * 
     * @return acquisitionDate
     */
    public java.lang.String getAcquisitionDate() {
        return acquisitionDate;
    }


    /**
     * Sets the acquisitionDate value for this SvcTaxonOtherSource.
     * 
     * @param acquisitionDate
     */
    public void setAcquisitionDate(java.lang.String acquisitionDate) {
        this.acquisitionDate = acquisitionDate;
    }


    /**
     * Gets the referenceFor value for this SvcTaxonOtherSource.
     * 
     * @return referenceFor
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement[] getReferenceFor() {
        return referenceFor;
    }


    /**
     * Sets the referenceFor value for this SvcTaxonOtherSource.
     * 
     * @param referenceFor
     */
    public void setReferenceFor(gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement[] referenceFor) {
        this.referenceFor = referenceFor;
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement getReferenceFor(int i) {
        return this.referenceFor[i];
    }

    public void setReferenceFor(int i, gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement _value) {
        this.referenceFor[i] = _value;
    }


    /**
     * Gets the source value for this SvcTaxonOtherSource.
     * 
     * @return source
     */
    public java.lang.String getSource() {
        return source;
    }


    /**
     * Sets the source value for this SvcTaxonOtherSource.
     * 
     * @param source
     */
    public void setSource(java.lang.String source) {
        this.source = source;
    }


    /**
     * Gets the sourceComment value for this SvcTaxonOtherSource.
     * 
     * @return sourceComment
     */
    public java.lang.String getSourceComment() {
        return sourceComment;
    }


    /**
     * Sets the sourceComment value for this SvcTaxonOtherSource.
     * 
     * @param sourceComment
     */
    public void setSourceComment(java.lang.String sourceComment) {
        this.sourceComment = sourceComment;
    }


    /**
     * Gets the sourceType value for this SvcTaxonOtherSource.
     * 
     * @return sourceType
     */
    public java.lang.String getSourceType() {
        return sourceType;
    }


    /**
     * Sets the sourceType value for this SvcTaxonOtherSource.
     * 
     * @param sourceType
     */
    public void setSourceType(java.lang.String sourceType) {
        this.sourceType = sourceType;
    }


    /**
     * Gets the updateDate value for this SvcTaxonOtherSource.
     * 
     * @return updateDate
     */
    public java.lang.String getUpdateDate() {
        return updateDate;
    }


    /**
     * Sets the updateDate value for this SvcTaxonOtherSource.
     * 
     * @param updateDate
     */
    public void setUpdateDate(java.lang.String updateDate) {
        this.updateDate = updateDate;
    }


    /**
     * Gets the version value for this SvcTaxonOtherSource.
     * 
     * @return version
     */
    public java.lang.String getVersion() {
        return version;
    }


    /**
     * Sets the version value for this SvcTaxonOtherSource.
     * 
     * @param version
     */
    public void setVersion(java.lang.String version) {
        this.version = version;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcTaxonOtherSource)) return false;
        SvcTaxonOtherSource other = (SvcTaxonOtherSource) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.acquisitionDate==null && other.getAcquisitionDate()==null) || 
             (this.acquisitionDate!=null &&
              this.acquisitionDate.equals(other.getAcquisitionDate()))) &&
            ((this.referenceFor==null && other.getReferenceFor()==null) || 
             (this.referenceFor!=null &&
              java.util.Arrays.equals(this.referenceFor, other.getReferenceFor()))) &&
            ((this.source==null && other.getSource()==null) || 
             (this.source!=null &&
              this.source.equals(other.getSource()))) &&
            ((this.sourceComment==null && other.getSourceComment()==null) || 
             (this.sourceComment!=null &&
              this.sourceComment.equals(other.getSourceComment()))) &&
            ((this.sourceType==null && other.getSourceType()==null) || 
             (this.sourceType!=null &&
              this.sourceType.equals(other.getSourceType()))) &&
            ((this.updateDate==null && other.getUpdateDate()==null) || 
             (this.updateDate!=null &&
              this.updateDate.equals(other.getUpdateDate()))) &&
            ((this.version==null && other.getVersion()==null) || 
             (this.version!=null &&
              this.version.equals(other.getVersion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAcquisitionDate() != null) {
            _hashCode += getAcquisitionDate().hashCode();
        }
        if (getReferenceFor() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getReferenceFor());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getReferenceFor(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSource() != null) {
            _hashCode += getSource().hashCode();
        }
        if (getSourceComment() != null) {
            _hashCode += getSourceComment().hashCode();
        }
        if (getSourceType() != null) {
            _hashCode += getSourceType().hashCode();
        }
        if (getUpdateDate() != null) {
            _hashCode += getUpdateDate().hashCode();
        }
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcTaxonOtherSource.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonOtherSource"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acquisitionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "acquisitionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceFor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "referenceFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcReferenceForElement"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("source");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "source"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceComment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "sourceComment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "sourceType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "updateDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
