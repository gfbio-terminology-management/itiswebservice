/**
 * SvcCoreMetadata.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcCoreMetadata  extends gov.usgs.itis.itis_service.data.xsd.SvcTaxonomicBase  implements java.io.Serializable {
    private java.lang.String credRating;

    private java.lang.Integer rankId;

    private java.lang.String taxonCoverage;

    private java.lang.String taxonCurrency;

    private java.lang.String taxonUsageRating;

    private java.lang.String unacceptReason;

    public SvcCoreMetadata() {
    }

    public SvcCoreMetadata(
           java.lang.String tsn,
           java.lang.String credRating,
           java.lang.Integer rankId,
           java.lang.String taxonCoverage,
           java.lang.String taxonCurrency,
           java.lang.String taxonUsageRating,
           java.lang.String unacceptReason) {
        super(
            tsn);
        this.credRating = credRating;
        this.rankId = rankId;
        this.taxonCoverage = taxonCoverage;
        this.taxonCurrency = taxonCurrency;
        this.taxonUsageRating = taxonUsageRating;
        this.unacceptReason = unacceptReason;
    }


    /**
     * Gets the credRating value for this SvcCoreMetadata.
     * 
     * @return credRating
     */
    public java.lang.String getCredRating() {
        return credRating;
    }


    /**
     * Sets the credRating value for this SvcCoreMetadata.
     * 
     * @param credRating
     */
    public void setCredRating(java.lang.String credRating) {
        this.credRating = credRating;
    }


    /**
     * Gets the rankId value for this SvcCoreMetadata.
     * 
     * @return rankId
     */
    public java.lang.Integer getRankId() {
        return rankId;
    }


    /**
     * Sets the rankId value for this SvcCoreMetadata.
     * 
     * @param rankId
     */
    public void setRankId(java.lang.Integer rankId) {
        this.rankId = rankId;
    }


    /**
     * Gets the taxonCoverage value for this SvcCoreMetadata.
     * 
     * @return taxonCoverage
     */
    public java.lang.String getTaxonCoverage() {
        return taxonCoverage;
    }


    /**
     * Sets the taxonCoverage value for this SvcCoreMetadata.
     * 
     * @param taxonCoverage
     */
    public void setTaxonCoverage(java.lang.String taxonCoverage) {
        this.taxonCoverage = taxonCoverage;
    }


    /**
     * Gets the taxonCurrency value for this SvcCoreMetadata.
     * 
     * @return taxonCurrency
     */
    public java.lang.String getTaxonCurrency() {
        return taxonCurrency;
    }


    /**
     * Sets the taxonCurrency value for this SvcCoreMetadata.
     * 
     * @param taxonCurrency
     */
    public void setTaxonCurrency(java.lang.String taxonCurrency) {
        this.taxonCurrency = taxonCurrency;
    }


    /**
     * Gets the taxonUsageRating value for this SvcCoreMetadata.
     * 
     * @return taxonUsageRating
     */
    public java.lang.String getTaxonUsageRating() {
        return taxonUsageRating;
    }


    /**
     * Sets the taxonUsageRating value for this SvcCoreMetadata.
     * 
     * @param taxonUsageRating
     */
    public void setTaxonUsageRating(java.lang.String taxonUsageRating) {
        this.taxonUsageRating = taxonUsageRating;
    }


    /**
     * Gets the unacceptReason value for this SvcCoreMetadata.
     * 
     * @return unacceptReason
     */
    public java.lang.String getUnacceptReason() {
        return unacceptReason;
    }


    /**
     * Sets the unacceptReason value for this SvcCoreMetadata.
     * 
     * @param unacceptReason
     */
    public void setUnacceptReason(java.lang.String unacceptReason) {
        this.unacceptReason = unacceptReason;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcCoreMetadata)) return false;
        SvcCoreMetadata other = (SvcCoreMetadata) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.credRating==null && other.getCredRating()==null) || 
             (this.credRating!=null &&
              this.credRating.equals(other.getCredRating()))) &&
            ((this.rankId==null && other.getRankId()==null) || 
             (this.rankId!=null &&
              this.rankId.equals(other.getRankId()))) &&
            ((this.taxonCoverage==null && other.getTaxonCoverage()==null) || 
             (this.taxonCoverage!=null &&
              this.taxonCoverage.equals(other.getTaxonCoverage()))) &&
            ((this.taxonCurrency==null && other.getTaxonCurrency()==null) || 
             (this.taxonCurrency!=null &&
              this.taxonCurrency.equals(other.getTaxonCurrency()))) &&
            ((this.taxonUsageRating==null && other.getTaxonUsageRating()==null) || 
             (this.taxonUsageRating!=null &&
              this.taxonUsageRating.equals(other.getTaxonUsageRating()))) &&
            ((this.unacceptReason==null && other.getUnacceptReason()==null) || 
             (this.unacceptReason!=null &&
              this.unacceptReason.equals(other.getUnacceptReason())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCredRating() != null) {
            _hashCode += getCredRating().hashCode();
        }
        if (getRankId() != null) {
            _hashCode += getRankId().hashCode();
        }
        if (getTaxonCoverage() != null) {
            _hashCode += getTaxonCoverage().hashCode();
        }
        if (getTaxonCurrency() != null) {
            _hashCode += getTaxonCurrency().hashCode();
        }
        if (getTaxonUsageRating() != null) {
            _hashCode += getTaxonUsageRating().hashCode();
        }
        if (getUnacceptReason() != null) {
            _hashCode += getUnacceptReason().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcCoreMetadata.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCoreMetadata"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("credRating");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "credRating"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "rankId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxonCoverage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "taxonCoverage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxonCurrency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "taxonCurrency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxonUsageRating");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "taxonUsageRating"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unacceptReason");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "unacceptReason"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
