/**
 * SvcTaxonExpert.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcTaxonExpert  implements java.io.Serializable {
    private java.lang.String comment;

    private java.lang.String expert;

    private gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement[] referenceFor;

    private java.lang.String updateDate;

    public SvcTaxonExpert() {
    }

    public SvcTaxonExpert(
           java.lang.String comment,
           java.lang.String expert,
           gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement[] referenceFor,
           java.lang.String updateDate) {
           this.comment = comment;
           this.expert = expert;
           this.referenceFor = referenceFor;
           this.updateDate = updateDate;
    }


    /**
     * Gets the comment value for this SvcTaxonExpert.
     * 
     * @return comment
     */
    public java.lang.String getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this SvcTaxonExpert.
     * 
     * @param comment
     */
    public void setComment(java.lang.String comment) {
        this.comment = comment;
    }


    /**
     * Gets the expert value for this SvcTaxonExpert.
     * 
     * @return expert
     */
    public java.lang.String getExpert() {
        return expert;
    }


    /**
     * Sets the expert value for this SvcTaxonExpert.
     * 
     * @param expert
     */
    public void setExpert(java.lang.String expert) {
        this.expert = expert;
    }


    /**
     * Gets the referenceFor value for this SvcTaxonExpert.
     * 
     * @return referenceFor
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement[] getReferenceFor() {
        return referenceFor;
    }


    /**
     * Sets the referenceFor value for this SvcTaxonExpert.
     * 
     * @param referenceFor
     */
    public void setReferenceFor(gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement[] referenceFor) {
        this.referenceFor = referenceFor;
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement getReferenceFor(int i) {
        return this.referenceFor[i];
    }

    public void setReferenceFor(int i, gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement _value) {
        this.referenceFor[i] = _value;
    }


    /**
     * Gets the updateDate value for this SvcTaxonExpert.
     * 
     * @return updateDate
     */
    public java.lang.String getUpdateDate() {
        return updateDate;
    }


    /**
     * Sets the updateDate value for this SvcTaxonExpert.
     * 
     * @param updateDate
     */
    public void setUpdateDate(java.lang.String updateDate) {
        this.updateDate = updateDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcTaxonExpert)) return false;
        SvcTaxonExpert other = (SvcTaxonExpert) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment()))) &&
            ((this.expert==null && other.getExpert()==null) || 
             (this.expert!=null &&
              this.expert.equals(other.getExpert()))) &&
            ((this.referenceFor==null && other.getReferenceFor()==null) || 
             (this.referenceFor!=null &&
              java.util.Arrays.equals(this.referenceFor, other.getReferenceFor()))) &&
            ((this.updateDate==null && other.getUpdateDate()==null) || 
             (this.updateDate!=null &&
              this.updateDate.equals(other.getUpdateDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        if (getExpert() != null) {
            _hashCode += getExpert().hashCode();
        }
        if (getReferenceFor() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getReferenceFor());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getReferenceFor(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUpdateDate() != null) {
            _hashCode += getUpdateDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcTaxonExpert.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonExpert"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expert");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "expert"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceFor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "referenceFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcReferenceForElement"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "updateDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
