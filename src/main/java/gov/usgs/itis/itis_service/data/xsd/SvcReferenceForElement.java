/**
 * SvcReferenceForElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcReferenceForElement  implements java.io.Serializable {
    private java.lang.String name;

    private java.lang.String refLanguage;

    private java.lang.String referredTsn;

    public SvcReferenceForElement() {
    }

    public SvcReferenceForElement(
           java.lang.String name,
           java.lang.String refLanguage,
           java.lang.String referredTsn) {
           this.name = name;
           this.refLanguage = refLanguage;
           this.referredTsn = referredTsn;
    }


    /**
     * Gets the name value for this SvcReferenceForElement.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this SvcReferenceForElement.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the refLanguage value for this SvcReferenceForElement.
     * 
     * @return refLanguage
     */
    public java.lang.String getRefLanguage() {
        return refLanguage;
    }


    /**
     * Sets the refLanguage value for this SvcReferenceForElement.
     * 
     * @param refLanguage
     */
    public void setRefLanguage(java.lang.String refLanguage) {
        this.refLanguage = refLanguage;
    }


    /**
     * Gets the referredTsn value for this SvcReferenceForElement.
     * 
     * @return referredTsn
     */
    public java.lang.String getReferredTsn() {
        return referredTsn;
    }


    /**
     * Sets the referredTsn value for this SvcReferenceForElement.
     * 
     * @param referredTsn
     */
    public void setReferredTsn(java.lang.String referredTsn) {
        this.referredTsn = referredTsn;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcReferenceForElement)) return false;
        SvcReferenceForElement other = (SvcReferenceForElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.refLanguage==null && other.getRefLanguage()==null) || 
             (this.refLanguage!=null &&
              this.refLanguage.equals(other.getRefLanguage()))) &&
            ((this.referredTsn==null && other.getReferredTsn()==null) || 
             (this.referredTsn!=null &&
              this.referredTsn.equals(other.getReferredTsn())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getRefLanguage() != null) {
            _hashCode += getRefLanguage().hashCode();
        }
        if (getReferredTsn() != null) {
            _hashCode += getReferredTsn().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcReferenceForElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcReferenceForElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refLanguage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "refLanguage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referredTsn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "referredTsn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
