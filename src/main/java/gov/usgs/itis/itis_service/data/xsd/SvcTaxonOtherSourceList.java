/**
 * SvcTaxonOtherSourceList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcTaxonOtherSourceList  extends gov.usgs.itis.itis_service.data.xsd.SvcListBase  implements java.io.Serializable {
    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSource[] otherSources;

    public SvcTaxonOtherSourceList() {
    }

    public SvcTaxonOtherSourceList(
           java.lang.String tsn,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSource[] otherSources) {
        super(
            tsn);
        this.otherSources = otherSources;
    }


    /**
     * Gets the otherSources value for this SvcTaxonOtherSourceList.
     * 
     * @return otherSources
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSource[] getOtherSources() {
        return otherSources;
    }


    /**
     * Sets the otherSources value for this SvcTaxonOtherSourceList.
     * 
     * @param otherSources
     */
    public void setOtherSources(gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSource[] otherSources) {
        this.otherSources = otherSources;
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSource getOtherSources(int i) {
        return this.otherSources[i];
    }

    public void setOtherSources(int i, gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSource _value) {
        this.otherSources[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcTaxonOtherSourceList)) return false;
        SvcTaxonOtherSourceList other = (SvcTaxonOtherSourceList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.otherSources==null && other.getOtherSources()==null) || 
             (this.otherSources!=null &&
              java.util.Arrays.equals(this.otherSources, other.getOtherSources())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getOtherSources() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOtherSources());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOtherSources(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcTaxonOtherSourceList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonOtherSourceList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherSources");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "otherSources"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonOtherSource"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
