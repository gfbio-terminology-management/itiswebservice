/**
 * SvcTaxonPublicationList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcTaxonPublicationList  extends gov.usgs.itis.itis_service.data.xsd.SvcListBase  implements java.io.Serializable {
    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublication[] publications;

    public SvcTaxonPublicationList() {
    }

    public SvcTaxonPublicationList(
           java.lang.String tsn,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublication[] publications) {
        super(
            tsn);
        this.publications = publications;
    }


    /**
     * Gets the publications value for this SvcTaxonPublicationList.
     * 
     * @return publications
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublication[] getPublications() {
        return publications;
    }


    /**
     * Sets the publications value for this SvcTaxonPublicationList.
     * 
     * @param publications
     */
    public void setPublications(gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublication[] publications) {
        this.publications = publications;
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublication getPublications(int i) {
        return this.publications[i];
    }

    public void setPublications(int i, gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublication _value) {
        this.publications[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcTaxonPublicationList)) return false;
        SvcTaxonPublicationList other = (SvcTaxonPublicationList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.publications==null && other.getPublications()==null) || 
             (this.publications!=null &&
              java.util.Arrays.equals(this.publications, other.getPublications())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPublications() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPublications());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPublications(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcTaxonPublicationList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonPublicationList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("publications");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "publications"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonPublication"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
