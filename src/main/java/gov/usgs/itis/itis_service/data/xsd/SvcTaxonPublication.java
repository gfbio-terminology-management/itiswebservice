/**
 * SvcTaxonPublication.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcTaxonPublication  implements java.io.Serializable {
    private java.lang.String actualPubDate;

    private java.lang.String isbn;

    private java.lang.String issn;

    private java.lang.String listedPubDate;

    private java.lang.String pages;

    private java.lang.String pubComment;

    private java.lang.String pubName;

    private java.lang.String pubPlace;

    private java.lang.String publisher;

    private java.lang.String referenceAuthor;

    private gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement[] referenceFor;

    private java.lang.String title;

    private java.lang.String updateDate;

    public SvcTaxonPublication() {
    }

    public SvcTaxonPublication(
           java.lang.String actualPubDate,
           java.lang.String isbn,
           java.lang.String issn,
           java.lang.String listedPubDate,
           java.lang.String pages,
           java.lang.String pubComment,
           java.lang.String pubName,
           java.lang.String pubPlace,
           java.lang.String publisher,
           java.lang.String referenceAuthor,
           gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement[] referenceFor,
           java.lang.String title,
           java.lang.String updateDate) {
           this.actualPubDate = actualPubDate;
           this.isbn = isbn;
           this.issn = issn;
           this.listedPubDate = listedPubDate;
           this.pages = pages;
           this.pubComment = pubComment;
           this.pubName = pubName;
           this.pubPlace = pubPlace;
           this.publisher = publisher;
           this.referenceAuthor = referenceAuthor;
           this.referenceFor = referenceFor;
           this.title = title;
           this.updateDate = updateDate;
    }


    /**
     * Gets the actualPubDate value for this SvcTaxonPublication.
     * 
     * @return actualPubDate
     */
    public java.lang.String getActualPubDate() {
        return actualPubDate;
    }


    /**
     * Sets the actualPubDate value for this SvcTaxonPublication.
     * 
     * @param actualPubDate
     */
    public void setActualPubDate(java.lang.String actualPubDate) {
        this.actualPubDate = actualPubDate;
    }


    /**
     * Gets the isbn value for this SvcTaxonPublication.
     * 
     * @return isbn
     */
    public java.lang.String getIsbn() {
        return isbn;
    }


    /**
     * Sets the isbn value for this SvcTaxonPublication.
     * 
     * @param isbn
     */
    public void setIsbn(java.lang.String isbn) {
        this.isbn = isbn;
    }


    /**
     * Gets the issn value for this SvcTaxonPublication.
     * 
     * @return issn
     */
    public java.lang.String getIssn() {
        return issn;
    }


    /**
     * Sets the issn value for this SvcTaxonPublication.
     * 
     * @param issn
     */
    public void setIssn(java.lang.String issn) {
        this.issn = issn;
    }


    /**
     * Gets the listedPubDate value for this SvcTaxonPublication.
     * 
     * @return listedPubDate
     */
    public java.lang.String getListedPubDate() {
        return listedPubDate;
    }


    /**
     * Sets the listedPubDate value for this SvcTaxonPublication.
     * 
     * @param listedPubDate
     */
    public void setListedPubDate(java.lang.String listedPubDate) {
        this.listedPubDate = listedPubDate;
    }


    /**
     * Gets the pages value for this SvcTaxonPublication.
     * 
     * @return pages
     */
    public java.lang.String getPages() {
        return pages;
    }


    /**
     * Sets the pages value for this SvcTaxonPublication.
     * 
     * @param pages
     */
    public void setPages(java.lang.String pages) {
        this.pages = pages;
    }


    /**
     * Gets the pubComment value for this SvcTaxonPublication.
     * 
     * @return pubComment
     */
    public java.lang.String getPubComment() {
        return pubComment;
    }


    /**
     * Sets the pubComment value for this SvcTaxonPublication.
     * 
     * @param pubComment
     */
    public void setPubComment(java.lang.String pubComment) {
        this.pubComment = pubComment;
    }


    /**
     * Gets the pubName value for this SvcTaxonPublication.
     * 
     * @return pubName
     */
    public java.lang.String getPubName() {
        return pubName;
    }


    /**
     * Sets the pubName value for this SvcTaxonPublication.
     * 
     * @param pubName
     */
    public void setPubName(java.lang.String pubName) {
        this.pubName = pubName;
    }


    /**
     * Gets the pubPlace value for this SvcTaxonPublication.
     * 
     * @return pubPlace
     */
    public java.lang.String getPubPlace() {
        return pubPlace;
    }


    /**
     * Sets the pubPlace value for this SvcTaxonPublication.
     * 
     * @param pubPlace
     */
    public void setPubPlace(java.lang.String pubPlace) {
        this.pubPlace = pubPlace;
    }


    /**
     * Gets the publisher value for this SvcTaxonPublication.
     * 
     * @return publisher
     */
    public java.lang.String getPublisher() {
        return publisher;
    }


    /**
     * Sets the publisher value for this SvcTaxonPublication.
     * 
     * @param publisher
     */
    public void setPublisher(java.lang.String publisher) {
        this.publisher = publisher;
    }


    /**
     * Gets the referenceAuthor value for this SvcTaxonPublication.
     * 
     * @return referenceAuthor
     */
    public java.lang.String getReferenceAuthor() {
        return referenceAuthor;
    }


    /**
     * Sets the referenceAuthor value for this SvcTaxonPublication.
     * 
     * @param referenceAuthor
     */
    public void setReferenceAuthor(java.lang.String referenceAuthor) {
        this.referenceAuthor = referenceAuthor;
    }


    /**
     * Gets the referenceFor value for this SvcTaxonPublication.
     * 
     * @return referenceFor
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement[] getReferenceFor() {
        return referenceFor;
    }


    /**
     * Sets the referenceFor value for this SvcTaxonPublication.
     * 
     * @param referenceFor
     */
    public void setReferenceFor(gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement[] referenceFor) {
        this.referenceFor = referenceFor;
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement getReferenceFor(int i) {
        return this.referenceFor[i];
    }

    public void setReferenceFor(int i, gov.usgs.itis.itis_service.data.xsd.SvcReferenceForElement _value) {
        this.referenceFor[i] = _value;
    }


    /**
     * Gets the title value for this SvcTaxonPublication.
     * 
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }


    /**
     * Sets the title value for this SvcTaxonPublication.
     * 
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }


    /**
     * Gets the updateDate value for this SvcTaxonPublication.
     * 
     * @return updateDate
     */
    public java.lang.String getUpdateDate() {
        return updateDate;
    }


    /**
     * Sets the updateDate value for this SvcTaxonPublication.
     * 
     * @param updateDate
     */
    public void setUpdateDate(java.lang.String updateDate) {
        this.updateDate = updateDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcTaxonPublication)) return false;
        SvcTaxonPublication other = (SvcTaxonPublication) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.actualPubDate==null && other.getActualPubDate()==null) || 
             (this.actualPubDate!=null &&
              this.actualPubDate.equals(other.getActualPubDate()))) &&
            ((this.isbn==null && other.getIsbn()==null) || 
             (this.isbn!=null &&
              this.isbn.equals(other.getIsbn()))) &&
            ((this.issn==null && other.getIssn()==null) || 
             (this.issn!=null &&
              this.issn.equals(other.getIssn()))) &&
            ((this.listedPubDate==null && other.getListedPubDate()==null) || 
             (this.listedPubDate!=null &&
              this.listedPubDate.equals(other.getListedPubDate()))) &&
            ((this.pages==null && other.getPages()==null) || 
             (this.pages!=null &&
              this.pages.equals(other.getPages()))) &&
            ((this.pubComment==null && other.getPubComment()==null) || 
             (this.pubComment!=null &&
              this.pubComment.equals(other.getPubComment()))) &&
            ((this.pubName==null && other.getPubName()==null) || 
             (this.pubName!=null &&
              this.pubName.equals(other.getPubName()))) &&
            ((this.pubPlace==null && other.getPubPlace()==null) || 
             (this.pubPlace!=null &&
              this.pubPlace.equals(other.getPubPlace()))) &&
            ((this.publisher==null && other.getPublisher()==null) || 
             (this.publisher!=null &&
              this.publisher.equals(other.getPublisher()))) &&
            ((this.referenceAuthor==null && other.getReferenceAuthor()==null) || 
             (this.referenceAuthor!=null &&
              this.referenceAuthor.equals(other.getReferenceAuthor()))) &&
            ((this.referenceFor==null && other.getReferenceFor()==null) || 
             (this.referenceFor!=null &&
              java.util.Arrays.equals(this.referenceFor, other.getReferenceFor()))) &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.updateDate==null && other.getUpdateDate()==null) || 
             (this.updateDate!=null &&
              this.updateDate.equals(other.getUpdateDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActualPubDate() != null) {
            _hashCode += getActualPubDate().hashCode();
        }
        if (getIsbn() != null) {
            _hashCode += getIsbn().hashCode();
        }
        if (getIssn() != null) {
            _hashCode += getIssn().hashCode();
        }
        if (getListedPubDate() != null) {
            _hashCode += getListedPubDate().hashCode();
        }
        if (getPages() != null) {
            _hashCode += getPages().hashCode();
        }
        if (getPubComment() != null) {
            _hashCode += getPubComment().hashCode();
        }
        if (getPubName() != null) {
            _hashCode += getPubName().hashCode();
        }
        if (getPubPlace() != null) {
            _hashCode += getPubPlace().hashCode();
        }
        if (getPublisher() != null) {
            _hashCode += getPublisher().hashCode();
        }
        if (getReferenceAuthor() != null) {
            _hashCode += getReferenceAuthor().hashCode();
        }
        if (getReferenceFor() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getReferenceFor());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getReferenceFor(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getUpdateDate() != null) {
            _hashCode += getUpdateDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcTaxonPublication.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonPublication"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actualPubDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "actualPubDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isbn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "isbn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "issn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listedPubDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "listedPubDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pages");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "pages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pubComment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "pubComment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pubName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "pubName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pubPlace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "pubPlace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("publisher");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "publisher"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceAuthor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "referenceAuthor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceFor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "referenceFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcReferenceForElement"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "updateDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
