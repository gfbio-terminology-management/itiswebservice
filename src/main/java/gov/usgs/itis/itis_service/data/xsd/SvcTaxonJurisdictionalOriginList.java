/**
 * SvcTaxonJurisdictionalOriginList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcTaxonJurisdictionalOriginList  extends gov.usgs.itis.itis_service.data.xsd.SvcListBase  implements java.io.Serializable {
    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOrigin[] jurisdictionalOrigins;

    public SvcTaxonJurisdictionalOriginList() {
    }

    public SvcTaxonJurisdictionalOriginList(
           java.lang.String tsn,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOrigin[] jurisdictionalOrigins) {
        super(
            tsn);
        this.jurisdictionalOrigins = jurisdictionalOrigins;
    }


    /**
     * Gets the jurisdictionalOrigins value for this SvcTaxonJurisdictionalOriginList.
     * 
     * @return jurisdictionalOrigins
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOrigin[] getJurisdictionalOrigins() {
        return jurisdictionalOrigins;
    }


    /**
     * Sets the jurisdictionalOrigins value for this SvcTaxonJurisdictionalOriginList.
     * 
     * @param jurisdictionalOrigins
     */
    public void setJurisdictionalOrigins(gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOrigin[] jurisdictionalOrigins) {
        this.jurisdictionalOrigins = jurisdictionalOrigins;
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOrigin getJurisdictionalOrigins(int i) {
        return this.jurisdictionalOrigins[i];
    }

    public void setJurisdictionalOrigins(int i, gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOrigin _value) {
        this.jurisdictionalOrigins[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcTaxonJurisdictionalOriginList)) return false;
        SvcTaxonJurisdictionalOriginList other = (SvcTaxonJurisdictionalOriginList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.jurisdictionalOrigins==null && other.getJurisdictionalOrigins()==null) || 
             (this.jurisdictionalOrigins!=null &&
              java.util.Arrays.equals(this.jurisdictionalOrigins, other.getJurisdictionalOrigins())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getJurisdictionalOrigins() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getJurisdictionalOrigins());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getJurisdictionalOrigins(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcTaxonJurisdictionalOriginList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonJurisdictionalOriginList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jurisdictionalOrigins");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "jurisdictionalOrigins"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonJurisdictionalOrigin"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
