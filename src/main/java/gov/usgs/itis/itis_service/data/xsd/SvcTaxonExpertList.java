/**
 * SvcTaxonExpertList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.data.xsd;

public class SvcTaxonExpertList  extends gov.usgs.itis.itis_service.data.xsd.SvcListBase  implements java.io.Serializable {
    private gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpert[] experts;

    public SvcTaxonExpertList() {
    }

    public SvcTaxonExpertList(
           java.lang.String tsn,
           gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpert[] experts) {
        super(
            tsn);
        this.experts = experts;
    }


    /**
     * Gets the experts value for this SvcTaxonExpertList.
     * 
     * @return experts
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpert[] getExperts() {
        return experts;
    }


    /**
     * Sets the experts value for this SvcTaxonExpertList.
     * 
     * @param experts
     */
    public void setExperts(gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpert[] experts) {
        this.experts = experts;
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpert getExperts(int i) {
        return this.experts[i];
    }

    public void setExperts(int i, gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpert _value) {
        this.experts[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcTaxonExpertList)) return false;
        SvcTaxonExpertList other = (SvcTaxonExpertList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.experts==null && other.getExperts()==null) || 
             (this.experts!=null &&
              java.util.Arrays.equals(this.experts, other.getExperts())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getExperts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getExperts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getExperts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcTaxonExpertList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonExpertList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("experts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "experts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcTaxonExpert"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
