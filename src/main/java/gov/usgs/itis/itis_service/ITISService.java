/**
 * ITISService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service;

public interface ITISService extends javax.xml.rpc.Service {
    public java.lang.String getITISServiceHttpSoap11EndpointAddress();

    public gov.usgs.itis.itis_service.ITISServicePortType getITISServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public gov.usgs.itis.itis_service.ITISServicePortType getITISServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
