/**
 * 
 */
package gov.usgs.itis.itis_service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.log4j.Logger;
import org.gfbio.config.WSConfiguration;
import org.gfbio.interfaces.WSInterface;
import org.gfbio.resultset.AllBroaderResultSetEntry;
import org.gfbio.resultset.CapabilitiesResultSetEntry;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.resultset.HierarchyResultSetEntry;
import org.gfbio.resultset.MetadataResultSetEntry;
import org.gfbio.resultset.SearchResultSetEntry;
import org.gfbio.resultset.SynonymsResultSetEntry;
import org.gfbio.resultset.TermCombinedResultSetEntry;
import org.gfbio.resultset.TermOriginalResultSetEntry;
import org.gfbio.util.YAMLConfigReader;
import org.json.JSONArray;
import org.json.JSONObject;
import de.pangaea.externalwebservices.ITISFacade;
import de.pangaea.externalwebservices.ITISSolrClient;
import gov.usgs.itis.itis_service.data.xsd.SvcAcceptedName;
import gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcCommonName;
import gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData;
import gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness;
import gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord;
import gov.usgs.itis.itis_service.data.xsd.SvcScientificName;
import gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonName;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSource;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo;


public class ITISRestfulWSImpl implements WSInterface {

  private static final Logger LOGGER = Logger.getLogger(ITISRestfulWSImpl.class);

  public final String ontologyDescription = "ITIS SOAP webservice";
  public final String taxonURLPrefix =
      "http://www.itis.gov/servlet/SingleRpt/SingleRpt?search_topic=TSN&search_value=";


  private ITISFacade itisFacade;
  private ExecutorService executor;

  private WSConfiguration itisConfig;

  public ITISRestfulWSImpl() {
    itisFacade = new ITISFacade(false);

    executor = Executors.newFixedThreadPool(4);

    YAMLConfigReader reader = YAMLConfigReader.getInstance();
    itisConfig = reader.getWSConfig("ITIS");

    if (itisConfig != null)
      LOGGER.info("ITIS Restful Webservice Adapter loaded!");
    else
      LOGGER.error("Error while loading ITIS config!");

  }

  @Override
  public String getAcronym() {
    return itisConfig.getAcronym();
  }

  @Override
  public String getDescription() {
    return itisConfig.getDescription();
  }

  @Override
  public String getName() {
    return itisConfig.getName();
  }

  @Override
  public String getURI() {
    return itisConfig.getWebserviceURL();
    // return "http://www.itis.gov/";
  }

  @Override
  public List<String> getDomains() {
    List<String> l = new ArrayList<String>();
    for (Domains d : itisConfig.getWsDomains()) {
      l.add(d.name());
    }
    return l;
  }

  @Override
  public String getCurrentVersion() {

    return itisConfig.getVersion();
  }

  @Override
  public GFBioResultSet<AllBroaderResultSetEntry> getAllBroader(String uriOrexternalID) {
    if (uriOrexternalID.startsWith(taxonURLPrefix)) {
      uriOrexternalID = uriOrexternalID.substring(taxonURLPrefix.length());
    }

    GFBioResultSet<AllBroaderResultSetEntry> rs =
        new GFBioResultSet<AllBroaderResultSetEntry>("itis");
    SvcHierarchyRecord[] records = itisFacade.getFullHierarchyFromTSN(uriOrexternalID);
    List<SvcHierarchyRecord> temp = Arrays.asList(records);
    Collections.reverse(temp);

    if (temp != null) {
      for (SvcHierarchyRecord hierarchy : temp) {
        LOGGER.info("TSN: " + hierarchy.getTsn());
        LOGGER.info("ParentTSN: " + hierarchy.getParentTsn());

        setEntry(rs, hierarchy);
      }
    } else {
      LOGGER.error(" no classification for this id  " + uriOrexternalID);
      return null;
    }
    return rs;
  }

  @Override
  public GFBioResultSet<CapabilitiesResultSetEntry> getCapabilities() {
    GFBioResultSet<CapabilitiesResultSetEntry> rs =
        new GFBioResultSet<CapabilitiesResultSetEntry>("geonames");
    CapabilitiesResultSetEntry e = new CapabilitiesResultSetEntry();
    ArrayList<String> servicesArray = new ArrayList<String>();
    for (Services s : itisConfig.getAvailableServices()) {
      servicesArray.add(s.toString());
    }
    e.setAvailableServices(servicesArray);

    ArrayList<String> modesArray = new ArrayList<String>();
    for (SearchModes m : itisConfig.getSearchModes()) {
      modesArray.add(m.toString());
    }
    e.setSearchModes(modesArray);
    rs.addEntry(e);
    return rs;
  }

  @Override
  public GFBioResultSet<HierarchyResultSetEntry> getHierarchy(String uriOrexternalID) {

    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    LOGGER.info("search query " + externalID);
    GFBioResultSet<HierarchyResultSetEntry> rs =
        new GFBioResultSet<HierarchyResultSetEntry>("itis");
    SvcHierarchyRecord[] hierarchy = itisFacade.getFullHierarchyFromTSN(externalID);
    for (SvcHierarchyRecord rec : hierarchy) {
      HierarchyResultSetEntry e = new HierarchyResultSetEntry();
      ArrayList<String> array = new ArrayList<String>();
      if (!rec.getParentTsn().equals(externalID)) {
        if (!rec.getParentTsn().equals("")) {
          array.add(taxonURLPrefix + rec.getParentTsn());
        }
        e.setLabel(rec.getTaxonName());
        e.setRank(rec.getRankName());
        e.setUri(taxonURLPrefix + rec.getTsn());
        e.setExternalID(rec.getTsn());
        e.setHierarchy(array);
        rs.addEntry(e);
      }
    }
    LOGGER.info("search query processed " + externalID);
    return rs;
  }

  @Override
  public GFBioResultSet<MetadataResultSetEntry> getMetadata() {
    GFBioResultSet<MetadataResultSetEntry> rs = new GFBioResultSet<MetadataResultSetEntry>("itis");
    MetadataResultSetEntry e = new MetadataResultSetEntry();
    e.setName(getName());
    e.setAcronym(getAcronym());
    e.setVersion(itisConfig.getVersion());
    e.setDescription(getDescription());
    e.setKeywords(itisConfig.getKeywords());
    e.setUri(getURI());
    e.setContact(itisConfig.getContact());
    ArrayList<String> domainUris = new ArrayList<String>();
    for (Domains d : itisConfig.getWsDomains()) {
      domainUris.add(d.getUri());
    }
    e.setDomain(domainUris);
    rs.addEntry(e);
    return rs;
  }

  /**
   * @param id URI or External ID
   */
  @Override
  public GFBioResultSet<SynonymsResultSetEntry> getSynonyms(String id) {

    if (id.startsWith(taxonURLPrefix)) {
      id = id.substring(taxonURLPrefix.length());
    }

    GFBioResultSet<SynonymsResultSetEntry> rs = new GFBioResultSet<SynonymsResultSetEntry>("itis");
    SvcSynonymNameList snl = itisFacade.getTaxonSynonymsByTSN(id);
    if (snl != null) {
      setEntry(rs, snl);
      return rs;
    }

    return null;
  }

  /**
   * Solr-based ITIS query
   * 
   * @param tsn
   * @return
   */
  private GFBioResultSet<TermCombinedResultSetEntry> getCombinedDataByTSN(String tsn) {
    GFBioResultSet<TermCombinedResultSetEntry> rs =
        new GFBioResultSet<TermCombinedResultSetEntry>("itis");

    // Solr result does not contain accepted names
    SvcAcceptedNameList acceptedNames = itisFacade.getAcceptedNamesFromTSN(tsn);
    JSONObject jo = ITISSolrClient.query("tsn:", tsn); // contains all required data
    JSONArray docs = jo.getJSONArray("docs");

    TermCombinedResultSetEntry e = new TermCombinedResultSetEntry();

    // for (Object o : docs) {
    for (int i = 0; i < docs.length(); i++) {
      JSONObject doc = docs.getJSONObject(i);

      e.setLabel(doc.getString("nameWInd").trim());
      e.setExternalID(doc.getString("tsn").trim());
      e.setUri(taxonURLPrefix + doc.getString("tsn").trim());

      // Common Names
      ArrayList<String> commonNamesList = new ArrayList<String>();
      if (doc.has("vernacular")) {
        JSONArray vernacular = doc.getJSONArray("vernacular"); // -> common names
        // for (Object ver : vernacular) {
        for (int j = 0; j < vernacular.length(); j++) {
          String name = vernacular.getString(j);
          name = name.replace("$", ";");
          name = name.replaceFirst(";", "");
          String[] parts = name.split(";");
          commonNamesList.add(parts[0].trim());
        }
        if (!commonNamesList.isEmpty()) {
          e.setCommonNames(commonNamesList);
        }
      }

      // Rank & Kingdom
      e.setKingdom(doc.getString("kingdom").trim());
      e.setRank(doc.getString("rank").trim());

      // Synonyms
      ArrayList<String> synonymsList = new ArrayList<String>();
      if (doc.has("synonyms")) {
        JSONArray synonyms = doc.getJSONArray("synonyms"); // -> common names
        // for (Object syn : synonyms) {
        for (int j = 0; j < synonyms.length(); j++) {
          String name = synonyms.getString(j);
          name = name.substring(name.indexOf(":"), name.length()).replace("$", ";");
          System.out.println("+++++ " + name);
          String[] parts = name.split(";");
          synonymsList.add(parts[0].trim());
        }
        if (!synonymsList.isEmpty()) {
          e.setSynonyms(synonymsList);
        }
      }

      // Source
      e.setSourceTerminology(getAcronym());

      // Accepted Names
      if (acceptedNames.getAcceptedNames() != null && acceptedNames.getAcceptedNames().length > 0) {
        ArrayList<String> acceptedNamesList = new ArrayList<String>();
        for (SvcAcceptedName a : acceptedNames.getAcceptedNames()) {
          if (a != null) {
            acceptedNamesList.add(a.getAcceptedName().trim());
          }
        }
        if (!acceptedNamesList.isEmpty()) {
          e.setOriginalTermInfo("AcceptedNames", acceptedNamesList);
        }
      }

      // credibilityRating
      e.setOriginalTermInfo("RecordCredibilityRating", doc.getString("credibilityRating").trim());

      // Completeness
      e.setOriginalTermInfo("GlobalSpeciesCompleteness",
          doc.getString("completenessRating").trim());

      // Other Source(s)
      ArrayList<String> otherSourceList = new ArrayList<String>();
      if (doc.has("otherSource")) {
        JSONArray otherSource = doc.getJSONArray("otherSource"); // -> common names
        for (int j = 0; j < otherSource.length(); j++) {
          // for (Object os : otherSource) {
          String name = otherSource.getString(j);
          name = name.replace("$", ";");
          name = name.replaceFirst(";", "");
          String[] parts = name.split(";");
          // otherSourceList.add(parts[2].trim()); // -> source
          otherSourceList.add(parts[3].trim()); // -> source name
        }
        if (!otherSourceList.isEmpty()) {
          e.setOriginalTermInfo("OtherSources", otherSourceList);
        }
      }

      rs.addEntry(e);
    }
    return rs;

  }

  @Override
  public GFBioResultSet<TermCombinedResultSetEntry> getTermInfosCombined(String termUri) {
    if (termUri.startsWith(taxonURLPrefix)) {
      termUri = termUri.substring(taxonURLPrefix.length());
    }
    final String tsn = termUri;

    return getCombinedDataByTSN(tsn);
  }

  // @Override
  @Deprecated
  public GFBioResultSet<TermCombinedResultSetEntry> getTermInfosCombinedd(String termUri) {
    if (termUri.startsWith(taxonURLPrefix)) {
      termUri = termUri.substring(taxonURLPrefix.length());
    }
    final String tsn = termUri;
    GFBioResultSet<TermCombinedResultSetEntry> rs =
        new GFBioResultSet<TermCombinedResultSetEntry>("itis");

    SvcAcceptedNameList acceptedNames = null;
    SvcTaxonRankInfo rank = null;
    SvcCommonNameList commonNames = null;
    SvcScientificName scientificName = null;
    SvcSynonymNameList synonyms = null;
    SvcCredibilityData cred = null;
    SvcGlobalSpeciesCompleteness completeness = null;
    SvcTaxonOtherSourceList sources = null;

    try {

      Future<SvcAcceptedNameList> f1 =
          executor.submit(() -> itisFacade.getAcceptedNamesFromTSN(tsn));
      Future<SvcTaxonRankInfo> f2 = executor.submit(() -> itisFacade.getTaxonRankInfoByTSN(tsn));
      Future<SvcCommonNameList> f3 = executor.submit(() -> itisFacade.GetCommonNamesFromTSN(tsn));
      Future<SvcScientificName> f4 = executor.submit(() -> itisFacade.getScientificName(tsn));
      Future<SvcSynonymNameList> f5 = executor.submit(() -> itisFacade.getTaxonSynonymsByTSN(tsn));
      Future<SvcCredibilityData> f6 =
          executor.submit(() -> itisFacade.getCredibilityRatingFromTSN(tsn));
      Future<SvcGlobalSpeciesCompleteness> f7 =
          executor.submit(() -> itisFacade.getGlobalSpeciesCompletenessFromTSN(tsn));
      Future<SvcTaxonOtherSourceList> f8 =
          executor.submit(() -> itisFacade.getOtherSourcesFromTSN(tsn));

      acceptedNames = f1.get();
      rank = f2.get();
      commonNames = f3.get();
      scientificName = f4.get();
      synonyms = f5.get();
      cred = f6.get();
      completeness = f7.get();
      sources = f8.get();

    } catch (ExecutionException | InterruptedException e) {
      LOGGER.error(e.getMessage());
    }
    executor.shutdown();

    TermCombinedResultSetEntry e = new TermCombinedResultSetEntry();
    if (scientificName != null) {
      if (scientificName.getCombinedName() != null) {
        e.setLabel(scientificName.getCombinedName().trim());

      }
      if (scientificName.getTsn() != null) {
        e.setExternalID(scientificName.getTsn().trim());
        e.setUri(taxonURLPrefix + scientificName.getTsn().trim());
      }
    }
    if (commonNames.getCommonNames().length > 0) {
      ArrayList<String> commonNamesList = new ArrayList<String>();
      for (SvcCommonName a : commonNames.getCommonNames()) {
        if (a != null) {
          commonNamesList.add(a.getCommonName().trim());
        }
      }
      if (!commonNamesList.isEmpty()) {
        e.setCommonNames(commonNamesList);
      }
    }
    if (rank != null) {
      if (rank.getRankName() != null) {
        e.setKingdom(rank.getKingdomName().trim());
      }
      if (rank.getKingdomName() != null) {
        e.setRank(rank.getRankName().trim());
      }
    }
    if (synonyms.getSynonyms() != null && synonyms.getSynonyms().length > 0) {
      ArrayList<String> synonymsList = new ArrayList<String>();
      for (SvcTaxonName syn : synonyms.getSynonyms()) {
        if (syn != null) {
          synonymsList.add(syn.getSciName().trim());
        }
      }
      if (!synonymsList.isEmpty()) {
        e.setSynonyms(synonymsList);
      }
    }
    e.setSourceTerminology(getAcronym());
    if (acceptedNames.getAcceptedNames() != null && acceptedNames.getAcceptedNames().length > 0) {
      ArrayList<String> acceptedNamesList = new ArrayList<String>();
      for (SvcAcceptedName a : acceptedNames.getAcceptedNames()) {
        if (a != null) {
          acceptedNamesList.add(a.getAcceptedName().trim());
        }
      }
      if (!acceptedNamesList.isEmpty()) {
        e.setOriginalTermInfo("AcceptedNames", acceptedNamesList);
      }
    }
    if (cred.getCredRating() != null) {
      e.setOriginalTermInfo("RecordCredibilityRating", cred.getCredRating().trim());
    }
    if (completeness.getCompleteness() != null) {
      e.setOriginalTermInfo("GlobalSpeciesCompleteness", completeness.getCompleteness().trim());
    }
    if (sources.getOtherSources().length > 0) {
      ArrayList<String> otherSourceList = new ArrayList<String>();
      for (SvcTaxonOtherSource source : sources.getOtherSources()) {
        if (source != null) {
          otherSourceList.add(source.getSource().trim());
        }
      }
      if (!otherSourceList.isEmpty()) {
        e.setOriginalTermInfo("OtherSources", otherSourceList);
      }
    }
    rs.addEntry(e);
    return rs;
  }

  @Override
  public GFBioResultSet<TermOriginalResultSetEntry> getTermInfosOriginal(String termUri) {

    if (termUri.startsWith(taxonURLPrefix)) {
      termUri = termUri.substring(taxonURLPrefix.length());
    }
    final String tsn = termUri;

    return getCompleteDataByTSN(tsn);
  }

  // @Override
  @Deprecated
  public GFBioResultSet<TermOriginalResultSetEntry> getTermInfosOriginall(String termUri) {

    if (termUri.startsWith(taxonURLPrefix)) {
      termUri = termUri.substring(taxonURLPrefix.length());
    }
    final String tsn = termUri;

    GFBioResultSet<TermOriginalResultSetEntry> rs =
        new GFBioResultSet<TermOriginalResultSetEntry>("itis");

    // Takes about 8s
    // SvcAcceptedNameList acceptedNames = itisFacade.getAcceptedNamesFromTSN(termUri);
    // SvcTaxonRankInfo rank = itisFacade.getTaxonRankInfoByTSN(termUri);
    // SvcCommonNameList commonNames = itisFacade.GetCommonNamesFromTSN(termUri);
    // SvcScientificName scientificName = itisFacade.getScientificName(termUri);
    // SvcSynonymNameList synonyms = itisFacade.getTaxonSynonymsByTSN(termUri);
    // SvcCredibilityData cred = itisFacade.getCredibilityRatingFromTSN(termUri);
    // SvcGlobalSpeciesCompleteness completeness =
    // itisFacade.getGlobalSpeciesCompletenessFromTSN(termUri);
    // SvcTaxonOtherSourceList sources = itisFacade.getOtherSourcesFromTSN(termUri);

    SvcAcceptedNameList acceptedNames = null;
    SvcTaxonRankInfo rank = null;
    SvcCommonNameList commonNames = null;
    SvcScientificName scientificName = null;
    SvcSynonymNameList synonyms = null;
    SvcCredibilityData cred = null;
    SvcGlobalSpeciesCompleteness completeness = null;
    SvcTaxonOtherSourceList sources = null;

    // Takes about 4.5s
    try {

      Future<SvcAcceptedNameList> f1 =
          executor.submit(() -> itisFacade.getAcceptedNamesFromTSN(tsn));
      Future<SvcTaxonRankInfo> f2 = executor.submit(() -> itisFacade.getTaxonRankInfoByTSN(tsn));
      Future<SvcCommonNameList> f3 = executor.submit(() -> itisFacade.GetCommonNamesFromTSN(tsn));
      Future<SvcScientificName> f4 = executor.submit(() -> itisFacade.getScientificName(tsn));
      Future<SvcSynonymNameList> f5 = executor.submit(() -> itisFacade.getTaxonSynonymsByTSN(tsn));
      Future<SvcCredibilityData> f6 =
          executor.submit(() -> itisFacade.getCredibilityRatingFromTSN(tsn));
      Future<SvcGlobalSpeciesCompleteness> f7 =
          executor.submit(() -> itisFacade.getGlobalSpeciesCompletenessFromTSN(tsn));
      Future<SvcTaxonOtherSourceList> f8 =
          executor.submit(() -> itisFacade.getOtherSourcesFromTSN(tsn));

      acceptedNames = f1.get();
      rank = f2.get();
      commonNames = f3.get();
      scientificName = f4.get();
      synonyms = f5.get();
      cred = f6.get();
      completeness = f7.get();
      sources = f8.get();
    } catch (Exception e) {
      LOGGER.info(e.getMessage());
    }
    executor.shutdown();

    TermOriginalResultSetEntry e = new TermOriginalResultSetEntry();
    if (scientificName != null) {
      if (scientificName.getCombinedName() != null) {
        e.setOriginalTermInfo("ScientificName", scientificName.getCombinedName().trim());

      }
      if (scientificName.getTsn() != null) {
        e.setOriginalTermInfo("TaxonomicSerialNumber", scientificName.getTsn().trim());
        e.setOriginalTermInfo("URL", taxonURLPrefix + scientificName.getTsn().trim());
      }
    }
    if (commonNames.getCommonNames().length > 0) {
      ArrayList<String> commonNamesList = new ArrayList<String>();
      for (SvcCommonName a : commonNames.getCommonNames()) {
        if (a != null) {
          commonNamesList.add(a.getCommonName().trim());
        }
      }
      if (!commonNamesList.isEmpty()) {
        e.setOriginalTermInfo("CommonNames", commonNamesList);
      }
    }
    if (rank != null) {
      if (rank.getRankName() != null) {
        e.setOriginalTermInfo("Kingdom", rank.getKingdomName().trim());
      }
      if (rank.getKingdomName() != null) {
        e.setOriginalTermInfo("TaxonomicRank", rank.getRankName().trim());
      }
    }
    if (synonyms.getSynonyms() != null && synonyms.getSynonyms().length > 0) {
      ArrayList<String> synonymsList = new ArrayList<String>();
      for (SvcTaxonName syn : synonyms.getSynonyms()) {
        if (syn != null) {
          synonymsList.add(syn.getSciName().trim());
        }
      }
      if (!synonymsList.isEmpty()) {
        e.setOriginalTermInfo("Synonyms", synonymsList);
      }
    }
    if (acceptedNames.getAcceptedNames() != null && acceptedNames.getAcceptedNames().length > 0) {
      ArrayList<String> acceptedNamesList = new ArrayList<String>();
      for (SvcAcceptedName a : acceptedNames.getAcceptedNames()) {
        if (a != null) {
          acceptedNamesList.add(a.getAcceptedName().trim());
        }
      }
      if (!acceptedNamesList.isEmpty()) {
        e.setOriginalTermInfo("AcceptedNames", acceptedNamesList);
      }
    }
    if (cred.getCredRating() != null) {
      e.setOriginalTermInfo("RecordCredibilityRating", cred.getCredRating().trim());
    }
    if (completeness.getCompleteness() != null) {
      e.setOriginalTermInfo("GlobalSpeciesCompleteness", completeness.getCompleteness().trim());
    }
    if (sources.getOtherSources().length > 0) {
      ArrayList<String> otherSourceList = new ArrayList<String>();
      for (SvcTaxonOtherSource source : sources.getOtherSources()) {
        if (source != null) {
          otherSourceList.add(source.getSource().trim());
        }
      }
      if (!otherSourceList.isEmpty()) {
        e.setOriginalTermInfo("OtherSources", otherSourceList);
      }
    }

    rs.addEntry(e);
    return rs;
  }

  /**
   * One call to the Solr-API returns all the required data at once!
   * 
   * @param tsn
   * @return
   */
  private GFBioResultSet<TermOriginalResultSetEntry> getCompleteDataByTSN(String tsn) {

    GFBioResultSet<TermOriginalResultSetEntry> rs =
        new GFBioResultSet<TermOriginalResultSetEntry>("itis");

    // Solr result does not contain accepted names
    SvcAcceptedNameList acceptedNames = itisFacade.getAcceptedNamesFromTSN(tsn);

    // all other elements are included in the Solr result
    // SvcTaxonRankInfo rank = null;
    // SvcCommonNameList commonNames = null;
    // SvcScientificName scientificName = null;
    // SvcSynonymNameList synonyms = null;
    // SvcCredibilityData cred = null;
    // SvcGlobalSpeciesCompleteness completeness = null;
    // SvcTaxonOtherSourceList sources = null;

    JSONObject jo = ITISSolrClient.query("tsn:", tsn);
    JSONArray docs = jo.getJSONArray("docs");

    TermOriginalResultSetEntry e = new TermOriginalResultSetEntry();

    for (int i = 0; i < docs.length(); i++) {
      // for (Object o : docs) {
      JSONObject doc = docs.getJSONObject(i);

      e.setOriginalTermInfo("ScientificName", doc.getString("nameWInd").trim());
      e.setOriginalTermInfo("TaxonomicSerialNumber", doc.getString("tsn").trim());
      e.setOriginalTermInfo("URL", taxonURLPrefix + doc.getString("tsn").trim());


      // Common Names
      ArrayList<String> commonNamesList = new ArrayList<String>();
      if (doc.has("vernacular")) {
        JSONArray vernacular = doc.getJSONArray("vernacular"); // -> common names
        for (int j = 0; j < vernacular.length(); j++) {
          // for (Object ver : vernacular) {
          String name = vernacular.getString(j);
          name = name.replace("$", ";");
          name = name.replaceFirst(";", "");
          String[] parts = name.split(";");
          commonNamesList.add(parts[0].trim());
        }
        if (!commonNamesList.isEmpty()) {
          e.setOriginalTermInfo("CommonNames", commonNamesList);
        }
      }

      // Kingdom & Rank
      e.setOriginalTermInfo("Kingdom", doc.getString("kingdom").trim());
      e.setOriginalTermInfo("TaxonomicRank", doc.getString("rank").trim());

      // Synonyms
      // e.g. 566578:$Agave americana americana expa$
      ArrayList<String> synonymsList = new ArrayList<String>();
      if (doc.has("synonyms")) {
        JSONArray synonyms = doc.getJSONArray("synonyms"); // -> common names
        for (int j = 0; j < synonyms.length(); j++) {
          // for (Object syn : synonyms) {
          String name = synonyms.getString(j);
          name = name.substring(name.indexOf(":"), name.length()).replace("$", ";");
          System.out.println("+++++ " + name);
          String[] parts = name.split(";");
          synonymsList.add(parts[0].trim());
        }
        if (!synonymsList.isEmpty()) {
          e.setOriginalTermInfo("Synonyms", synonymsList);
        }
      }

      // Accepted Names
      if (acceptedNames.getAcceptedNames() != null && acceptedNames.getAcceptedNames().length > 0) {
        ArrayList<String> acceptedNamesList = new ArrayList<String>();
        for (SvcAcceptedName a : acceptedNames.getAcceptedNames()) {
          if (a != null) {
            acceptedNamesList.add(a.getAcceptedName().trim());
          }
        }
        if (!acceptedNamesList.isEmpty()) {
          e.setOriginalTermInfo("AcceptedNames", acceptedNamesList);
        }
      }

      // credibilityRating
      e.setOriginalTermInfo("RecordCredibilityRating", doc.getString("credibilityRating").trim());

      // Completeness
      e.setOriginalTermInfo("GlobalSpeciesCompleteness",
          doc.getString("completenessRating").trim());

      // Other Source(s)
      ArrayList<String> otherSourceList = new ArrayList<String>();
      if (doc.has("otherSource")) {
        JSONArray otherSource = doc.getJSONArray("otherSource"); // -> common names
        for (int j = 0; j < otherSource.length(); j++) {
          // for (Object os : otherSource) {
          String name = otherSource.getString(j);
          name = name.replace("$", ";");
          name = name.replaceFirst(";", "");
          String[] parts = name.split(";");
          // otherSourceList.add(parts[2].trim()); // -> source
          otherSourceList.add(parts[3].trim()); // -> source name
        }
        if (!otherSourceList.isEmpty()) {
          e.setOriginalTermInfo("OtherSources", otherSourceList);
        }
      }

      rs.addEntry(e);
    }
    return rs;
  }

  @Override
  public GFBioResultSet<SearchResultSetEntry> getTermInfosProcessed(String termUri) {
    if (termUri.startsWith(taxonURLPrefix)) {
      termUri = termUri.substring(taxonURLPrefix.length());
    }
    final String tsn = termUri;
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("itis");

    SvcTaxonRankInfo rankInfo = null;
    SvcScientificName sName = null;

    try {
      Future<SvcTaxonRankInfo> fut1 = executor.submit(() -> itisFacade.getTaxonRankInfoByTSN(tsn));
      Future<SvcScientificName> fut2 = executor.submit(() -> itisFacade.getScientificName(tsn));

      rankInfo = fut1.get();
      sName = fut2.get();

    } catch (Exception e) {
      LOGGER.error(e.getMessage());
    }

    SearchResultSetEntry e = new SearchResultSetEntry();
    if (rankInfo != null) {
      e.setKingdom(rankInfo.getKingdomName());
      e.setRank(rankInfo.getRankName());
    }
    if (sName != null) {
      e.setLabel(sName.getCombinedName());
      e.setExternalID(sName.getTsn());
      e.setUri(taxonURLPrefix + sName.getTsn());
    }
    e.setSourceTerminology(itisConfig.getAcronym());
    SvcCommonNameList commonNames = itisFacade.GetCommonNamesFromTSN(tsn);
    if (commonNames.getCommonNames().length > 0) {
      ArrayList<String> commonNamesList = new ArrayList<String>();
      for (SvcCommonName a : commonNames.getCommonNames()) {
        if (a != null) {
          commonNamesList.add(a.getCommonName().trim());
        }
      }
      if (!commonNamesList.isEmpty()) {
        e.setCommonNames(commonNamesList);
      }
    }
    rs.addEntry(e);
    return rs;
  }

  @Override
  public boolean isResponding() {
    return itisFacade.checkITISAvailability();
  }

  @Override
  public GFBioResultSet<SearchResultSetEntry> search(final String query, final String match_type) {
    LOGGER.info("search query " + query + " matchtype " + match_type);

    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("itis");
    SvcScientificName[] trans =
        itisFacade.searchByScientificName(query, SearchTypes.valueOf(match_type));
    if (trans.length > 0)
      format(trans, rs);

    // TODO 10/21/20 common names
    SvcCommonName[] commons = itisFacade.searchByCommonName(query, SearchTypes.valueOf(match_type));
    if (commons.length > 0)
      format(commons, rs);

    LOGGER.info("search query processed " + query + " matchtype " + match_type);
    return rs;
  }

  @Override
  public boolean supportsMatchType(String matchType) {
    return Arrays.stream(itisConfig.getSearchModes())
        .anyMatch(SearchModes.valueOf(matchType)::equals);
  }

  /*
   * Utility Methods
   */

  /**
   * ITISFacade conform format method
   * 
   * @param trans
   * @param rs
   */
  private void format(SvcScientificName[] trans, GFBioResultSet<SearchResultSetEntry> rs) {
    if (trans[0] != null) {
      // unfortunately, these could be multiple calls
      // long startTime = System.nanoTime();

      for (SvcScientificName sn : trans) {
        if (sn != null) {

          SvcTaxonRankInfo rankInfo = itisFacade.getTaxonRankInfoByTSN(sn.getTsn());
          setEntry(rs, sn, rankInfo);
          // SvcTaxonRankInfo rankInfo = null;
          // try {
          // Future<SvcTaxonRankInfo> f =
          // executor.submit(() -> itisFacade.getTaxonRankInfoByTSN(sn.getTsn()));
          //
          // rankInfo = f.get();
          // } catch (ExecutionException | InterruptedException e) {
          // LOGGER.error(e.getMessage());
          // }

          // Future future = executor.submit(new Callable() {
          // public Object call() throws Exception {
          // SvcTaxonRankInfo rankInfo = itisFacade.getTaxonRankInfoByTSN(sn.getTsn());
          // setEntry(rs, sn, rankInfo);
          //
          // return null;
          // }
          // });
          // setEntry(rs, sn, rankInfo);
        }
      }
      // ... the code being measured ...
      // long estimatedTime = System.nanoTime() - startTime;
      // double elapsedTimeInSecond = (double) estimatedTime / 1_000_000_000;
      // LOGGER.info(elapsedTimeInSecond);
    }
  }

  private void format(SvcCommonName[] commons, GFBioResultSet<SearchResultSetEntry> rs) {
    if (commons != null) {
      for (SvcCommonName cn : commons) {
        if (cn != null) {
          // TODO 10/23/20 FB Parallelize?!
          // SvcScientificName sn = itisFacade.getScientificName(cn.getTsn());
          // SvcTaxonRankInfo rankInfo = itisFacade.getTaxonRankInfoByTSN(sn.getTsn());
          // setEntry(rs, sn, rankInfo);
          SvcScientificName sn = null;
          SvcTaxonRankInfo rankInfo = null;
          try {
            Future<SvcScientificName> f1 =
                executor.submit(() -> itisFacade.getScientificName(cn.getTsn()));
            Future<SvcTaxonRankInfo> f2 =
                executor.submit(() -> itisFacade.getTaxonRankInfoByTSN(cn.getTsn()));

            sn = f1.get();
            rankInfo = f2.get();
          } catch (ExecutionException | InterruptedException e) {
            LOGGER.error(e.getMessage());
          }

          setEntry(rs, sn, rankInfo);
        }
      }
    }
    executor.shutdown();
  }

  private void setEntry(GFBioResultSet<SearchResultSetEntry> rs, final SvcScientificName sName,
      final SvcTaxonRankInfo rankInfo) {
    SearchResultSetEntry e = new SearchResultSetEntry();
    e.setKingdom(rankInfo.getKingdomName());
    e.setLabel(sName.getCombinedName());
    e.setRank(rankInfo.getRankName());
    e.setSourceTerminology(itisConfig.getAcronym());
    e.setUri(taxonURLPrefix + sName.getTsn());
    e.setExternalID(sName.getTsn());
    e.setSourceTerminology(getAcronym());

    SvcCommonNameList cmNames = itisFacade.GetCommonNamesFromTSN(sName.getTsn());
    if (cmNames.getCommonNames() != null && cmNames.getCommonNames().length > 0) {
      ArrayList<String> commonNamesList = new ArrayList<String>();
      for (SvcCommonName a : cmNames.getCommonNames()) {
        if (a != null) {
          commonNamesList.add(a.getCommonName().trim());
        }
      }
      if (!commonNamesList.isEmpty()) {
        e.setCommonNames(commonNamesList);
      }
    }

    rs.addEntry(e);
  }

  private void setEntry(GFBioResultSet<SynonymsResultSetEntry> rs, SvcSynonymNameList snl) {
    SvcTaxonName[] list = snl.getSynonyms();
    SynonymsResultSetEntry e = new SynonymsResultSetEntry();
    ArrayList<String> array = new ArrayList<String>();
    if (list[0] != null) {
      for (SvcTaxonName n : list) {
        array.add(n.getSciName());
      }
    }
    e.setSynonyms(array);
    rs.addEntry(e);
  }

  private void setEntry(GFBioResultSet<AllBroaderResultSetEntry> rs, SvcHierarchyRecord rec) {
    AllBroaderResultSetEntry e = new AllBroaderResultSetEntry();
    e.setRank(rec.getRankName());
    e.setLabel(rec.getTaxonName());
    e.setUri(taxonURLPrefix + rec.getTsn());
    e.setExternalID(rec.getTsn());
    rs.addEntry(e);
  }
}
