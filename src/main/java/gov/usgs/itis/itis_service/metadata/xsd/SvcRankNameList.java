/**
 * SvcRankNameList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.metadata.xsd;

public class SvcRankNameList  extends gov.usgs.itis.itis_service.metadata.xsd.SvcMetadataListBase  implements java.io.Serializable {
    private gov.usgs.itis.itis_service.metadata.xsd.SvcRankName[] rankNames;

    public SvcRankNameList() {
    }

    public SvcRankNameList(
           gov.usgs.itis.itis_service.metadata.xsd.SvcRankName[] rankNames) {
        this.rankNames = rankNames;
    }


    /**
     * Gets the rankNames value for this SvcRankNameList.
     * 
     * @return rankNames
     */
    public gov.usgs.itis.itis_service.metadata.xsd.SvcRankName[] getRankNames() {
        return rankNames;
    }


    /**
     * Sets the rankNames value for this SvcRankNameList.
     * 
     * @param rankNames
     */
    public void setRankNames(gov.usgs.itis.itis_service.metadata.xsd.SvcRankName[] rankNames) {
        this.rankNames = rankNames;
    }

    public gov.usgs.itis.itis_service.metadata.xsd.SvcRankName getRankNames(int i) {
        return this.rankNames[i];
    }

    public void setRankNames(int i, gov.usgs.itis.itis_service.metadata.xsd.SvcRankName _value) {
        this.rankNames[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcRankNameList)) return false;
        SvcRankNameList other = (SvcRankNameList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.rankNames==null && other.getRankNames()==null) || 
             (this.rankNames!=null &&
              java.util.Arrays.equals(this.rankNames, other.getRankNames())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRankNames() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRankNames());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRankNames(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcRankNameList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcRankNameList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rankNames");
        elemField.setXmlName(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "rankNames"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcRankName"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
