/**
 * SvcKingdomName.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.metadata.xsd;

public class SvcKingdomName  implements java.io.Serializable {
    private java.lang.String kingdomId;

    private java.lang.String kingdomName;

    private java.lang.String tsn;

    public SvcKingdomName() {
    }

    public SvcKingdomName(
           java.lang.String kingdomId,
           java.lang.String kingdomName,
           java.lang.String tsn) {
           this.kingdomId = kingdomId;
           this.kingdomName = kingdomName;
           this.tsn = tsn;
    }


    /**
     * Gets the kingdomId value for this SvcKingdomName.
     * 
     * @return kingdomId
     */
    public java.lang.String getKingdomId() {
        return kingdomId;
    }


    /**
     * Sets the kingdomId value for this SvcKingdomName.
     * 
     * @param kingdomId
     */
    public void setKingdomId(java.lang.String kingdomId) {
        this.kingdomId = kingdomId;
    }


    /**
     * Gets the kingdomName value for this SvcKingdomName.
     * 
     * @return kingdomName
     */
    public java.lang.String getKingdomName() {
        return kingdomName;
    }


    /**
     * Sets the kingdomName value for this SvcKingdomName.
     * 
     * @param kingdomName
     */
    public void setKingdomName(java.lang.String kingdomName) {
        this.kingdomName = kingdomName;
    }


    /**
     * Gets the tsn value for this SvcKingdomName.
     * 
     * @return tsn
     */
    public java.lang.String getTsn() {
        return tsn;
    }


    /**
     * Sets the tsn value for this SvcKingdomName.
     * 
     * @param tsn
     */
    public void setTsn(java.lang.String tsn) {
        this.tsn = tsn;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcKingdomName)) return false;
        SvcKingdomName other = (SvcKingdomName) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.kingdomId==null && other.getKingdomId()==null) || 
             (this.kingdomId!=null &&
              this.kingdomId.equals(other.getKingdomId()))) &&
            ((this.kingdomName==null && other.getKingdomName()==null) || 
             (this.kingdomName!=null &&
              this.kingdomName.equals(other.getKingdomName()))) &&
            ((this.tsn==null && other.getTsn()==null) || 
             (this.tsn!=null &&
              this.tsn.equals(other.getTsn())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getKingdomId() != null) {
            _hashCode += getKingdomId().hashCode();
        }
        if (getKingdomName() != null) {
            _hashCode += getKingdomName().hashCode();
        }
        if (getTsn() != null) {
            _hashCode += getTsn().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcKingdomName.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcKingdomName"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kingdomId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "kingdomId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kingdomName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "kingdomName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tsn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "tsn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
