/**
 * SvcOriginValueList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.metadata.xsd;

public class SvcOriginValueList  extends gov.usgs.itis.itis_service.metadata.xsd.SvcMetadataListBase  implements java.io.Serializable {
    private gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValue[] originValues;

    public SvcOriginValueList() {
    }

    public SvcOriginValueList(
           gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValue[] originValues) {
        this.originValues = originValues;
    }


    /**
     * Gets the originValues value for this SvcOriginValueList.
     * 
     * @return originValues
     */
    public gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValue[] getOriginValues() {
        return originValues;
    }


    /**
     * Sets the originValues value for this SvcOriginValueList.
     * 
     * @param originValues
     */
    public void setOriginValues(gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValue[] originValues) {
        this.originValues = originValues;
    }

    public gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValue getOriginValues(int i) {
        return this.originValues[i];
    }

    public void setOriginValues(int i, gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValue _value) {
        this.originValues[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcOriginValueList)) return false;
        SvcOriginValueList other = (SvcOriginValueList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.originValues==null && other.getOriginValues()==null) || 
             (this.originValues!=null &&
              java.util.Arrays.equals(this.originValues, other.getOriginValues())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getOriginValues() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOriginValues());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOriginValues(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcOriginValueList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcOriginValueList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originValues");
        elemField.setXmlName(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "originValues"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcOriginValue"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
