/**
 * SvcVernacularLanguageList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.metadata.xsd;

public class SvcVernacularLanguageList  extends gov.usgs.itis.itis_service.metadata.xsd.SvcMetadataListBase  implements java.io.Serializable {
    private java.lang.String[] languageNames;

    public SvcVernacularLanguageList() {
    }

    public SvcVernacularLanguageList(
           java.lang.String[] languageNames) {
        this.languageNames = languageNames;
    }


    /**
     * Gets the languageNames value for this SvcVernacularLanguageList.
     * 
     * @return languageNames
     */
    public java.lang.String[] getLanguageNames() {
        return languageNames;
    }


    /**
     * Sets the languageNames value for this SvcVernacularLanguageList.
     * 
     * @param languageNames
     */
    public void setLanguageNames(java.lang.String[] languageNames) {
        this.languageNames = languageNames;
    }

    public java.lang.String getLanguageNames(int i) {
        return this.languageNames[i];
    }

    public void setLanguageNames(int i, java.lang.String _value) {
        this.languageNames[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcVernacularLanguageList)) return false;
        SvcVernacularLanguageList other = (SvcVernacularLanguageList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.languageNames==null && other.getLanguageNames()==null) || 
             (this.languageNames!=null &&
              java.util.Arrays.equals(this.languageNames, other.getLanguageNames())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getLanguageNames() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLanguageNames());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLanguageNames(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcVernacularLanguageList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcVernacularLanguageList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("languageNames");
        elemField.setXmlName(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "languageNames"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
