/**
 * SvcVernacularTsnList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.metadata.xsd;

public class SvcVernacularTsnList  extends gov.usgs.itis.itis_service.metadata.xsd.SvcMetadataListBase  implements java.io.Serializable {
    private java.lang.String language;

    private gov.usgs.itis.itis_service.data.xsd.SvcCommonName[] vernacularTsns;

    public SvcVernacularTsnList() {
    }

    public SvcVernacularTsnList(
           java.lang.String language,
           gov.usgs.itis.itis_service.data.xsd.SvcCommonName[] vernacularTsns) {
        this.language = language;
        this.vernacularTsns = vernacularTsns;
    }


    /**
     * Gets the language value for this SvcVernacularTsnList.
     * 
     * @return language
     */
    public java.lang.String getLanguage() {
        return language;
    }


    /**
     * Sets the language value for this SvcVernacularTsnList.
     * 
     * @param language
     */
    public void setLanguage(java.lang.String language) {
        this.language = language;
    }


    /**
     * Gets the vernacularTsns value for this SvcVernacularTsnList.
     * 
     * @return vernacularTsns
     */
    public gov.usgs.itis.itis_service.data.xsd.SvcCommonName[] getVernacularTsns() {
        return vernacularTsns;
    }


    /**
     * Sets the vernacularTsns value for this SvcVernacularTsnList.
     * 
     * @param vernacularTsns
     */
    public void setVernacularTsns(gov.usgs.itis.itis_service.data.xsd.SvcCommonName[] vernacularTsns) {
        this.vernacularTsns = vernacularTsns;
    }

    public gov.usgs.itis.itis_service.data.xsd.SvcCommonName getVernacularTsns(int i) {
        return this.vernacularTsns[i];
    }

    public void setVernacularTsns(int i, gov.usgs.itis.itis_service.data.xsd.SvcCommonName _value) {
        this.vernacularTsns[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcVernacularTsnList)) return false;
        SvcVernacularTsnList other = (SvcVernacularTsnList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.language==null && other.getLanguage()==null) || 
             (this.language!=null &&
              this.language.equals(other.getLanguage()))) &&
            ((this.vernacularTsns==null && other.getVernacularTsns()==null) || 
             (this.vernacularTsns!=null &&
              java.util.Arrays.equals(this.vernacularTsns, other.getVernacularTsns())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getLanguage() != null) {
            _hashCode += getLanguage().hashCode();
        }
        if (getVernacularTsns() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVernacularTsns());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVernacularTsns(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcVernacularTsnList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcVernacularTsnList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("language");
        elemField.setXmlName(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "language"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vernacularTsns");
        elemField.setXmlName(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "vernacularTsns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://data.itis_service.itis.usgs.gov/xsd", "SvcCommonName"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
