/**
 * SvcKingdomNameList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service.metadata.xsd;

public class SvcKingdomNameList  extends gov.usgs.itis.itis_service.metadata.xsd.SvcMetadataListBase  implements java.io.Serializable {
    private gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomName[] kingdomNames;

    public SvcKingdomNameList() {
    }

    public SvcKingdomNameList(
           gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomName[] kingdomNames) {
        this.kingdomNames = kingdomNames;
    }


    /**
     * Gets the kingdomNames value for this SvcKingdomNameList.
     * 
     * @return kingdomNames
     */
    public gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomName[] getKingdomNames() {
        return kingdomNames;
    }


    /**
     * Sets the kingdomNames value for this SvcKingdomNameList.
     * 
     * @param kingdomNames
     */
    public void setKingdomNames(gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomName[] kingdomNames) {
        this.kingdomNames = kingdomNames;
    }

    public gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomName getKingdomNames(int i) {
        return this.kingdomNames[i];
    }

    public void setKingdomNames(int i, gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomName _value) {
        this.kingdomNames[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SvcKingdomNameList)) return false;
        SvcKingdomNameList other = (SvcKingdomNameList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.kingdomNames==null && other.getKingdomNames()==null) || 
             (this.kingdomNames!=null &&
              java.util.Arrays.equals(this.kingdomNames, other.getKingdomNames())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getKingdomNames() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getKingdomNames());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getKingdomNames(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SvcKingdomNameList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcKingdomNameList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kingdomNames");
        elemField.setXmlName(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "kingdomNames"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://metadata.itis_service.itis.usgs.gov/xsd", "SvcKingdomName"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
