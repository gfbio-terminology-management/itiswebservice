package gov.usgs.itis.itis_service;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.rpc.ServiceException;
import org.apache.log4j.Logger;
import org.gfbio.config.WSConfiguration;
import org.gfbio.interfaces.WSInterface;
import org.gfbio.resultset.AllBroaderResultSetEntry;
import org.gfbio.resultset.CapabilitiesResultSetEntry;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.resultset.HierarchyResultSetEntry;
import org.gfbio.resultset.MetadataResultSetEntry;
import org.gfbio.resultset.SearchResultSetEntry;
import org.gfbio.resultset.SynonymsResultSetEntry;
import org.gfbio.resultset.TermCombinedResultSetEntry;
import org.gfbio.resultset.TermOriginalResultSetEntry;
import org.gfbio.util.YAMLConfigReader;
import de.pangaea.externalwebservices.ITISDAO;
import gov.usgs.itis.itis_service.data.xsd.SvcAcceptedName;
import gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcCommonName;
import gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData;
import gov.usgs.itis.itis_service.data.xsd.SvcFullRecord;
import gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness;
import gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord;
import gov.usgs.itis.itis_service.data.xsd.SvcScientificName;
import gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonName;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSource;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList;
import gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo;

public class ITISWSServiceImpl implements WSInterface {

  private static final Logger LOGGER = Logger.getLogger(ITISWSServiceImpl.class);

  public final String ontologyDescription = "ITIS SOAP webservice";
  public final String taxonURLPrefix =
      "http://www.itis.gov/servlet/SingleRpt/SingleRpt?search_topic=TSN&search_value=";

  private ITISDAO itisdao;
  private WSConfiguration itisConfig;

  public ITISWSServiceImpl() {
    itisdao = new ITISDAO();

    YAMLConfigReader reader = YAMLConfigReader.getInstance();
    itisConfig = reader.getWSConfig("ITIS");

    LOGGER.info(" a ITIS webservice is ready to use");
  }

  @Override
  public String getAcronym() {
    return itisConfig.getAcronym();
  }

  @Override
  public String getDescription() {
    return itisConfig.getDescription();
  }

  @Override
  public String getName() {
    return itisConfig.getName();
  }

  @Override
  public String getURI() {
    return itisConfig.getWebserviceURL();
    // return "http://www.itis.gov/";
  }

  @Override
  public List<String> getDomains() {
    List<String> l = new ArrayList<String>();
    for (Domains d : itisConfig.getWsDomains()) {
      l.add(d.name());
    }
    return l;
  }

  @Override
  public String getCurrentVersion() {

    return itisConfig.getVersion();
  }

  /*
   * currently only exact is supported
   *
   */
  @Override
  public GFBioResultSet<SearchResultSetEntry> search(final String query, final String match_type) {
    LOGGER.info("search query " + query + " matchtype " + match_type);
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("itis");
    SvcScientificName[] trans = null;
    if (match_type.equals(SearchTypes.exact.name())) {
      trans = this.itisdao.searchByNameExact(query);
      format(trans, rs);
      SvcCommonName[] commons = this.itisdao.searchByCommonNameExact(query).getCommonNames();
      format(commons, rs);
    } else if (match_type.equals(SearchTypes.included.name())) {
      trans = this.itisdao.searchByNameContaining(query);
      format(trans, rs);
      SvcCommonName[] commons = this.itisdao.searchByCommonName(query).getCommonNames();
      format(commons, rs);
    }

    LOGGER.info("search query processed " + query + " matchtype " + match_type);
    return rs;
  }

  private void format(SvcCommonName[] commons, GFBioResultSet<SearchResultSetEntry> rs) {
    if (commons != null) {
      for (SvcCommonName cn : commons) {
        if (cn != null) {
          SvcScientificName sn = this.itisdao.getScientificName(cn.getTsn());
          SvcTaxonRankInfo rankInfo = this.itisdao.getTaxonRankInfoByTSN(sn.getTsn());
          setEntry(rs, sn, rankInfo);
        }
      }
    }
  }

  private void format(SvcScientificName[] trans, GFBioResultSet<SearchResultSetEntry> rs) {
    if (trans[0] != null) {
      for (SvcScientificName sn : trans) {
        if (sn != null) {
          SvcTaxonRankInfo rankInfo = this.itisdao.getTaxonRankInfoByTSN(sn.getTsn());
          setEntry(rs, sn, rankInfo);
        }
      }
    }
  }

  private void setEntry(GFBioResultSet<SearchResultSetEntry> rs, final SvcScientificName sName,
      final SvcTaxonRankInfo rankInfo) {
    SearchResultSetEntry e = new SearchResultSetEntry();
    e.setKingdom(rankInfo.getKingdomName());
    e.setLabel(sName.getCombinedName());
    e.setRank(rankInfo.getRankName());
    e.setSourceTerminology(itisConfig.getAcronym());
    e.setUri(taxonURLPrefix + sName.getTsn());
    e.setExternalID(sName.getTsn());
    e.setSourceTerminology(getAcronym());
    SvcFullRecord rec = this.itisdao.getFullRecordFromTSN(sName.getTsn());
    SvcCommonNameList commonNames = rec.getCommonNameList();
    if (commonNames.getCommonNames().length > 0) {
      ArrayList<String> commonNamesList = new ArrayList<String>();
      for (SvcCommonName a : commonNames.getCommonNames()) {
        if (a != null) {
          commonNamesList.add(a.getCommonName().trim());
        }
      }
      if (!commonNamesList.isEmpty()) {
        e.setCommonNames(commonNamesList);
      }
    }
    rs.addEntry(e);
  }

  @Override
  public GFBioResultSet<SynonymsResultSetEntry> getSynonyms(final String uriOrexternalID) {
    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    LOGGER.info("synonym query " + externalID);
    GFBioResultSet<SynonymsResultSetEntry> rs = new GFBioResultSet<SynonymsResultSetEntry>("itis");
    SvcSynonymNameList snl = this.itisdao.getTaxonSynonymsByTSN(externalID);
    if (snl != null) {
      setEntry(rs, snl);
      LOGGER.info("synonym query processed " + externalID);
      return rs;
    }
    return null;
  }

  private void setEntry(GFBioResultSet<SynonymsResultSetEntry> rs, SvcSynonymNameList snl) {
    SvcTaxonName[] list = snl.getSynonyms();
    SynonymsResultSetEntry e = new SynonymsResultSetEntry();
    ArrayList<String> array = new ArrayList<String>();
    if (list[0] != null) {
      for (SvcTaxonName n : list) {
        array.add(n.getSciName());
      }
    }
    e.setSynonyms(array);
    rs.addEntry(e);
  }

  @Override
  public GFBioResultSet<TermOriginalResultSetEntry> getTermInfosOriginal(String term_uri) {
    String externalID = term_uri;
    if (term_uri.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    GFBioResultSet<TermOriginalResultSetEntry> rs =
        new GFBioResultSet<TermOriginalResultSetEntry>("itis");
    SvcFullRecord rec = this.itisdao.getFullRecordFromTSN(externalID);
    SvcAcceptedNameList acceptedNames = rec.getAcceptedNameList();
    SvcTaxonRankInfo rank = rec.getTaxRank();
    SvcCommonNameList commonNames = rec.getCommonNameList();
    SvcScientificName scientificName = rec.getScientificName();
    SvcSynonymNameList synonyms = rec.getSynonymList();
    SvcCredibilityData cred = rec.getCredibilityRating();
    SvcGlobalSpeciesCompleteness completeness = rec.getCompletenessRating();
    SvcTaxonOtherSourceList sources = rec.getOtherSourceList();

    TermOriginalResultSetEntry e = new TermOriginalResultSetEntry();
    if (scientificName != null) {
      if (scientificName.getCombinedName() != null) {
        e.setOriginalTermInfo("ScientificName", scientificName.getCombinedName().trim());

      }
      if (scientificName.getTsn() != null) {
        e.setOriginalTermInfo("TaxonomicSerialNumber", scientificName.getTsn().trim());
        e.setOriginalTermInfo("URL", taxonURLPrefix + scientificName.getTsn().trim());
      }
    }
    if (commonNames.getCommonNames().length > 0) {
      ArrayList<String> commonNamesList = new ArrayList<String>();
      for (SvcCommonName a : commonNames.getCommonNames()) {
        if (a != null) {
          commonNamesList.add(a.getCommonName().trim());
        }
      }
      if (!commonNamesList.isEmpty()) {
        e.setOriginalTermInfo("CommonNames", commonNamesList);
      }
    }
    if (rank != null) {
      if (rank.getRankName() != null) {
        e.setOriginalTermInfo("Kingdom", rank.getKingdomName().trim());
      }
      if (rank.getKingdomName() != null) {
        e.setOriginalTermInfo("TaxonomicRank", rank.getRankName().trim());
      }
    }
    if (synonyms.getSynonyms().length > 0) {
      ArrayList<String> synonymsList = new ArrayList<String>();
      for (SvcTaxonName syn : synonyms.getSynonyms()) {
        if (syn != null) {
          synonymsList.add(syn.getSciName().trim());
        }
      }
      if (!synonymsList.isEmpty()) {
        e.setOriginalTermInfo("Synonyms", synonymsList);
      }
    }
    if (acceptedNames.getAcceptedNames().length > 0) {
      ArrayList<String> acceptedNamesList = new ArrayList<String>();
      for (SvcAcceptedName a : acceptedNames.getAcceptedNames()) {
        if (a != null) {
          acceptedNamesList.add(a.getAcceptedName().trim());
        }
      }
      if (!acceptedNamesList.isEmpty()) {
        e.setOriginalTermInfo("AcceptedNames", acceptedNamesList);
      }
    }
    if (cred.getCredRating() != null) {
      e.setOriginalTermInfo("RecordCredibilityRating", cred.getCredRating().trim());
    }
    if (completeness.getCompleteness() != null) {
      e.setOriginalTermInfo("GlobalSpeciesCompleteness", completeness.getCompleteness().trim());
    }
    if (sources.getOtherSources().length > 0) {
      ArrayList<String> otherSourceList = new ArrayList<String>();
      for (SvcTaxonOtherSource source : sources.getOtherSources()) {
        if (source != null) {
          otherSourceList.add(source.getSource().trim());
        }
      }
      if (!otherSourceList.isEmpty()) {
        e.setOriginalTermInfo("OtherSources", otherSourceList);
      }
    }
    rs.addEntry(e);
    return rs;
  }

  @Override
  public GFBioResultSet<SearchResultSetEntry> getTermInfosProcessed(String term_uri) {
    String externalID = term_uri;
    if (term_uri.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("itis");
    SvcScientificName sName = this.itisdao.getScientificName(externalID);
    SvcTaxonRankInfo rankInfo = this.itisdao.getTaxonRankInfoByTSN(externalID);
    SearchResultSetEntry e = new SearchResultSetEntry();
    if (rankInfo != null) {
      e.setKingdom(rankInfo.getKingdomName());
      e.setRank(rankInfo.getRankName());
    }
    if (sName != null) {
      e.setLabel(sName.getCombinedName());
      e.setExternalID(sName.getTsn());
      e.setUri(taxonURLPrefix + sName.getTsn());
    }
    e.setSourceTerminology(itisConfig.getAcronym());
    SvcFullRecord rec = this.itisdao.getFullRecordFromTSN(sName.getTsn());
    SvcCommonNameList commonNames = rec.getCommonNameList();
    if (commonNames.getCommonNames().length > 0) {
      ArrayList<String> commonNamesList = new ArrayList<String>();
      for (SvcCommonName a : commonNames.getCommonNames()) {
        if (a != null) {
          commonNamesList.add(a.getCommonName().trim());
        }
      }
      if (!commonNamesList.isEmpty()) {
        e.setCommonNames(commonNamesList);
      }
    }
    rs.addEntry(e);
    return rs;

  }

  @Override
  public GFBioResultSet<TermCombinedResultSetEntry> getTermInfosCombined(String uriOrexternalID) {
    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    GFBioResultSet<TermCombinedResultSetEntry> rs =
        new GFBioResultSet<TermCombinedResultSetEntry>("itis");
    SvcFullRecord rec = this.itisdao.getFullRecordFromTSN(externalID);
    SvcAcceptedNameList acceptedNames = rec.getAcceptedNameList();
    SvcTaxonRankInfo rank = rec.getTaxRank();
    SvcCommonNameList commonNames = rec.getCommonNameList();
    SvcScientificName scientificName = rec.getScientificName();
    SvcSynonymNameList synonyms = rec.getSynonymList();
    SvcCredibilityData cred = rec.getCredibilityRating();
    SvcGlobalSpeciesCompleteness completeness = rec.getCompletenessRating();
    SvcTaxonOtherSourceList sources = rec.getOtherSourceList();

    TermCombinedResultSetEntry e = new TermCombinedResultSetEntry();
    if (scientificName != null) {
      if (scientificName.getCombinedName() != null) {
        e.setLabel(scientificName.getCombinedName().trim());

      }
      if (scientificName.getTsn() != null) {
        e.setExternalID(scientificName.getTsn().trim());
        e.setUri(taxonURLPrefix + scientificName.getTsn().trim());
      }
    }
    if (commonNames.getCommonNames().length > 0) {
      ArrayList<String> commonNamesList = new ArrayList<String>();
      for (SvcCommonName a : commonNames.getCommonNames()) {
        if (a != null) {
          commonNamesList.add(a.getCommonName().trim());
        }
      }
      if (!commonNamesList.isEmpty()) {
        e.setCommonNames(commonNamesList);
      }
    }
    if (rank != null) {
      if (rank.getRankName() != null) {
        e.setKingdom(rank.getKingdomName().trim());
      }
      if (rank.getKingdomName() != null) {
        e.setRank(rank.getRankName().trim());
      }
    }
    if (synonyms.getSynonyms().length > 0) {
      ArrayList<String> synonymsList = new ArrayList<String>();
      for (SvcTaxonName syn : synonyms.getSynonyms()) {
        if (syn != null) {
          synonymsList.add(syn.getSciName().trim());
        }
      }
      if (!synonymsList.isEmpty()) {
        e.setSynonyms(synonymsList);
      }
    }
    e.setSourceTerminology(getAcronym());
    if (acceptedNames.getAcceptedNames().length > 0) {
      ArrayList<String> acceptedNamesList = new ArrayList<String>();
      for (SvcAcceptedName a : acceptedNames.getAcceptedNames()) {
        if (a != null) {
          acceptedNamesList.add(a.getAcceptedName().trim());
        }
      }
      if (!acceptedNamesList.isEmpty()) {
        e.setOriginalTermInfo("AcceptedNames", acceptedNamesList);
      }
    }
    if (cred.getCredRating() != null) {
      e.setOriginalTermInfo("RecordCredibilityRating", cred.getCredRating().trim());
    }
    if (completeness.getCompleteness() != null) {
      e.setOriginalTermInfo("GlobalSpeciesCompleteness", completeness.getCompleteness().trim());
    }
    if (sources.getOtherSources().length > 0) {
      ArrayList<String> otherSourceList = new ArrayList<String>();
      for (SvcTaxonOtherSource source : sources.getOtherSources()) {
        if (source != null) {
          otherSourceList.add(source.getSource().trim());
        }
      }
      if (!otherSourceList.isEmpty()) {
        e.setOriginalTermInfo("OtherSources", otherSourceList);
      }
    }
    rs.addEntry(e);
    return rs;
  }

  @Override
  public boolean supportsMatchType(String match_type) {
    return true;
  }

  @Override
  public boolean isResponding() {
    ITISServiceLocator locator = new ITISServiceLocator();
    try {
      ITISServicePortType port = locator.getITISServiceHttpSoap11Endpoint();
      port.searchByScientificNameExact("quercus robur");
    } catch (ServiceException e) {
      LOGGER.error("ITIS service is not responding.");
      return false;
    } catch (RemoteException e) {
      LOGGER.error("ITIS service is not responding.");
      return false;
    }
    LOGGER.info("ITIS is responding.");
    return true;
  }

  @Override
  public GFBioResultSet<HierarchyResultSetEntry> getHierarchy(String uriOrexternalID) {
    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    LOGGER.info("search query " + externalID);
    GFBioResultSet<HierarchyResultSetEntry> rs =
        new GFBioResultSet<HierarchyResultSetEntry>("itis");
    SvcHierarchyRecord[] hierarchy = this.itisdao.getFullHierarchyByTSN(externalID);
    for (SvcHierarchyRecord rec : hierarchy) {
      HierarchyResultSetEntry e = new HierarchyResultSetEntry();
      ArrayList<String> array = new ArrayList<String>();
      if (!rec.getParentTsn().equals(externalID)) {
        if (!rec.getParentTsn().equals("")) {
          array.add(taxonURLPrefix + rec.getParentTsn());
        }
        e.setLabel(rec.getTaxonName());
        e.setRank(rec.getRankName());
        e.setUri(taxonURLPrefix + rec.getTsn());
        e.setExternalID(rec.getTsn());
        e.setHierarchy(array);
        rs.addEntry(e);
      }
    }
    LOGGER.info("search query processed " + externalID);
    return rs;
  }

  @Override
  public GFBioResultSet<AllBroaderResultSetEntry> getAllBroader(String uriOrexternalID) {
    String externalID = uriOrexternalID;
    if (uriOrexternalID.startsWith(taxonURLPrefix)) {
      externalID = externalID.substring(taxonURLPrefix.length());
    }
    LOGGER.info("search query " + externalID);
    GFBioResultSet<AllBroaderResultSetEntry> rs =
        new GFBioResultSet<AllBroaderResultSetEntry>("itis");
    SvcHierarchyRecord hierarchy = this.itisdao.getHierarchyByTSN(externalID);
    hierarchy = this.itisdao.getHierarchyByTSN(hierarchy.getParentTsn());

    if (hierarchy != null) {
      while (hierarchy.getParentTsn() != null) {
        setEntry(rs, hierarchy);
        hierarchy = this.itisdao.getHierarchyByTSN(hierarchy.getParentTsn());
      }
    } else {
      LOGGER.error(" no classification for this id  " + externalID);
      return null;
    }
    LOGGER.info("search query processed " + externalID);
    return rs;
  }

  public void setEntry(GFBioResultSet<AllBroaderResultSetEntry> rs, SvcHierarchyRecord rec) {
    AllBroaderResultSetEntry e = new AllBroaderResultSetEntry();
    e.setRank(rec.getRankName());
    e.setLabel(rec.getTaxonName());
    e.setUri(taxonURLPrefix + rec.getTsn());
    e.setExternalID(rec.getTsn());
    rs.addEntry(e);
  }

  @Override
  public GFBioResultSet<MetadataResultSetEntry> getMetadata() {
    GFBioResultSet<MetadataResultSetEntry> rs = new GFBioResultSet<MetadataResultSetEntry>("itis");
    MetadataResultSetEntry e = new MetadataResultSetEntry();
    e.setName(getName());
    e.setAcronym(getAcronym());
    e.setVersion(itisConfig.getVersion());
    e.setDescription(getDescription());
    e.setKeywords(itisConfig.getKeywords());
    e.setUri(getURI());
    e.setContact(itisConfig.getContact());
    ArrayList<String> domainUris = new ArrayList<String>();
    for (Domains d : itisConfig.getWsDomains()) {
      domainUris.add(d.getUri());
    }
    e.setDomain(domainUris);
    rs.addEntry(e);
    return rs;
  }

  public GFBioResultSet<CapabilitiesResultSetEntry> getCapabilities() {
    GFBioResultSet<CapabilitiesResultSetEntry> rs =
        new GFBioResultSet<CapabilitiesResultSetEntry>("geonames");
    CapabilitiesResultSetEntry e = new CapabilitiesResultSetEntry();
    ArrayList<String> servicesArray = new ArrayList<String>();
    for (Services s : itisConfig.getAvailableServices()) {
      servicesArray.add(s.toString());
    }
    e.setAvailableServices(servicesArray);

    ArrayList<String> modesArray = new ArrayList<String>();
    for (SearchModes m : itisConfig.getSearchModes()) {
      modesArray.add(m.toString());
    }
    e.setSearchModes(modesArray);
    rs.addEntry(e);
    return rs;
  }
}
