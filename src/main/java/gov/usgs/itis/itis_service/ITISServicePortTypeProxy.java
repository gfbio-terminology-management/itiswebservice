package gov.usgs.itis.itis_service;

public class ITISServicePortTypeProxy implements gov.usgs.itis.itis_service.ITISServicePortType {
  private String _endpoint = null;
  private gov.usgs.itis.itis_service.ITISServicePortType iTISServicePortType = null;
  
  public ITISServicePortTypeProxy() {
    _initITISServicePortTypeProxy();
  }
  
  public ITISServicePortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initITISServicePortTypeProxy();
  }
  
  private void _initITISServicePortTypeProxy() {
    try {
      iTISServicePortType = (new gov.usgs.itis.itis_service.ITISServiceLocator()).getITISServiceHttpSoap11Endpoint();
      if (iTISServicePortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iTISServicePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iTISServicePortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iTISServicePortType != null)
      ((javax.xml.rpc.Stub)iTISServicePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public gov.usgs.itis.itis_service.ITISServicePortType getITISServicePortType() {
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType;
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcKingdomInfo getKingdomNameFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getKingdomNameFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcFullRecord getFullRecordFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getFullRecordFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.metadata.xsd.SvcRankNameList getRankNames() throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getRankNames();
  }
  
  public gov.usgs.itis.itis_service.metadata.xsd.SvcGeographicValueList getGeographicValues() throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getGeographicValues();
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcItisTermList getITISTerms(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getITISTerms(srchKey);
  }
  
  public gov.usgs.itis.itis_service.xsd.SvcDescription getDescription() throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getDescription();
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcTaxonAuthorship getTaxonAuthorshipFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getTaxonAuthorshipFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList searchByCommonNameBeginsWith(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.searchByCommonNameBeginsWith(srchKey);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcTaxonUsageData getTaxonomicUsageFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getTaxonomicUsageFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcItisTermList getITISTermsFromCommonName(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getITISTermsFromCommonName(srchKey);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList getFullHierarchyFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getFullHierarchyFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcLSIDRecord getRecordFromLSID(java.lang.String lsid) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getRecordFromLSID(lsid);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcCurrencyData getCurrencyFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getCurrencyFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[] searchForAnyMatchPaged(java.lang.String srchKey, java.lang.Integer pageSize, java.lang.Integer pageNum, java.lang.Boolean ascend) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.searchForAnyMatchPaged(srchKey, pageSize, pageNum, ascend);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData getUnacceptabilityReasonFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getUnacceptabilityReasonFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularTsnList getTsnByVernacularLanguage(java.lang.String language) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getTsnByVernacularLanguage(language);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList getSynonymNamesFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getSynonymNamesFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificNameExact(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.searchByScientificNameExact(srchKey);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificNameBeginsWith(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.searchByScientificNameBeginsWith(srchKey);
  }
  
  public gov.usgs.itis.itis_service.metadata.xsd.SvcDateData getLastChangeDate() throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getLastChangeDate();
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness getGlobalSpeciesCompletenessFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getGlobalSpeciesCompletenessFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcTaxonDateData getDateDataFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getDateDataFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList getCommonNamesFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getCommonNamesFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList searchByCommonName(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.searchByCommonName(srchKey);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList searchByCommonNameEndsWith(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.searchByCommonNameEndsWith(srchKey);
  }
  
  public java.lang.Long getAnyMatchCount(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getAnyMatchCount(srchKey);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcScientificName getScientificNameFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getScientificNameFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo getTaxonomicRankNameFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getTaxonomicRankNameFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList getAcceptedNamesFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getAcceptedNamesFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOriginList getJurisdictionalOriginFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getJurisdictionalOriginFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificNameContains(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.searchByScientificNameContains(srchKey);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublicationList getPublicationsFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getPublicationsFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcItisTermList getITISTermsFromScientificName(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getITISTermsFromScientificName(srchKey);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord getHierarchyUpFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getHierarchyUpFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcReviewYear getReviewYearFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getReviewYearFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValueList getJurisdictionalOriginValues() throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getJurisdictionalOriginValues();
  }
  
  public java.lang.String getLSIDFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getLSIDFromTSN(tsn);
  }
  
  public java.lang.String getTSNFromLSID(java.lang.String lsid) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getTSNFromLSID(lsid);
  }
  
  public gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomNameList getKingdomNames() throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getKingdomNames();
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivisionList getGeographicDivisionsFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getGeographicDivisionsFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData getCredibilityRatingFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getCredibilityRatingFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList getOtherSourcesFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getOtherSourcesFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList getHierarchyDownFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getHierarchyDownFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcTaxonCommentList getCommentDetailFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getCommentDetailFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.metadata.xsd.SvcCredibilityValueList getCredibilityRatings() throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getCredibilityRatings();
  }
  
  public gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularLanguageList getVernacularLanguages() throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getVernacularLanguages();
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata getCoreMetadataFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getCoreMetadataFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.metadata.xsd.SvcJurisdictionValueList getJurisdictionValues() throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getJurisdictionValues();
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcParentTsn getParentTSNFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getParentTSNFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificNameEndsWith(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.searchByScientificNameEndsWith(srchKey);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcFullRecord getFullRecordFromLSID(java.lang.String lsid) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getFullRecordFromLSID(lsid);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[] searchForAnyMatch(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.searchForAnyMatch(srchKey);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcCoverageData getCoverageFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getCoverageFromTSN(tsn);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificName(java.lang.String srchKey) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.searchByScientificName(srchKey);
  }
  
  public gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpertList getExpertsFromTSN(java.lang.String tsn) throws java.rmi.RemoteException{
    if (iTISServicePortType == null)
      _initITISServicePortTypeProxy();
    return iTISServicePortType.getExpertsFromTSN(tsn);
  }
  
  
}