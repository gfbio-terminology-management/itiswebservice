/**
 * ITISServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package gov.usgs.itis.itis_service;

public interface ITISServicePortType extends java.rmi.Remote {
    public gov.usgs.itis.itis_service.data.xsd.SvcKingdomInfo getKingdomNameFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcFullRecord getFullRecordFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.metadata.xsd.SvcRankNameList getRankNames() throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.metadata.xsd.SvcGeographicValueList getGeographicValues() throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcItisTermList getITISTerms(java.lang.String srchKey) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.xsd.SvcDescription getDescription() throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonAuthorship getTaxonAuthorshipFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList searchByCommonNameBeginsWith(java.lang.String srchKey) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonUsageData getTaxonomicUsageFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcItisTermList getITISTermsFromCommonName(java.lang.String srchKey) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList getFullHierarchyFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcLSIDRecord getRecordFromLSID(java.lang.String lsid) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcCurrencyData getCurrencyFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[] searchForAnyMatchPaged(java.lang.String srchKey, java.lang.Integer pageSize, java.lang.Integer pageNum, java.lang.Boolean ascend) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcUnacceptData getUnacceptabilityReasonFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularTsnList getTsnByVernacularLanguage(java.lang.String language) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcSynonymNameList getSynonymNamesFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificNameExact(java.lang.String srchKey) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificNameBeginsWith(java.lang.String srchKey) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.metadata.xsd.SvcDateData getLastChangeDate() throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcGlobalSpeciesCompleteness getGlobalSpeciesCompletenessFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonDateData getDateDataFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList getCommonNamesFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList searchByCommonName(java.lang.String srchKey) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcCommonNameList searchByCommonNameEndsWith(java.lang.String srchKey) throws java.rmi.RemoteException;
    public java.lang.Long getAnyMatchCount(java.lang.String srchKey) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName getScientificNameFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonRankInfo getTaxonomicRankNameFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcAcceptedNameList getAcceptedNamesFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonJurisdictionalOriginList getJurisdictionalOriginFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificNameContains(java.lang.String srchKey) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonPublicationList getPublicationsFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcItisTermList getITISTermsFromScientificName(java.lang.String srchKey) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecord getHierarchyUpFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcReviewYear getReviewYearFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.metadata.xsd.SvcOriginValueList getJurisdictionalOriginValues() throws java.rmi.RemoteException;
    public java.lang.String getLSIDFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public java.lang.String getTSNFromLSID(java.lang.String lsid) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.metadata.xsd.SvcKingdomNameList getKingdomNames() throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonGeoDivisionList getGeographicDivisionsFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcCredibilityData getCredibilityRatingFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonOtherSourceList getOtherSourcesFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcHierarchyRecordList getHierarchyDownFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonCommentList getCommentDetailFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.metadata.xsd.SvcCredibilityValueList getCredibilityRatings() throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.metadata.xsd.SvcVernacularLanguageList getVernacularLanguages() throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcCoreMetadata getCoreMetadataFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.metadata.xsd.SvcJurisdictionValueList getJurisdictionValues() throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcParentTsn getParentTSNFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificNameEndsWith(java.lang.String srchKey) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcFullRecord getFullRecordFromLSID(java.lang.String lsid) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcAnyMatch[] searchForAnyMatch(java.lang.String srchKey) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcCoverageData getCoverageFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcScientificName[] searchByScientificName(java.lang.String srchKey) throws java.rmi.RemoteException;
    public gov.usgs.itis.itis_service.data.xsd.SvcTaxonExpertList getExpertsFromTSN(java.lang.String tsn) throws java.rmi.RemoteException;
}
